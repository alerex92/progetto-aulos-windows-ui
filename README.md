# AULOS WINDOWS FORM UI ![Loccioni Logo](/WindowsUI/Documentation/LoccioniLogoSmall.png)

## How to use the UI
First, create a Visual Studio project using the Windows Form Application template and instal the Loccioni.Aulos.CSharp.WindowsUI nuget package.

### Startup Catalog
Create a Catalog to start the UI, here is an example of the code needed:

WinFormUI.csx
```c#
#load "..\references.csx"

#r "System.Drawing"
#r "System.IO"
#r "System.Windows.Forms"

using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using System.Threading;
using System.Threading.Tasks;
using Loccioni.Aulos;
using Loccioni.Aulos.Configuration;
using Loccioni.AulosProject;
using WindowsUI.UI;

public class WinFormLocalUICatalog : Catalog
{
    public WinFormLocalUICatalog(string name, string filename) : base(name, filename)
    {
    }

    public override void Build()
    {
        Thread thread = new Thread(CreateAndShow);
        thread.SetApartmentState(ApartmentState.STA);
        thread.IsBackground = true;
        thread.Start();
    }

    private void CreateAndShow()
    {
        dynamic webServerCatalog = null;
        if (ServiceBroker.GetService<CatalogsService>().ContainsKey("WebServer"))
            webServerCatalog = ServiceBroker.GetService<CatalogsService>()["WebServer"];
        
        GlobalConfiguration configuration = JsonConvert.DeserializeObject<GlobalConfiguration>(File.ReadAllText(@"Configuration\globalConfiguration.json"));
        WebClientService webClientService = new WebClientService();
        webClientService.Add(typeof(AlarmsClient), new AlarmsClient("http://" + configuration.ConnectionParameters.baseUrl + ":" + configuration.ConnectionParameters.urlPort + "/" + configuration.ConnectionParameters.baseWebService + "/", "ws://" + configuration.ConnectionParameters.baseUrl + ":" + configuration.ConnectionParameters.urlWsPort + "/" + configuration.ConnectionParameters.baseWebService + "/messages/alarms/subscriptions"));
        webClientService.Add(typeof(WarningsClient), new WarningsClient("http://" + configuration.ConnectionParameters.baseUrl + ":" + configuration.ConnectionParameters.urlPort + "/" + configuration.ConnectionParameters.baseWebService + "/", "ws://" + configuration.ConnectionParameters.baseUrl + ":" + configuration.ConnectionParameters.urlWsPort + "/" + configuration.ConnectionParameters.baseWebService + "/messages/warnings/subscriptions"));
        webClientService.Add(typeof(SystemClient), new SystemClient("http://" + configuration.ConnectionParameters.baseUrl + ":" + configuration.ConnectionParameters.urlPort + "/" + configuration.ConnectionParameters.baseWebService + "/"));
        webClientService.Add(typeof(SecurityClient), new SecurityClient("http://" + configuration.ConnectionParameters.baseUrl + ":" + configuration.ConnectionParameters.urlPort + "/" + configuration.ConnectionParameters.baseWebService + "/", "ws://" + configuration.ConnectionParameters.baseUrl + ":" + configuration.ConnectionParameters.urlWsPort + "/" + configuration.ConnectionParameters.baseWebService + "/security/subscriptions"));

        ServiceBroker.RegistService<WebClientService, WebClientService>(webClientService);

        if (configuration.SmallScreens)
        {
            Application.Run(new MainLayoutSmall() { Configuration = configuration });
        }
        else
        {
            Application.Run(new MainLayout() { Configuration = configuration });
        }

        if (CfxRuntime.LibrariesLoaded)
        {
            CfxRuntime.Shutdown();
        }
    }

    public override void Destroy()
    {
        if (!winForm.IsDisposed)
            winForm.Invoke((MethodInvoker)delegate () { winForm.Close(); });
    }
}
```

### Fonts
Install the fonts (Aulos Font and Open Sans) located in the folder Documentation/Fonts.  

It's already possible to start the UI with the default demo configuration to check if everything works correctly.

## Configuration
The application is configured using the 2 files located in the Configuration folder:
- globalConfiguration.json: contains general information of the application. The accepted parameters are the following:
    + MenuItemsFile: contains the path of the configuration file that controls the content (menu and containers) of the UI
    + ConnectionParameters: contains the parameters to configure the connection with the server. An example of 2 standard url is  
        http://baseUrl:urlPort/baseWebService/  
        ws://baseUrl:urlWsPort/baseWebService/messages/alarms/subscriptions  
    + ShutdownServer: indicates if the server will be shutdown when the UI is closed
    + LoginRequired (DEVELOPMENT ONLY): indicates if users are configured / needed
	+ SplashRequired (DEVELOPMENT ONLY): indicates if a splash screen is required
    + SmallScreens: activates the smaller version of UI usually used in resolution of 800x600 or lower
	+ NotificationSetup (DEVELOPMENT ONLY): indicates if the notification are requested from the server
	+ BenchName: Name of the bench displayed on the bottom left of the UI  
Here is an example of this file:  
```json
{
  "MenuItemsFile": "Configuration\\nav.json",
  "ConnectionParameters": {
    "baseUrl": "localhost",
    "urlPort": 3000,
    "urlWsPort": 3001,
    "baseWebService": "jsonserver"
  },
  "ShutdownServer": false,
  "LoginRequired": false,
  "SplashRequired": false,
  "SmallScreens": false,
  "NotificationSetup": false,
  "BenchName" : "AULOS demo test bench"
}
```
- nav.json: configuration file that controls the content (menu and containers) of the UI. There are 2 types of items that can be configured:
	+ Submenu item: an item that does not match with a container. This item has 2 required fields and 1 optional field:
		* label: indicates the name of the menu item
		* icon (FIRST MENU LEVEL ONLY): an icon that needs to be set for items of the first level menu only
		* items: an array of items
	+ Container item: an item that match with a container in the Content Tape. This item has 2 possible configuration according to its type (WinContainer or WebContainer):
		* label: indicates the name of the menu item
		* icon (FIRST MENU LEVEL ONLY): an icon that needs to be set for items of the first level menu only
		* containerId: a unique id to identify the container
		* type: the type of the container (Namespace.Class)
        * path (OPTIONAL): the path of the assembly that contains the class of the container. The path can be absolute or relative (to the windowsui.exe folder)
		* options (WebContainer ONLY): a sub-object that contains 2 parameters, url and width of the WebContainer  
Here is an example of this file:  
```json
  {
  "items": [
    {
      "label": "Automatic",
      "icon": "automatic",
      "items": [
        {
          "label": "Overview",
          "items": [
            {
              "label": "Program Editor",
              "type": "WindowsUI.MainContent.WebContainer",
              "containerId": "automatic.overview.programEditor",
              "path": "",
              "options": {
                "url": "google.it",
                "width": 900
              }
            },
            {
              "label": "Diagnostic",
              "type": "WindowsUI.MainContent.ExampleContainer",
              "containerId": "automatic.overview.diagnostic",
              "path": ""
            }
          ]
        }
      ]
    },
    {
      "label": "Manual",
      "icon": "manual",
      "items": [
        {
          "label": "Flow1",
          "type": "WindowsUI.MainContent.ExampleContainer",
          "containerId": "manual.flow1"
        },
        {
          "label": "Flow2",
          "type": "WindowsUI.MainContent.ExampleContainer",
          "containerId": "manual.flow2"
        }
      ]
    }
  ]
}
```

There are 2 types of nav that can be configured according to the SmallScreens value in globalConfiguration.json :  
- Small menu: Only 2 level of menu, on the first level both types of items can be configured while on the second level only Container items can be configured;
- Big menu: 3 level of menu, on the first level only Submenu items can be configured, on second level both types of items can be configured and on the third 
 level only Container items can be configured;
 
## How to create a new type of container
A container is a User control that inherits from the WinContainer class included in the WindowsUI package.  
The standard container has only a title, a operator message and an empty body.  
In the body the developer can add Modules, user controls that inherits from the ContainerModule class.  
An example of WinContainer is provided in the project (MainContent/ExampleContainer.cs)