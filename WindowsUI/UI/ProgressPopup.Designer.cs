﻿namespace WindowsUI.UI
{
    partial class ProgressPopup
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.popupTitle = new System.Windows.Forms.Label();
            this.popupMessage = new System.Windows.Forms.Label();
            this.progressBar = new WindowsUI.UI.CustomProgressBar();
            this.body.SuspendLayout();
            this.SuspendLayout();
            // 
            // body
            // 
            this.body.Controls.Add(this.progressBar);
            this.body.Controls.Add(this.popupMessage);
            this.body.Controls.Add(this.popupTitle);
            this.body.Location = new System.Drawing.Point(9, 9);
            this.body.Size = new System.Drawing.Size(360, 140);
            // 
            // popupTitle
            // 
            this.popupTitle.AutoSize = true;
            this.popupTitle.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.popupTitle.Location = new System.Drawing.Point(136, 20);
            this.popupTitle.Name = "popupTitle";
            this.popupTitle.Size = new System.Drawing.Size(88, 19);
            this.popupTitle.TabIndex = 0;
            this.popupTitle.Text = "Popup Title";
            this.popupTitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // popupMessage
            // 
            this.popupMessage.AutoEllipsis = true;
            this.popupMessage.AutoSize = true;
            this.popupMessage.Font = new System.Drawing.Font("Open Sans", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.popupMessage.Location = new System.Drawing.Point(20, 59);
            this.popupMessage.MaximumSize = new System.Drawing.Size(320, 50);
            this.popupMessage.Name = "popupMessage";
            this.popupMessage.Size = new System.Drawing.Size(314, 34);
            this.popupMessage.TabIndex = 1;
            this.popupMessage.Text = "Lorem ipsum dolor sit amet, consectetuer s sadipiscing elit. Aenean commodo ligul" +
    "a eget dolor.";
            this.popupMessage.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // progressBar
            // 
            this.progressBar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(204)))), ((int)(((byte)(204)))));
            this.progressBar.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(102)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
            this.progressBar.Location = new System.Drawing.Point(20, 110);
            this.progressBar.Margin = new System.Windows.Forms.Padding(0);
            this.progressBar.Name = "progressBar";
            this.progressBar.Size = new System.Drawing.Size(320, 10);
            this.progressBar.Step = 1;
            this.progressBar.TabIndex = 2;
            // 
            // ProgressPopup
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "ProgressPopup";
            this.Size = new System.Drawing.Size(378, 158);
            this.body.ResumeLayout(false);
            this.body.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label popupTitle;
        private System.Windows.Forms.Label popupMessage;
        private CustomProgressBar progressBar;
    }
}
