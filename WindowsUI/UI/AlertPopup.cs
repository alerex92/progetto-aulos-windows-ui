﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsUI.UI
{
    public partial class AlertPopup : Popup
    { 
        public bool Critical { private get; set; }

        public AlertPopup()
        {
            InitializeComponent();
        }

        public void BuildPopup(string title, string message)
        {
            if (MainForm.Height >= 1024)
            {
                body.Size = new Size(400, 200);
            }
            else
            {
                body.Size = new Size(360, 200);
            }

            alertIcon.Location = new Point((body.Width - 34) / 2, 20);
            alertIcon.Text = FontAulos.Alarm;

            popupTitle.Text = title;
            popupTitle.MaximumSize = new Size(body.Width - 55 - 20, 19);
            popupTitle.Location = new Point((body.Width - popupTitle.Width) / 2, alertIcon.Location.Y + alertIcon.Height);
           
            popupMessage.Text = message;
            popupMessage.Size = new Size(body.Width - 40, 35);
            popupMessage.Location = new Point(20, popupTitle.Location.Y + popupTitle.Height + 20);

            acknowledge.Size = new Size(body.Width - 40, 40);
            acknowledge.Location = new Point(20, body.Height - 20 - acknowledge.Height);

            this.Add();
        }

        private void acknowledge_Click(object sender, EventArgs e)
        {
            this.CustomDispose();
            if (Critical)
            {
                MainForm.Close();
            }
        }
    }
}
