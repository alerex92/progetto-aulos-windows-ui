﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Windows.Forms;

namespace WindowsUI.UI
{
    public partial class Popup : UserControl
    {
//Alert active si attiva quando fai qualcosa ed è quello con la progress bar (quindi tipo il restart o lo shutdown)
//mentre l'alert passiv è quello che si attiva quando "subisci" qualcosa cioè qualcun altro ad esempio ha spento il 
//server oppure c'è un errore da qualche parte e tutto quello che puoi fare è l'acknowledge

        
        //CustomProgressBar progressBar;

        Rectangle rect;

        PictureBox backgroundScreen;

        int shadowDepth = 9;
        int shadowMaxOpacity = 125;
        Color shadowColor = Color.Black;

        //Parametri di test
        //BackgroundWorker backgroundWorker;


        //Parametri esterni
        public Form MainForm { get; set;}

        public Popup()
        {
            InitializeComponent();
            this.SetStyle(ControlStyles.OptimizedDoubleBuffer, true);
            this.SetStyle(ControlStyles.AllPaintingInWmPaint, true);
        }

        public void Add()
        {
            Bitmap screenshot = new Bitmap(MainForm.Width, MainForm.Height, PixelFormat.Format32bppArgb);
            //Graphics gfxScreenshot = Graphics.FromImage(screenshot);
            //gfxScreenshot.CopyFromScreen(new Point(0, 0), new Point(0, 0), MainForm.Size);
            using (var canvas = Graphics.FromImage(screenshot))
            {
                canvas.CopyFromScreen(new Point(0, 0), new Point(0, 0), MainForm.Size);
                canvas.InterpolationMode = InterpolationMode.HighQualityBicubic;
                canvas.FillRectangle(new SolidBrush(Color.FromArgb(100, Color.Black)), new Rectangle(new Point(0, 0), screenshot.Size));
                canvas.Save();
            }
            backgroundScreen = new PictureBox()
            {
                Image = screenshot,
                SizeMode = PictureBoxSizeMode.StretchImage,
                Size = new Size(MainForm.Width, MainForm.Height),
                Location = MainForm.Location,
            };
            MainForm.Controls.Add(backgroundScreen);
            backgroundScreen.BringToFront();
            /*Panel darkOverlay = new Panel()
            {
                BackColor = Color.FromArgb(100, Color.Black),
                Size = MainForm.Size,
                Location = new Point(0, 0),
            };
            backgroundScreen.Controls.Add(darkOverlay);*/
            this.Location = new Point((MainForm.Width - this.Width) / 2, (MainForm.Height - this.Height) / 2);
            backgroundScreen.Controls.Add(this);
        }

        private void Popup_Paint(object sender, PaintEventArgs e)
        {
            rect = new Rectangle(new Point(0, 0), this.Size);
            RectangleDropShadow(e.Graphics, rect, shadowColor, shadowDepth, shadowMaxOpacity);
        }

        public void RectangleDropShadow(Graphics tg, Rectangle rc, Color shadowColor, int depth, int maxOpacity)
        {
            //calculate the opacities
            Color darkShadow = Color.FromArgb(maxOpacity, shadowColor);
            Color lightShadow = Color.FromArgb(0, shadowColor);
            //Create a brush that will create a softshadow circle
            GraphicsPath gp = new GraphicsPath();
            gp.AddEllipse(0, 0, 2 * depth, 2 * depth);
            PathGradientBrush pgb = new PathGradientBrush(gp);
            pgb.CenterColor = darkShadow;
            pgb.SurroundColors = new Color[] { lightShadow };
            //generate a softshadow pattern that can be used to paint the shadow
            Bitmap patternbm = new Bitmap(2 * depth, 2 * depth);
            Graphics g = Graphics.FromImage(patternbm);
            g.FillEllipse(pgb, 0, 0, 2 * depth, 2 * depth);
            g.Dispose();
            pgb.Dispose();
            //SolidBrush sb = new SolidBrush(Color.FromArgb(maxOpacity, shadowColor));
            //tg.FillRectangle(sb, rc.Left + depth, rc.Top + depth, rc.Width - (2 * depth), rc.Height - (2 * depth));
            //sb.Dispose();
            //top left corner
            tg.DrawImage(patternbm, new Rectangle(rc.Left, rc.Top, depth, depth), 0, 0, depth, depth, GraphicsUnit.Pixel);
            //top side
            tg.DrawImage(patternbm, new Rectangle(rc.Left + depth, rc.Top, rc.Width - (2 * depth), depth), depth, 0, 1, depth, GraphicsUnit.Pixel);
            //top right corner
            tg.DrawImage(patternbm, new Rectangle(rc.Right - depth, rc.Top, depth, depth), depth, 0, depth, depth, GraphicsUnit.Pixel);
            //right side
            tg.DrawImage(patternbm, new Rectangle(rc.Right - depth, rc.Top + depth, depth, rc.Height - (2 * depth)), depth, depth, depth, 1, GraphicsUnit.Pixel);
            //bottom left corner
            tg.DrawImage(patternbm, new Rectangle(rc.Right - depth, rc.Bottom - depth, depth, depth), depth, depth, depth, depth, GraphicsUnit.Pixel);
            //bottom side
            tg.DrawImage(patternbm, new Rectangle(rc.Left + depth, rc.Bottom - depth, rc.Width - (2 * depth), depth), depth, depth, 1, depth, GraphicsUnit.Pixel);
            //bottom left corner
            tg.DrawImage(patternbm, new Rectangle(rc.Left, rc.Bottom - depth, depth, depth), 0, depth, depth, depth, GraphicsUnit.Pixel);
            //left side
            tg.DrawImage(patternbm, new Rectangle(rc.Left, rc.Top + depth, depth, rc.Height - (2 * depth)), 0, depth, depth, 1, GraphicsUnit.Pixel);
            patternbm.Dispose();
        }

        private void Body_Resize(object sender, EventArgs e)
        {
            body.Location = new Point(shadowDepth, shadowDepth);
            this.Size = new Size(body.Width + (2 * shadowDepth), body.Height + (2 * shadowDepth));
            this.Refresh();
        }

        public void CustomDispose()
        {
            backgroundScreen.Dispose();
        }
    }
}
