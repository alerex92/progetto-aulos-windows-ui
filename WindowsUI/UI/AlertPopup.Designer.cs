﻿namespace WindowsUI.UI
{
    partial class AlertPopup
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.alertIcon = new WindowsUI.UI.NoPaddingLabel();
            this.popupTitle = new System.Windows.Forms.Label();
            this.popupMessage = new System.Windows.Forms.Label();
            this.acknowledge = new System.Windows.Forms.Button();
            this.body.SuspendLayout();
            this.SuspendLayout();
            // 
            // body
            // 
            this.body.Controls.Add(this.acknowledge);
            this.body.Controls.Add(this.popupMessage);
            this.body.Controls.Add(this.popupTitle);
            this.body.Controls.Add(this.alertIcon);
            this.body.Location = new System.Drawing.Point(9, 9);
            this.body.Size = new System.Drawing.Size(360, 200);
            // 
            // alertIcon
            // 
            this.alertIcon.BackColor = System.Drawing.Color.Transparent;
            this.alertIcon.Font = new System.Drawing.Font("Aulos", 34F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.alertIcon.Location = new System.Drawing.Point(163, 20);
            this.alertIcon.Name = "alertIcon";
            this.alertIcon.Size = new System.Drawing.Size(34, 34);
            this.alertIcon.TabIndex = 0;
            this.alertIcon.Text = "A";
            // 
            // popupTitle
            // 
            this.popupTitle.AutoSize = true;
            this.popupTitle.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.popupTitle.Location = new System.Drawing.Point(160, 54);
            this.popupTitle.Name = "popupTitle";
            this.popupTitle.Size = new System.Drawing.Size(39, 19);
            this.popupTitle.TabIndex = 1;
            this.popupTitle.Text = "Title";
            // 
            // popupMessage
            // 
            this.popupMessage.Font = new System.Drawing.Font("Open Sans", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.popupMessage.Location = new System.Drawing.Point(20, 93);
            this.popupMessage.Name = "popupMessage";
            this.popupMessage.Size = new System.Drawing.Size(320, 35);
            this.popupMessage.TabIndex = 2;
            this.popupMessage.Text = "Lorem ipsum dolor sit amet, consectetuer  s s s sadipiscing elit. Aenean commodo " +
    "ligula eget dolor.";
            this.popupMessage.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // acknowledge
            // 
            this.acknowledge.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(176)))), ((int)(((byte)(207)))), ((int)(((byte)(254)))));
            this.acknowledge.FlatAppearance.BorderSize = 0;
            this.acknowledge.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(102)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
            this.acknowledge.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(207)))), ((int)(((byte)(254)))));
            this.acknowledge.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.acknowledge.Location = new System.Drawing.Point(20, 140);
            this.acknowledge.Margin = new System.Windows.Forms.Padding(0);
            this.acknowledge.Name = "acknowledge";
            this.acknowledge.Size = new System.Drawing.Size(320, 40);
            this.acknowledge.TabIndex = 3;
            this.acknowledge.Text = "Acknowledge";
            this.acknowledge.UseVisualStyleBackColor = false;
            this.acknowledge.Click += new System.EventHandler(this.acknowledge_Click);
            // 
            // AlertPopup
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "AlertPopup";
            this.Size = new System.Drawing.Size(378, 218);
            this.body.ResumeLayout(false);
            this.body.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private NoPaddingLabel alertIcon;
        private System.Windows.Forms.Label popupTitle;
        private System.Windows.Forms.Label popupMessage;
        private System.Windows.Forms.Button acknowledge;
    }
}
