﻿namespace WindowsUI.UI
{
    static class Animation
    {
        public static float EaseInOut(float time, float begin, float change, float duration)
        {
            if ((time /= duration / 2) < 1) return change / 2 * time * time * time * time + begin;
            return -change / 2 * ((time -= 2) * time * time * time - 2) + begin;
        }

        /*public static int[] CappedEaseInOut(float time, float begin, float change, float duration, int maxDistance)
        {
            int easeDistance = (int) EaseInOut(time, begin, change, duration);
            int percentage = 100;
            if (easeDistance > maxDistance)
            {
                percentage = maxDistance*100/easeDistance;
                easeDistance = maxDistance;
            }
            return new int[] {(int) easeDistance, percentage};
        }*/
    }
}
