﻿using System;
using System.Drawing;
using System.Windows.Forms;
using Loccioni.Aulos;
using Loccioni.Aulos.Security.TransferModel;
using Loccioni.Aulos.Security.WebClient;

namespace WindowsUI.UI
{
    public partial class LoginPopup : Popup
    {

        //Parametri di test
        bool onetime = true;

        private SecurityClient securityClient;
        //private string Username { get; set; }

        public event LoggedUserEventHandler LoggedUser;

        public LoginPopup()
        {
            InitializeComponent();
        }

        public void BuildLoginPopup()
        {
            WebClientService webClientService = ServiceBroker.GetService<WebClientService>();
            securityClient = (SecurityClient)webClientService[typeof(SecurityClient)];

            if (MainForm.Height >= 1024)
            {
                body.Size = new Size(400, 300);
            }
            else
            {
                body.Size = new Size(360, 220);
            }

            usernameText.Text = "Username";
            usernameText.Width = 200; //Per attivare l'evento resize
            passwordText.Text = "Password";
            passwordText.Width = 200; //Per attivare l'evento resize
            passwordText.PasswordChar = '*';
            loginButton.Size = new Size(body.Width - 40, 40);

            this.Add();
        }

        private async void SubmitLogin(object sender, EventArgs e)
        {
            Session session = await securityClient.LogIn(usernameText.Text, passwordText.Text);
            // session = new Session() {Username = "ciao"};
            if (session == null)
            {
                if (onetime)
                {
                    NoPaddingLabel errorIcon = new NoPaddingLabel()
                    {
                        Text = FontAulos.Alarm,
                        Font = new Font("Aulos", 34, GraphicsUnit.Pixel),
                        Size = new Size(34, 34),
                        Location = new Point(20, loginButton.Location.Y + loginButton.Height + 20),
                    };
                    body.Controls.Add(errorIcon);

                    Label errorMessage = new Label()
                    {
                        Text = "There was a problem with your login. Please try again",
                        AutoSize = true,
                        MaximumSize = new Size(body.Width - 55 - 20, 50),
                        Location = new Point(55, loginButton.Location.Y + loginButton.Height + 20),
                        Font = new Font("Open Sans", 12, GraphicsUnit.Pixel)
                    };
                    body.Controls.Add(errorMessage);
                
                    onetime = false;
                    body.Size = new Size(body.Width, body.Height + errorMessage.Height + 20);
                }
                usernameText.ActivateError();
                passwordText.ActivateError();
            }
            else
            {
                LoggedUser?.Invoke(this, new LoggedUserEventArgs() {UserSession = session});
                this.CustomDispose();
            }
        }
    }
}
