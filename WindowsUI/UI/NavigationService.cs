﻿using WindowsUI.MainContent;

namespace WindowsUI.UI
{
    static class NavigationService
    {
        static ContentTape contentTape;

        public static void RegisterContentTape (ContentTape cT)
        {
            contentTape = cT;
        }

        public static void MoveContent(string containerId)
        {
            contentTape.MoveToContainer(containerId);
        }
    }
}
