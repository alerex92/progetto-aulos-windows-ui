﻿using System.Collections.Generic;

namespace WindowsUI.UI
{
    public class NavItem
    {
        public string label { get; set; }
        public string type { get; set; }
        public string icon { get; set; }
        public List<NavItem> items { get; set; }
        public string containerId { get; set; }
        public string path { get; set; }
        public ContainerOptions options { get; set; }
    }

    public class ContainerOptions
    {
        public string url { get; set; }
        public int width { get; set; }
    }

    public class NavItems
    {
        public List<NavItem> items { get; set; }
    }
}
