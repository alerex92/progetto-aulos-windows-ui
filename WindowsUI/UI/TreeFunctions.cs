﻿using System.Collections.Generic;

namespace WindowsUI.UI
{
    public static class TreeFunctions
    {
        static List<NavItem> flatList = new List<NavItem>();
        static List<NavItem> path = new List<NavItem>();
        private static bool found = false;

        public static List<NavItem> ToList(List<NavItem> itemTree)
        {
            for (int i = 0; i < itemTree.Count; i++)
            {
                if (itemTree[i].items != null)
                {
                    ToList(itemTree[i].items);
                }
                else
                {
                    itemTree[i].items = null;
                    flatList.Add(itemTree[i]);
                }
            }
            return flatList;
        }

        public static List<NavItem> Search(List<NavItem> itemTree, string containerId)
        {
            found = false;
            path.Clear();
            return SearchPath(itemTree, containerId);
        }
        private static List<NavItem> SearchPath(List<NavItem> itemTree, string containerId)
        {
            for (int i = 0; i < itemTree.Count && found == false; i++)
            {
                if (itemTree[i].containerId == containerId)
                {
                    //path.Add(itemTree[i]);
                    found = true;
                }
                else if (itemTree[i].items != null)
                {
                    SearchPath(itemTree[i].items, containerId);
                }
                if (found)
                {
                    path.Add(itemTree[i]);
                }
            }
            return path;
        }
    }
}

