﻿using System;
using System.Reflection;

namespace WindowsUI.UI
{
    //Nota : Transparency works over the container's background, but not over sibling controls.
    //Cioè la trasparenza prenderà il colore del container, quindi attenzione a dove aggiungo le icone, 
    //Al massimo toccherà fare dei workaround in caso ho bisogno di questa feature
    
    //Bisogna usare tutte le set per configurare bene il font
    static class FontAulos
    {
        //private static float dpi = 96; //96 sembra essere il default value della maggiorparte degli schermi
        //public static float Dpi { set { dpi = value; } }

        /*private static FontFamily textFont;
        private static FontFamily iconFont;*/
        private static Type myType;

        static FontAulos()
        {
            /*PrivateFontCollection openSans = new PrivateFontCollection();
            openSans.AddFontFile(@"Resources\OpenSans.ttf");
            textFont = openSans.Families[0];

            PrivateFontCollection aulosFont = new PrivateFontCollection();
            aulosFont.AddFontFile(@"Resources\Aulos.ttf");
            iconFont = aulosFont.Families[0];*/

            myType = typeof(FontAulos);
        }

        /*public static void SetTextFont()
        {
            PrivateFontCollection openSans = new PrivateFontCollection();
            openSans.AddFontFile(@"Resources\OpenSans.ttf");
            textFont = openSans.Families[0];
        }*/

        /*public static FontFamily GetTextFont()
        {
            return textFont;
        }*/

        /*public static void SetIconFont()
        {
            PrivateFontCollection aulosFont = new PrivateFontCollection();
            aulosFont.AddFontFile(@"Resources\Aulos.ttf");
            iconFont = aulosFont.Families[0];
        }*/

        /*public static FontFamily GetIconFont()
        {
            return iconFont;
        }*/


        /*public static float PxToPt(int pixel) Non serve perchè ho visto che si può impostare tutto direttamente in px
        {     
            return (float)pixel * 72 / dpi;
        }*/

        private static string UnicodeToChar(string hex)
        {
            int code = int.Parse(hex, System.Globalization.NumberStyles.HexNumber);
            string unicodeString = char.ConvertFromUtf32(code);
            return unicodeString;
        }        

        public static string GetIconByName(string name)
        {
            name = name.Substring(0, 1).ToUpper() + name.Substring(1);
            PropertyInfo myPropInfo = myType.GetProperty(name);
            return (string) myPropInfo.GetValue(null, null);
        }

        public static string AlarmBaloon        { get { return UnicodeToChar("E001"); } }
        public static string Automatic          { get { return UnicodeToChar("E002"); } }
        public static string Diagnostic         { get { return UnicodeToChar("E003"); } }
        public static string Login              { get { return UnicodeToChar("E004"); } }
        public static string Manual             { get { return UnicodeToChar("E005"); } }
        public static string Monitoring         { get { return UnicodeToChar("E006"); } }
        public static string WarningBaloon      { get { return UnicodeToChar("E007"); } }
        public static string Settings           { get { return UnicodeToChar("E008"); } }
        public static string Alarm              { get { return UnicodeToChar("E009"); } }
        public static string Sort               { get { return UnicodeToChar("E010"); } }
        public static string SortDesc           { get { return UnicodeToChar("E011"); } }
        public static string SortAsc            { get { return UnicodeToChar("E012"); } }
        public static string Pin                { get { return UnicodeToChar("E013"); } }
        public static string ComboboxDown       { get { return UnicodeToChar("E014"); } }
        public static string ComboboxUp         { get { return UnicodeToChar("E015"); } }
        public static string Search             { get { return UnicodeToChar("E016"); } }
        public static string Delete             { get { return UnicodeToChar("E017"); } }
        public static string Add                { get { return UnicodeToChar("E018"); } }
        public static string DigitalHigh        { get { return UnicodeToChar("E019"); } }
        public static string DigitalLow         { get { return UnicodeToChar("E020"); } }
        public static string Edit               { get { return UnicodeToChar("E021"); } }
        public static string Plus               { get { return UnicodeToChar("E022"); } }
        public static string Denied             { get { return UnicodeToChar("E023"); } }
        public static string List               { get { return UnicodeToChar("E024"); } }
        public static string Copy               { get { return UnicodeToChar("E025"); } }
        public static string Cut                { get { return UnicodeToChar("E026"); } }
        public static string Paste              { get { return UnicodeToChar("E027"); } }
        public static string Attention          { get { return UnicodeToChar("E028"); } }
        public static string Up                 { get { return UnicodeToChar("E029"); } }
        public static string Down               { get { return UnicodeToChar("E030"); } }
        public static string Export             { get { return UnicodeToChar("E031"); } }
        public static string DeleteProgram      { get { return UnicodeToChar("E032"); } }
        public static string LoadProgram        { get { return UnicodeToChar("E033"); } }
        public static string NewProgram         { get { return UnicodeToChar("E034"); } }
        public static string SaveProgram        { get { return UnicodeToChar("E035"); } }
        public static string Play               { get { return UnicodeToChar("E036"); } }
        public static string Stop               { get { return UnicodeToChar("E037"); } }
        public static string OpenEnvironment    { get { return UnicodeToChar("E038"); } }
        public static string ArrowLeft          { get { return UnicodeToChar("E039"); } }
        public static string ArrowRight         { get { return UnicodeToChar("E040"); } }
        public static string LinkBot            { get { return UnicodeToChar("E041"); } }
        public static string LinkPub            { get { return UnicodeToChar("E042"); } }
        public static string LinkPubsub         { get { return UnicodeToChar("E043"); } }
        public static string LinkSub            { get { return UnicodeToChar("E044"); } }
        public static string LinkTop            { get { return UnicodeToChar("E045"); } }
        public static string DeleteEnvironment              { get { return UnicodeToChar("E046"); } }
        public static string DebugInstructionExecuted       { get { return UnicodeToChar("E047"); } }
        public static string DebugInstructionNotExecuted    { get { return UnicodeToChar("E048"); } }
        public static string DebugInstructionInExecution    { get { return UnicodeToChar("E049"); } }
        public static string DebugBreakpointExecuted        { get { return UnicodeToChar("E050"); } }
        public static string DebugBreakpointNotExecuted     { get { return UnicodeToChar("E051"); } }
        public static string DebugBreakpointInStop          { get { return UnicodeToChar("E052"); } }
        public static string DebugBreakpointInExecution     { get { return UnicodeToChar("E053"); } }
        public static string GraphicSelection               { get { return UnicodeToChar("E054"); } }
        public static string GraphicSelectionAdapt          { get { return UnicodeToChar("E055"); } }
        public static string GraphicSelectionHorizontal     { get { return UnicodeToChar("E056"); } }
        public static string GraphicSelectionVertical       { get { return UnicodeToChar("E057"); } }
        public static string DebugInstructionStart          { get { return UnicodeToChar("E058"); } }
        public static string DebugInstructionStop           { get { return UnicodeToChar("E059"); } }
        public static string ZoomIn             { get { return UnicodeToChar("E060"); } }
        public static string ZoomOut            { get { return UnicodeToChar("E061"); } }
        public static string LinkToLeft         { get { return UnicodeToChar("E062"); } }
        public static string NolinkToRight      { get { return UnicodeToChar("E063"); } }
        public static string LinkToRight        { get { return UnicodeToChar("E064"); } }
        public static string NolinkToLeft       { get { return UnicodeToChar("E065"); } }
        public static string MoreLinkUp         { get { return UnicodeToChar("E067"); } }
        public static string MoreLinkDown       { get { return UnicodeToChar("E068"); } }
        public static string Sub                { get { return UnicodeToChar("E069"); } }
        public static string Pub                { get { return UnicodeToChar("E070"); } }
        public static string DebugProgramStop   { get { return UnicodeToChar("E071"); } }
        public static string DebugProgramStart  { get { return UnicodeToChar("E072"); } }
    }
}
