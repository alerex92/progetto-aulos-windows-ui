﻿using System.Windows.Forms;

namespace WindowsUI.UI
{
    public partial class NoPaddingLabel : Control
    {
        private TextFormatFlags flags = TextFormatFlags.SingleLine | TextFormatFlags.VerticalCenter | TextFormatFlags.HorizontalCenter | TextFormatFlags.NoPadding;

        public NoPaddingLabel()
        {
            SetStyle(ControlStyles.UserPaint, true);
            SetStyle(ControlStyles.SupportsTransparentBackColor, true);
            SetStyle(ControlStyles.OptimizedDoubleBuffer, true);
            InitializeComponent();
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            TextRenderer.DrawText(e.Graphics, this.Text, this.Font, ClientRectangle, this.ForeColor, this.BackColor, flags);
            base.OnPaint(e); //Il base onpaint dopo perchè se no disegna l'icona sopra il cerchio
        }
    }
}
