﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace WindowsUI.UI
{
    public partial class CustomTextBox : UserControl
    {
        [Description("Text displayed in the textbox"), Category("Aulos")]
        public string PlaceHolder { get; set; }

        [Description("Text displayed in the textbox"), Category("Aulos")]
        public char PasswordChar { get; set; }

        public override string Text { get { return textBox.Text; } set { textBox.Text = value; } }

        bool userText = false;
        private bool error = false;

        public CustomTextBox()
        {
            InitializeComponent();
        }

        private void textBox_Enter(object sender, EventArgs e)
        {
            if (userText == false)
            {
                textBox.ForeColor = Color.Black;
                this.BackColor = Color.Black;
                textBox.Text = "";
                if(PasswordChar != 0)
                {
                    textBox.PasswordChar = PasswordChar;
                }
            }
            
        }

        private void textBox_Leave(object sender, EventArgs e)
        {
            if(textBox.Text == "")
            {
                textBox.ForeColor = ColorTranslator.FromHtml("#B3B3B3");
                this.BackColor = ColorTranslator.FromHtml("#B3B3B3");
                textBox.Text = PlaceHolder;
                if (PasswordChar != 0)
                {
                    textBox.PasswordChar = '\0';
                }
                userText = false;
            }
            else
            {
                userText = true;
            }
        }

        public void ActivateError ()
        {
            textBox.ForeColor = Color.Red;
            this.BackColor = Color.Red;
            userText = true;
            error = true;
        }

        private void CustomTextBox_Load(object sender, EventArgs e)
        {
            textBox.Font = this.Font;
            textBox.ForeColor = ColorTranslator.FromHtml("#B3B3B3");
            this.BackColor = ColorTranslator.FromHtml("#B3B3B3");
            textBox.Text = PlaceHolder;
        }

        private void textBox_TextChanged(object sender, EventArgs e)
        {
            if (error)
            {
                error = false;
                textBox.ForeColor = Color.Black;
                this.BackColor = Color.Black;
            }    
        }
    }
}
