﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsUI.UI
{
    public partial class SplashForm : Form
    {
        public bool LoginRequired { get; set; }

        public event EventHandler SplashLoaded;
        public event LoggedUserEventHandler LoggedUser;

        public SplashForm()
        {
            InitializeComponent();
        }

        private void SplashForm_Load(object sender, EventArgs e)
        {
            splashScreen.LoginRequired = LoginRequired;
            splashScreen.Build();
            splashScreen.SplashLoaded += BubbleSplashLoaded;
            splashScreen.LoggedUser += BubbleLoggedUser;
        }

        private void BubbleLoggedUser(object sender, LoggedUserEventArgs e)
        {
            LoggedUser?.Invoke(sender, e);
        }

        private void BubbleSplashLoaded(object sender, EventArgs e)
        {
            SplashLoaded?.Invoke(sender, e);
        }
    }
}
