﻿namespace WindowsUI.UI
{
    partial class LoginPopup
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.popupTitle = new System.Windows.Forms.Label();
            this.usernameText = new CustomTextBox();
            this.passwordText = new CustomTextBox();
            this.loginButton = new System.Windows.Forms.Button();
            this.body.SuspendLayout();
            this.SuspendLayout();
            // 
            // body
            // 
            this.body.Controls.Add(this.loginButton);
            this.body.Controls.Add(this.passwordText);
            this.body.Controls.Add(this.usernameText);
            this.body.Controls.Add(this.popupTitle);
            this.body.Location = new System.Drawing.Point(9, 9);
            this.body.Size = new System.Drawing.Size(360, 220);
            // 
            // popupTitle
            // 
            this.popupTitle.AutoSize = true;
            this.popupTitle.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.popupTitle.Location = new System.Drawing.Point(20, 20);
            this.popupTitle.Name = "popupTitle";
            this.popupTitle.Size = new System.Drawing.Size(149, 19);
            this.popupTitle.TabIndex = 0;
            this.popupTitle.Text = "LOGIN IN TO AULOS";
            // 
            // usernameText
            // 
            this.usernameText.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(179)))), ((int)(((byte)(179)))), ((int)(((byte)(179)))));
            this.usernameText.Font = new System.Drawing.Font("Open Sans", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.usernameText.Location = new System.Drawing.Point(20, 59);
            this.usernameText.Name = "usernameText";
            this.usernameText.PasswordChar = '\0';
            this.usernameText.PlaceHolder = "Username";
            this.usernameText.Size = new System.Drawing.Size(200, 19);
            this.usernameText.TabIndex = 1;
            // 
            // passwordText
            // 
            this.passwordText.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(179)))), ((int)(((byte)(179)))), ((int)(((byte)(179)))));
            this.passwordText.Font = new System.Drawing.Font("Open Sans", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.passwordText.Location = new System.Drawing.Point(20, 98);
            this.passwordText.Name = "passwordText";
            this.passwordText.PasswordChar = '*';
            this.passwordText.PlaceHolder = "Password";
            this.passwordText.Size = new System.Drawing.Size(200, 19);
            this.passwordText.TabIndex = 2;
            // 
            // loginButton
            // 
            this.loginButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(176)))), ((int)(((byte)(207)))), ((int)(((byte)(254)))));
            this.loginButton.FlatAppearance.BorderSize = 0;
            this.loginButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(102)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
            this.loginButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(176)))), ((int)(((byte)(207)))), ((int)(((byte)(254)))));
            this.loginButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.loginButton.Location = new System.Drawing.Point(20, 147);
            this.loginButton.Name = "loginButton";
            this.loginButton.Size = new System.Drawing.Size(320, 40);
            this.loginButton.TabIndex = 3;
            this.loginButton.Text = "Login";
            this.loginButton.UseVisualStyleBackColor = false;
            this.loginButton.Click += new System.EventHandler(this.SubmitLogin);
            // 
            // LoginPopup
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.MaximumSize = new System.Drawing.Size(0, 0);
            this.Name = "LoginPopup";
            this.Size = new System.Drawing.Size(378, 238);
            this.body.ResumeLayout(false);
            this.body.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label popupTitle;
        private CustomTextBox usernameText;
        private CustomTextBox passwordText;
        private System.Windows.Forms.Button loginButton;
    }
}
