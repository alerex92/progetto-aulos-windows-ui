﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsUI.UI
{
    public class ConnectionParams
    {
        public string baseUrl { get; set; }
        public int urlPort { get; set; }
        public int urlWsPort { get; set; }
        public string baseWebService { get; set; }
    }
    
    public class GlobalConfiguration
    {
        public string MenuItemsFile { get; set; }
        public ConnectionParams ConnectionParameters { get; set; }
        public bool ShutdownServer { get; set; }
        public bool LoginRequired { get; set; }
        public bool SplashRequired { get; set; }
        public bool SmallScreens { get; set; }
        public bool NotificationSetup { get; set; }
        public string BenchName { get; set; }
    }
}
