﻿using WindowsUI.Footer;
using WindowsUI.Header;
using WindowsUI.MainContent;

namespace WindowsUI.UI
{
    partial class MainLayoutSmall
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.contentTape = new WindowsUI.MainContent.ContentTape();
            this.notificationBar = new WindowsUI.Header.NotificationCenter();
            this.footer = new WindowsUI.Footer.FooterUCSmall();
            this.SuspendLayout();
            // 
            // contentTape
            // 
            this.contentTape.BackColor = System.Drawing.Color.White;
            this.contentTape.Dock = System.Windows.Forms.DockStyle.Fill;
            this.contentTape.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.contentTape.Location = new System.Drawing.Point(0, 40);
            this.contentTape.Margin = new System.Windows.Forms.Padding(0);
            this.contentTape.Name = "contentTape";
            this.contentTape.Padding = new System.Windows.Forms.Padding(0, 20, 20, 20);
            this.contentTape.Size = new System.Drawing.Size(800, 360);
            this.contentTape.TabIndex = 18;
            // 
            // notificationBar
            // 
            this.notificationBar.BackColor = System.Drawing.Color.Transparent;
            this.notificationBar.Dock = System.Windows.Forms.DockStyle.Top;
            this.notificationBar.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.notificationBar.Location = new System.Drawing.Point(0, 0);
            this.notificationBar.Margin = new System.Windows.Forms.Padding(0);
            this.notificationBar.Name = "notificationBar";
            this.notificationBar.Size = new System.Drawing.Size(800, 40);
            this.notificationBar.TabIndex = 20;
            // 
            // footer
            // 
            this.footer.AppName = "Nome del banco";
            this.footer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.footer.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.footer.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.footer.FormHeight = 0;
            this.footer.Location = new System.Drawing.Point(0, 400);
            this.footer.Margin = new System.Windows.Forms.Padding(0);
            this.footer.Name = "footer";
            this.footer.Size = new System.Drawing.Size(800, 80);
            this.footer.TabIndex = 19;
            this.footer.Username = null;
            // 
            // MainLayoutSmall
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(800, 480);
            this.Controls.Add(this.contentTape);
            this.Controls.Add(this.notificationBar);
            this.Controls.Add(this.footer);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "MainLayoutSmall";
            this.Text = "MainLayoutSmall";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MainLayout_FormClosed);
            this.Load += new System.EventHandler(this.WindowsUI_Load);
            this.ResumeLayout(false);

        }

        #endregion
        private ContentTape contentTape;
        private FooterUCSmall footer;
        private NotificationCenter notificationBar;
    }
}

