﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using WindowsUI.MainContent;
using Loccioni.Aulos;
using Loccioni.Aulos.Configuration.WebClient;

namespace WindowsUI.UI
{
    public partial class MainLayoutSmall : Form
    {
        private SplashScreen splashScreen;

        public GlobalConfiguration Configuration { get; set; }

        int elementsLoaded = 0;

        public MainLayoutSmall()
        {
            InitializeComponent();

            this.StartPosition = FormStartPosition.Manual;
            this.Location = new Point(0, 0);

            this.SetStyle(ControlStyles.OptimizedDoubleBuffer, true);
            this.SetStyle(ControlStyles.AllPaintingInWmPaint, true);

            //this.WindowState = FormWindowState.Maximized;

            //configuration = JsonConvert.DeserializeObject<GlobalConfiguration>(File.ReadAllText(@"Configuration\globalConfiguration.json"));

            //urlParam = new Dictionary<string, string>();

            /*urlParam.Add("baseUrl", "localhost");
            urlParam.Add("urlPort", "3000");
            urlParam.Add("urlWsPort", "3001");
            urlParam.Add("baseWebService", "jsonServer");

            urlParam.Add("baseUrl", "aulos-pc02");
            urlParam.Add("urlPort", "9000");
            urlParam.Add("urlWsPort", "9001");
            urlParam.Add("baseWebService", "webservice");

            urlParam.Add("baseUrl", "172.17.83.2");
            urlParam.Add("urlPort", "9000");
            urlParam.Add("urlWsPort", "9001");
            urlParam.Add("baseWebService", "webservice");
            
            "baseUrl": "aulos-pc02",
            "urlPort": 9000,
            "urlWsPort": 9001,
            "baseWebService": "webservice"

            "baseUrl": "localhost",
            "urlPort": 3000,
            "urlWsPort": 3001,
            "baseWebService": "jsonServer"*/
        }

        private void WindowsUI_Load(object sender, EventArgs e)
        {
            //Inizializzo il Font
            /*Graphics g = this.CreateGraphics();
            FontAulos.Dpi = g.DpiX;
            g.Dispose();
            FontAulos.SetTextFont();
            FontAulos.SetIconFont();*/

            //Configuro la notification bar
            /*NotificationCenter notificationBar = new NotificationCenter();
            notificationBar.FormHeight = screenHeight;
            notificationBar.Dock = DockStyle.Top;
            
            notificationBar.Location = new Point(0, 0);
            notificationBar.FormHeight = screenHeight;
            notificationBar.FormWidth = screenWidth;
            notificationBar.UrlParam = urlParam;*/


            //TEST POPUP
            /*Popup loginPopup = new Popup();
            loginPopup.Location = new Point(10, 100);
            loginPopup.FormHeight = screenHeight;
            loginPopup.BuildLoginPopup();
            this.Controls.Add(loginPopup);

            Popup activeAlert = new Popup();
            activeAlert.Location = new Point(500, 100);
            activeAlert.FormHeight = screenHeight;
            string activeTitle = "RESTART";
            string activeText = "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor.";
            activeAlert.BuildActiveAlert(activeTitle, activeText);
            this.Controls.Add(activeAlert);

            Popup passiveAlert = new Popup();
            passiveAlert.Location = new Point(500, 400);
            passiveAlert.FormHeight = screenHeight;
            string passiveTitle = "SERVER RESTART";
            string passiveText = "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor.";
            passiveAlert.BuildPassiveAlert(passiveTitle, passiveText);
            this.Controls.Add(passiveAlert);*/

            notificationBar.FormHeight = this.Height;
            //notificationBar.FormWidth = screenWidth;
            //notificationBar.UrlParam = urlParam;
            notificationBar.Configuration = Configuration;

            NavItems navigationMenu = JsonConvert.DeserializeObject<NavItems>(File.ReadAllText(Configuration.MenuItemsFile));

            footer.FormHeight = this.Height;
            //footer.FormWidth = screenWidth;
            //footer.UrlParam = urlParam;
            footer.Configuration = Configuration;
            footer.NavigationMenu = navigationMenu;
            footer.MenuLoaded += ElementsLoaded;
            footer.UserLogout += UserLogout;

            //contentTape.UrlParam = urlParam;
            contentTape.Configuration = Configuration;
            contentTape.NavigationMenu = navigationMenu;
            contentTape.ContainerLoaded += ElementsLoaded;
            contentTape.VisibleContainersChanged += VisibleContainersChanged;

            if (Configuration.SplashRequired)
            {
                splashScreen = new SplashScreen() { Size = this.Size };
                splashScreen.LoginRequired = Configuration.LoginRequired;
                splashScreen.Build();
                this.Controls.Add(splashScreen);
                splashScreen.BringToFront();
                splashScreen.LoggedUser += LoggedUser;
            }
            if (!Configuration.LoginRequired)
            {
                if (Configuration.NotificationSetup)
                {
                    notificationBar.Setup();
                }
                footer.Setup();
                contentTape.Setup();
            }
        }

        private void UserLogout(object sender, EventArgs e)
        {
            splashScreen = new SplashScreen() { Size = this.Size };
            splashScreen.Build();
            this.Controls.Add(splashScreen);
            splashScreen.BringToFront();
        }

        private void LoggedUser(object sender, LoggedUserEventArgs e)
        {
            if (Configuration.NotificationSetup)
            {
                notificationBar.Setup();
            }
            footer.Username = e.UserSession.Username;
            footer.Setup();
            contentTape.Setup();
        }

        private void VisibleContainersChanged(object sender, VisibleContainersChangedEventArgs e)
        {
            footer.ActivateMenu(e.CentralContainer);
            footer.ChangeBar(e.ContainerVisiblePercentage);
        }

        private void ElementsLoaded(object sender, EventArgs e)
        {
            elementsLoaded++;
            if(elementsLoaded == 2)
            {
                if (!Configuration.LoginRequired && Configuration.SplashRequired)
                {
                    splashScreen.Dispose();
                }
                contentTape.ContainersOnScreen(true);
                footer.FirstActive();
            }
        }

        private async void MainLayout_FormClosed(object sender, FormClosedEventArgs e)
        {
            //System.Diagnostics.Debug.WriteLine(typeof(WebContainer).IsSubclassOf(typeof(Container)));
            if (Configuration.ShutdownServer)
            {
                WebClientService webClientService = ServiceBroker.GetService<WebClientService>();
                await ((SystemClient) webClientService[typeof(SystemClient)]).Shutdown();
            }
        }
    }
}
