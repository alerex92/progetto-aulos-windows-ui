﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsUI.Properties;

namespace WindowsUI.UI
{
    public partial class SplashScreen : UserControl
    {
        public event LoggedUserEventHandler LoggedUser;
        public event EventHandler SplashLoaded;

        public bool LoginRequired { private get; set; }

        private Size logoAulosSize = new Size(360, 166);
        private Size logoLoccioniSize = new Size(320, 42);
        private bool firstTime = true;
        

        public SplashScreen()
        {
            InitializeComponent();
            loginIcon.Text = FontAulos.Login;
            loginIcon.Click += BuildPopup;
            loginPanel.Click += BuildPopup;
            loginLabel.Click += BuildPopup;
        }

        private void BuildPopup(object sender, EventArgs e)
        {
            LoginPopup loginPopup = new LoginPopup {MainForm = this.ParentForm};
            loginPopup.BuildLoginPopup();
            loginPopup.LoggedUser += LoggedUserToParent;
        }

        private void LoggedUserToParent(object sender, LoggedUserEventArgs e)
        {
            LoggedUser?.Invoke(this, e);
            this.Dispose();
        }

        public void Build()
        {
            if (this.Height >= 1024)
            {
                logoAulosSize = new Size(400, 184);
            }
            //logoAulos.Location = new Point((this.Width - logoAulos.Width)/2, (this.Height - logoAulos.Height) / 2);
            //logoLoccioni.Location = new Point((this.Width - logoLoccioni.Width) / 2, logoAulos.Location.Y + logoAulos.Height + 100);

            loginPanel.Visible = LoginRequired;
        }

        private void SplashScreen_Paint(object sender, PaintEventArgs e)
        {
            using (Bitmap image = new Bitmap(Resources.logoAulos))
            {
                Rectangle rect = new Rectangle(new Point((this.Width - logoAulosSize.Width) / 2, (this.Height - logoAulosSize.Height) / 2), logoAulosSize);
                e.Graphics.DrawImage(image, rect);
            }
            using (Bitmap image = new Bitmap(Resources.loccioni))
            {
                Rectangle rect = new Rectangle(new Point((this.Width - logoLoccioniSize.Width) / 2, this.Height - logoLoccioniSize.Height - 30), logoLoccioniSize);
                e.Graphics.DrawImage(image, rect);
            }
            if (firstTime)
            {
                SplashLoaded?.Invoke(this, EventArgs.Empty);
                firstTime = false;
            }
        }
    }
}
