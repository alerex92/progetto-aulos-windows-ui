﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsUI.UI
{
    public partial class ProgressPopup : Popup
    {
        public ProgressPopup()
        {
            InitializeComponent();
        }

        /*private void backgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            for (int i = 1; i <= 100; i++)
            {
                // Wait 100 milliseconds.
                Thread.Sleep(100);
                // Report progress.
                backgroundWorker.ReportProgress(i);
            }
        }

        private void backgroundWorker_ProgressChanged(object sender,
        ProgressChangedEventArgs e)
        {
            // Change the value of the ProgressBar to the BackgroundWorker progress.
            progressBar.Value = e.ProgressPercentage;
            // Set the text.
            this.Text = e.ProgressPercentage.ToString();
        }*/

        public void BuildProgressPopup(string title, string message)
        {
            /*backgroundWorker = new BackgroundWorker();
            backgroundWorker.WorkerReportsProgress = true;
            backgroundWorker.DoWork += new DoWorkEventHandler(backgroundWorker_DoWork);
            backgroundWorker.ProgressChanged += new ProgressChangedEventHandler(backgroundWorker_ProgressChanged);
            backgroundWorker.RunWorkerAsync();*/

            if (MainForm.Height >= 1024)
            {
                body.Size = new Size(400, 140);
            }
            else
            {
                body.Size = new Size(360, 140);
            }
            popupTitle.Text = title;
            popupTitle.Location = new Point((body.Width - popupTitle.Width) / 2, 20);

            popupMessage.Text = message;
            popupMessage.MaximumSize = new Size(body.Width - 40, 50);

            progressBar.Width = body.Width - 40;
            progressBar.Location = new Point(20, body.Height - 30);
            progressBar.Value = 50;
            progressBar.Text = "50";

            this.Add();
        }

        public void SetProgress(int progress)
        {
            progressBar.Value = progress;
            progressBar.Text = progress.ToString();
        }
    }
}
