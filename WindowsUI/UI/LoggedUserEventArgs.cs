﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Loccioni.Aulos.Security.TransferModel;

namespace WindowsUI.UI
{
    public class LoggedUserEventArgs : EventArgs
    {
        public Session UserSession { get; set; }
    }
    public delegate void LoggedUserEventHandler(object sender, LoggedUserEventArgs e);
}