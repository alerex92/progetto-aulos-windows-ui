﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Threading;
using System.Windows.Forms;
using WindowsUI.MainContent;
using Loccioni.Aulos;
using Loccioni.Aulos.Configuration.WebClient;

namespace WindowsUI.UI
{
    public partial class MainLayout : Form
    {
        //private SplashScreen splashScreen;
        private SplashForm splashScreen;
        private bool logged;
        private bool setuped;
        int elementsLoaded = 0;

        public GlobalConfiguration Configuration { get; set; }
        
        public MainLayout()
        {
            InitializeComponent();

            this.StartPosition = FormStartPosition.Manual;
            this.Location = new Point(0, 0);

            this.SetStyle(ControlStyles.OptimizedDoubleBuffer, true);
            this.SetStyle(ControlStyles.AllPaintingInWmPaint, true);

            //this.Size = new Size(1366, 768);
            this.WindowState = FormWindowState.Maximized;
            
            /*"baseUrl": "aulos-pc02",
            "urlPort": 9000,
            "urlWsPort": 9001,
            "baseWebService": "webservice"

            "baseUrl": "localhost",
            "urlPort": 3000,
            "urlWsPort": 3001,
            "baseWebService": "jsonServer"*/
        }

        private void WindowsUI_Load(object sender, EventArgs e)
        {
            notificationBar.FormHeight = this.Height;
            notificationBar.Configuration = Configuration;

            NavItems navigationMenu = JsonConvert.DeserializeObject<NavItems>(File.ReadAllText(Configuration.MenuItemsFile));

            footer.FormHeight = this.Height;
            footer.Configuration = Configuration;
            footer.NavigationMenu = navigationMenu;
            footer.MenuLoaded += ElementsLoaded;
            footer.UserLogout += UserLogout;

            contentTape.Configuration = Configuration;
            contentTape.NavigationMenu = navigationMenu;
            contentTape.ContainerLoaded += ElementsLoaded;
            contentTape.VisibleContainersChanged += VisibleContainersChanged;

            if (Configuration.SplashRequired)
            {
                splashScreen = new SplashForm()
                {
                    Owner = this,
                    Size = this.Size,
                    LoginRequired = Configuration.LoginRequired,
                };
                splashScreen.SplashLoaded += SplashScreenLoaded;
                splashScreen.LoggedUser += LoggedUser;
                splashScreen.Show();
            }
            else
            {
                Setup();
            }
        }

        private void Setup()
        {
            if (!setuped)
            {
                if (Configuration.NotificationSetup)
                {
                    notificationBar.Setup();
                }
                footer.Setup();
                contentTape.Setup();
            }
        }

        private void SplashScreenLoaded(object sender, EventArgs e)
        {
            if (!Configuration.LoginRequired)
            {
                Setup();
            }
        }

        private void UserLogout(object sender, EventArgs e)
        {
            WebClientService webClientService = ServiceBroker.GetService<WebClientService>();
            //SecurityTokenNotYetValidException+warningsClient = (WarningsClient)webClientService[typeof(WarningsClient)];
            logged = false;
            splashScreen = new SplashForm()
            {
                Owner = this,
                Size = this.Size,
                LoginRequired = Configuration.LoginRequired,
            };
            splashScreen.SplashLoaded += SplashScreenLoaded;
            splashScreen.LoggedUser += LoggedUser;
            splashScreen.Show();
            
        }

        private void LoggedUser(object sender, LoggedUserEventArgs e)
        {
            logged = true;
            footer.Username = e.UserSession.Username;
            Setup();
        }

        private void VisibleContainersChanged(object sender, VisibleContainersChangedEventArgs e)
        {
            footer.ActivateMenu(e.CentralContainer);
            footer.ChangeBar(e.ContainerVisiblePercentage);
        }

        private void ElementsLoaded(object sender, EventArgs e)
        {
            elementsLoaded++;
            if(elementsLoaded == 2)
            {
                if (Configuration.SplashRequired)
                {
                    splashScreen.Close();
                    splashScreen.Dispose();
                }
                contentTape.ContainersOnScreen(true);
                footer.FirstActive();
            }
        }

        private async void MainLayout_FormClosed(object sender, FormClosedEventArgs e)
        {
            //System.Diagnostics.Debug.WriteLine(typeof(WebContainer).IsSubclassOf(typeof(Container)));
            if (Configuration.ShutdownServer)
            {
                WebClientService webClientService = ServiceBroker.GetService<WebClientService>();
                await ((SystemClient) webClientService[typeof(SystemClient)]).Shutdown();
            }
        }
    }
}
