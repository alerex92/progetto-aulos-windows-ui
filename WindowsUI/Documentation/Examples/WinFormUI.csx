﻿#load "..\references.csx"

#r "System.Drawing"
#r "System.IO"
#r "System.Windows.Forms"

using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using System.Threading;
using System.Threading.Tasks;
using Loccioni.Aulos;
using Loccioni.Aulos.Configuration;
using Loccioni.AulosProject;
using WindowsUI.UI;

public class WinFormLocalUICatalog : Catalog
{
    public WinFormLocalUICatalog(string name, string filename) : base(name, filename)
    {
    }

    public override void Build()
    {
        Thread thread = new Thread(CreateAndShow);
        thread.SetApartmentState(ApartmentState.STA);
        thread.IsBackground = true;
        thread.Start();
    }

    private void CreateAndShow()
    {
        dynamic webServerCatalog = null;
        if (ServiceBroker.GetService<CatalogsService>().ContainsKey("WebServer"))
            webServerCatalog = ServiceBroker.GetService<CatalogsService>()["WebServer"];
        
        GlobalConfiguration configuration = JsonConvert.DeserializeObject<GlobalConfiguration>(File.ReadAllText(@"Configuration\globalConfiguration.json"));
        WebClientService webClientService = new WebClientService();
        webClientService.Add(typeof(AlarmsClient), new AlarmsClient("http://" + configuration.ConnectionParameters.baseUrl + ":" + configuration.ConnectionParameters.urlPort + "/" + configuration.ConnectionParameters.baseWebService + "/", "ws://" + configuration.ConnectionParameters.baseUrl + ":" + configuration.ConnectionParameters.urlWsPort + "/" + configuration.ConnectionParameters.baseWebService + "/messages/alarms/subscriptions"));
        webClientService.Add(typeof(WarningsClient), new WarningsClient("http://" + configuration.ConnectionParameters.baseUrl + ":" + configuration.ConnectionParameters.urlPort + "/" + configuration.ConnectionParameters.baseWebService + "/", "ws://" + configuration.ConnectionParameters.baseUrl + ":" + configuration.ConnectionParameters.urlWsPort + "/" + configuration.ConnectionParameters.baseWebService + "/messages/warnings/subscriptions"));
        webClientService.Add(typeof(SystemClient), new SystemClient("http://" + configuration.ConnectionParameters.baseUrl + ":" + configuration.ConnectionParameters.urlPort + "/" + configuration.ConnectionParameters.baseWebService + "/"));
        webClientService.Add(typeof(SecurityClient), new SecurityClient("http://" + configuration.ConnectionParameters.baseUrl + ":" + configuration.ConnectionParameters.urlPort + "/" + configuration.ConnectionParameters.baseWebService + "/", "ws://" + configuration.ConnectionParameters.baseUrl + ":" + configuration.ConnectionParameters.urlWsPort + "/" + configuration.ConnectionParameters.baseWebService + "/security/subscriptions"));

        ServiceBroker.RegistService<WebClientService, WebClientService>(webClientService);

        if (configuration.SmallScreens)
        {
            Application.Run(new MainLayoutSmall() { Configuration = configuration });
        }
        else
        {
            Application.Run(new MainLayout() { Configuration = configuration });
        }

        if (CfxRuntime.LibrariesLoaded)
        {
            CfxRuntime.Shutdown();
        }
    }

    public override void Destroy()
    {
        if (!winForm.IsDisposed)
            winForm.Invoke((MethodInvoker)delegate () { winForm.Close(); });
    }
}