﻿using Chromium;
using System;
using System.IO;
using System.Windows.Forms;
using WindowsUI.UI;
using Loccioni.Aulos;
using Loccioni.Aulos.Configuration.WebClient;
using Loccioni.Aulos.Messages.WebClient;
using Loccioni.Aulos.Security.WebClient;
using Newtonsoft.Json;

namespace WindowsUI
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            GlobalConfiguration configuration = JsonConvert.DeserializeObject<GlobalConfiguration>(File.ReadAllText(@"Configuration\globalConfiguration.json"));
            WebClientService webClientService = new WebClientService();
            webClientService.Add(typeof(AlarmsClient), new AlarmsClient("http://" + configuration.ConnectionParameters.baseUrl + ":" + configuration.ConnectionParameters.urlPort + "/" + configuration.ConnectionParameters.baseWebService + "/", "ws://" + configuration.ConnectionParameters.baseUrl + ":" + configuration.ConnectionParameters.urlWsPort + "/" + configuration.ConnectionParameters.baseWebService + "/messages/alarms/subscriptions"));
            webClientService.Add(typeof(WarningsClient), new WarningsClient("http://" + configuration.ConnectionParameters.baseUrl + ":" + configuration.ConnectionParameters.urlPort + "/" + configuration.ConnectionParameters.baseWebService + "/", "ws://" + configuration.ConnectionParameters.baseUrl + ":" + configuration.ConnectionParameters.urlWsPort + "/" + configuration.ConnectionParameters.baseWebService + "/messages/warnings/subscriptions"));
            webClientService.Add(typeof(SystemClient), new SystemClient("http://" + configuration.ConnectionParameters.baseUrl + ":" + configuration.ConnectionParameters.urlPort + "/" + configuration.ConnectionParameters.baseWebService + "/"));
            webClientService.Add(typeof(SecurityClient), new SecurityClient("http://" + configuration.ConnectionParameters.baseUrl + ":" + configuration.ConnectionParameters.urlPort + "/" + configuration.ConnectionParameters.baseWebService + "/", "ws://" + configuration.ConnectionParameters.baseUrl + ":" + configuration.ConnectionParameters.urlWsPort + "/" + configuration.ConnectionParameters.baseWebService + "/security/subscriptions"));

            ServiceBroker.RegistService<WebClientService, WebClientService>(webClientService);

            if (configuration.SmallScreens)
            //if (Screen.PrimaryScreen.Bounds.Width < 1000)
            {
                configuration.MenuItemsFile = "Configuration\\navSmall.json";
                Application.Run(new MainLayoutSmall() { Configuration = configuration });
            }
            else
            {
                Application.Run(new MainLayout() { Configuration = configuration });
            }
            
            if (CfxRuntime.LibrariesLoaded)
            {
                CfxRuntime.Shutdown();
            }
        }
    }
}
