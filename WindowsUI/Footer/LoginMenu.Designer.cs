﻿namespace WindowsUI.Footer
{
    partial class LoginMenu
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.loginIcon = new WindowsUI.UI.NoPaddingLabel();
            this.loginLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // loginIcon
            // 
            this.loginIcon.Font = new System.Drawing.Font("Aulos", 45F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.loginIcon.Location = new System.Drawing.Point(18, 8);
            this.loginIcon.Margin = new System.Windows.Forms.Padding(0);
            this.loginIcon.Name = "loginIcon";
            this.loginIcon.Size = new System.Drawing.Size(45, 45);
            this.loginIcon.TabIndex = 3;
            this.loginIcon.Text = "L";
            this.loginIcon.Click += new System.EventHandler(this.loginIcon_Click);
            // 
            // loginLabel
            // 
            this.loginLabel.Font = new System.Drawing.Font("Open Sans", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.loginLabel.Location = new System.Drawing.Point(3, 55);
            this.loginLabel.Margin = new System.Windows.Forms.Padding(0);
            this.loginLabel.Name = "loginLabel";
            this.loginLabel.Size = new System.Drawing.Size(75, 17);
            this.loginLabel.TabIndex = 2;
            this.loginLabel.Text = "Login";
            this.loginLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.loginLabel.Click += new System.EventHandler(this.loginLabel_Click);
            // 
            // LoginMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.loginIcon);
            this.Controls.Add(this.loginLabel);
            this.Font = new System.Drawing.Font("Open Sans", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.Name = "LoginMenu";
            this.Size = new System.Drawing.Size(80, 80);
            this.Click += new System.EventHandler(this.LoginMenu_Click);
            this.ResumeLayout(false);

        }

        #endregion

        private UI.NoPaddingLabel loginIcon;
        private System.Windows.Forms.Label loginLabel;
    }
}
