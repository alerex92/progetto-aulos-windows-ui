﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsUI.MainContent;
using WindowsUI.UI;

namespace WindowsUI.Footer
{
    public partial class FooterUCBig : FooterUC
    {
        private Panel menuL1;
        private Panel menuL2;
        private Panel menuL3;

        public NavItems NavigationMenu { protected get; set; }
        private string activeMenuL1Name;
        private string activeMenuL2Name;
        private string activeMenuL3Name;
        private Panel menuL2ScrollBar;
        private Panel menuL3ScrollBar;

        private Color blue01 = ColorTranslator.FromHtml("#D2E6FF"); //più chiaro 
        private Color blue02 = ColorTranslator.FromHtml("#B0CFFE");
        private Color blue03 = ColorTranslator.FromHtml("#6699FF");
        private Color footerL2BarColor;

        public FooterUCBig()
        {
            InitializeComponent();

            footerL2BarColor = blue02;
        }

        protected override void GetMenu()
        {
            //NavClient navClient = new NavClient(urlParam);
            //navigationMenu = await navClient.GetNavigationItems();
            //navigationMenu = JsonConvert.DeserializeObject<NavItems>(File.ReadAllText(Configuration.MenuItemsFile));
            BuildL1Menu(NavigationMenu.items);
            List<NavItem> menuL2Items = ActivateL1MenuItem(NavigationMenu.items[0]);
            if (menuL2Items != null)
            {
                BuildL2Menu(menuL2Items);
                List<NavItem> menuL3Items = ActivateL2MenuItem(menuL2Items[0], false);
                if (menuL3Items != null)
                {
                    BuildL3Menu(menuL3Items);
                }
            }
            OnMenuLoaded();
        }

        private void DestroyMenu(Panel menuContainer)
        {
            //TODO magari invece di di distruggere e ricreare controlli è meglio nasconderli?
            if (menuContainer != null)
            {
                while (menuContainer.Controls.Count > 0)
                {
                    menuContainer.Controls[0].Dispose();
                }
                menuContainer.Dispose();
            }
        }

        private void BuildL1Menu(List<NavItem> menuL1Configuration)
        {
            /* Menu Livello 1 */
            menuL1 = new Panel()
            {
                Size = new Size(80 * menuL1Configuration.Count, 80),
                Location = new Point(this.Width / 2 - 40 * menuL1Configuration.Count, this.Height - 80),
                BackColor = Color.Transparent,
            };

            this.Controls.Add(menuL1);

            int i = 0;
            foreach (NavItem menuItem in menuL1Configuration)
            {
                MenuL1Item tempMenuItem = new MenuL1Item()
                {
                    Name = menuItem.label,
                    Icon = menuItem.icon,
                    LabelText = menuItem.label,
                    Location = new Point(i * 80, 0),
                };
                tempMenuItem.Click += new EventHandler(MenuL1Item_Click);
                menuL1.Controls.Add(tempMenuItem);
                i++;
            }
        }

        private void MenuL1Item_Click(object sender, EventArgs e)
        {
            string selectedName = ((Control)sender).Name;
            //selectedName = selectedName.Substring(0, selectedName.Length);
            NavItem menuL1Selected = NavigationMenu.items.Find(item => item.label == selectedName);
            List<NavItem> menuL2Items = ActivateL1MenuItem(menuL1Selected);
            if (menuL2Items != null)
            {
                BuildL2Menu(menuL2Items);
                //ActivateL2MenuItem(menuL2Items[0]);
            }
        }

        private List<NavItem> ActivateL1MenuItem(NavItem itemL1)
        {
            string selectedName = itemL1.label;
            MenuL1Item tempItem;
            List<NavItem> returnItems;

            if ((itemL1.items == null) || (activeMenuL1Name == selectedName))
            {
                returnItems = null;
            }
            else
            {
                returnItems = itemL1.items;
            }

            if ((activeMenuL1Name != null) && (activeMenuL1Name != selectedName))
            {
                //TODO da verificare se è più performante crearsi un dictionary con i controlli oppure no
                //sicuro così è più comodo
                tempItem = (MenuL1Item)menuL1.Controls.Find(activeMenuL1Name, false)[0];
                tempItem.BackColor = Color.Transparent;
                tempItem.Font = new Font(tempItem.Font, FontStyle.Regular);

                DestroyMenu(menuL2);
                DestroyMenu(menuL3);
            }
            if (activeMenuL1Name != selectedName)
            {
                footerL2BarColor = blue02;
                tempItem = (MenuL1Item)menuL1.Controls.Find(selectedName, false)[0];
                tempItem.BackColor = blue02;
                tempItem.Font = new Font(tempItem.Font, FontStyle.Bold);
                activeMenuL1Name = selectedName;
                activeMenuL2Name = null;
                activeMenuL3Name = null;
            }
            return returnItems;
        }

        private void BuildL2Menu(List<NavItem> menuL2Configuration)
        {
            //this.Invalidate(new Rectangle(0, screenHeight - 120, screenWidth, 40));

            menuL2 = new Panel()
            {
                Size = new Size(120 * menuL2Configuration.Count, 40),
                Location = new Point(this.Width / 2 - 60 * menuL2Configuration.Count, this.Height - 120),
                BackColor = blue02,
            };
            this.Controls.Add(menuL2);
            menuL2.Paint += new PaintEventHandler(Footer_Paint);
            //Label tempMenuLabel;
            int i = 0;
            i = 0;
            foreach (NavItem item in menuL2Configuration)
            {
                MenuL2L3Item tempMenuItem = new MenuL2L3Item()
                {
                    Text = item.label,
                    Location = new Point(i * 120, 0),
                    Name = item.label,
                };
                tempMenuItem.Click += MenuL2Item_Click;
                menuL2.Controls.Add(tempMenuItem);
                /*tempMenuLabel = new Label()
                {
                    
                    Font = new Font("Open Sans", 12, GraphicsUnit.Pixel),
                    AutoSize = false,
                    Size = new Size(120, 40),
                    
                    TextAlign = ContentAlignment.MiddleCenter,
                    
                };
                tempMenzuLabel.Click += new EventHandler(MenuL2Item_Click);
                menuL2.Controls.Add(tempMenuLabel);*/
                i++;
            }
        }

        private void MenuL2Item_Click(object sender, EventArgs e)
        {
            string selectedName = ((Control)sender).Name;
            //selectedName = selectedName.Substring(0, selectedName.Length);
            NavItem menuL2Selected = NavigationMenu.items.Find(item => item.label == activeMenuL1Name).items.Find(item => item.label == selectedName);

            if (menuL2Selected.containerId != null)
            {
                NavigationService.MoveContent(menuL2Selected.containerId);
            }
            else
            {
                List<NavItem> menuL3Items = ActivateL2MenuItem(menuL2Selected, true);
                if (menuL3Items != null)
                {
                    BuildL3Menu(menuL3Items);
                    //ActivateL3MenuItem(menuL3Items[0]);
                }
            }
        }

        private List<NavItem> ActivateL2MenuItem(NavItem itemL2, bool move)
        {
            //TODO un'idea potrebbe essere quella di creare un foglio di stile semplificato con costanti
            string selectedName = itemL2.label;
            List<NavItem> returnItems;

            //Se il menu era già attivo e poi ne attivo un altro diverso cancello le cose vecchie
            if ((activeMenuL2Name != null) && (activeMenuL2Name != selectedName))
            {
                MenuL2L3Item oldActive = (MenuL2L3Item)menuL2.Controls.Find(activeMenuL2Name, false)[0];
                oldActive.Font = new Font(oldActive.Font, FontStyle.Regular);
                oldActive.BackColor = Color.Transparent;
                if ((menuL3 != null) && (itemL2.items == null))
                {
                    menuL2.BackColor = blue02;
                    footerL2BarColor = blue02;
                    menuL1.Controls.Find(activeMenuL1Name, false).First().BackColor = blue02;
                    //menuL2.Invalidate();
                }
                DestroyMenu(menuL3);
            }
            //Cerco il caso che esclude essere rimasto sullo stesso menu (inglobando anche il caso = null)
            if ((activeMenuL2Name != selectedName))
            {
                MenuL2L3Item newActive = (MenuL2L3Item)menuL2.Controls.Find(selectedName, false)[0];
                newActive.Font = new Font(newActive.Font, FontStyle.Bold);
                newActive.BackColor = blue02;
                activeMenuL2Name = selectedName;
                activeMenuL3Name = null;

                if (itemL2.items == null)
                {

                    string containerId = NavigationMenu.items.Find(item => item.label == activeMenuL1Name).items.Find(item => item.label == activeMenuL2Name).containerId;
                    if (move == true)
                    {
                        NavigationService.MoveContent(containerId);
                    }

                    //TODO ci sono rischi legati al nome? magari pensare di usare delle costanti?
                    if (menuL2.Controls.ContainsKey("menuL2ScrollBar") == false)
                    {
                        menuL2ScrollBar = new Panel() //chissà chi chiama il metodo (e dove sta questo metodo) per cambiare
                        {
                            // la dimensione di questa barra
                            Height = 5,
                            Width = 0,
                            Location = new Point(0, 0),
                            BackColor = blue03,
                            Name = "menuL2ScrollBar",
                        };
                        menuL2.Controls.Add(menuL2ScrollBar);
                        menuL2ScrollBar.BringToFront();
                    }
                }
                returnItems = itemL2.items;
            }
            else
            {
                //Faccio in modo che se ho selezionato la stessa cosa mi ritorna nullo e non ricrea il menu 3
                returnItems = null;
            }

            return returnItems;
        }

        private void BuildL3Menu(List<NavItem> menuL3Configuration)
        {
            ((MenuL1Item)menuL1.Controls.Find(activeMenuL1Name, false)[0]).BackColor = blue01;
            footerL2BarColor = blue01;
            menuL2.BackColor = blue01;
            //this.Invalidate(new Rectangle(0, screenHeight - 120, screenWidth, 40));

            menuL3 = new Panel()
            {
                Size = new Size(120 * menuL3Configuration.Count, 40),
                BackColor = blue02,
            };
            MenuL2L3Item menuL2LabelSelected = (MenuL2L3Item)menuL2.Controls.Find(activeMenuL2Name, false)[0];
            int offset = 0;
            if ((menuL2.Location.X + menuL2LabelSelected.Location.X) >= this.Width / 2)
            {
                if (menuL3.Width > (menuL2.Location.X + menuL2LabelSelected.Location.X + menuL2LabelSelected.Width))
                {
                    offset = menuL3.Width - (menuL2.Location.X + menuL2LabelSelected.Location.X + menuL2LabelSelected.Width);
                }
                menuL3.Location = new Point(menuL2.Location.X + menuL2LabelSelected.Location.X + menuL2LabelSelected.Width - menuL3.Width + offset, FormHeight - 160);
            }
            else
            {
                if (menuL3.Width > (this.Width - menuL2.Location.X - menuL2LabelSelected.Location.X))
                {
                    offset = menuL3.Width - (this.Width - menuL2.Location.X - menuL2LabelSelected.Location.X);
                }
                menuL3.Location = new Point(menuL2.Location.X + menuL2LabelSelected.Location.X - offset, FormHeight - 160);
            }
            this.ParentForm.Controls.Add(menuL3);
            if (IsLoginMenuOpened())
            {
                this.ParentForm.Controls.SetChildIndex(menuL3, 1);
            }
            else
            {
                this.ParentForm.Controls.SetChildIndex(menuL3, 0);
            }
            menuL2.Paint -= Footer_Paint;
            menuL2.Paint += Footer_Paint;
            //Label tempMenuLabel;
            int i = 0;
            i = 0;
            foreach (NavItem item in menuL3Configuration)
            {
                MenuL2L3Item tempMenuItem = new MenuL2L3Item()
                {
                    Text = item.label,
                    Location = new Point(i * 120, 0),
                    Name = item.label,
                };
                tempMenuItem.Click += MenuL3Item_Click;
                menuL3.Controls.Add(tempMenuItem);
                /*tempMenuLabel = new Label()
                {
                    Text = item.label,
                    Font = new Font("Open Sans", 12, GraphicsUnit.Pixel),
                    AutoSize = false,
                    Size = new Size(120, 40),
                    Location = new Point(i * 120, 0),
                    TextAlign = ContentAlignment.MiddleCenter,
                    Name = item.label,
                };
                tempMenuLabel.Click += new EventHandler(MenuL3Item_Click);
                menuL3.Controls.Add(tempMenuLabel);*/
                i++;
            }
        }

        private void MenuL3Item_Click(object sender, EventArgs e)
        {
            string selectedName = ((Control)sender).Name;
            //selectedName = selectedName.Substring(0, selectedName.Length);
            NavItem menuL3Selected = NavigationMenu.items.Find(item => item.label == activeMenuL1Name).items.Find(item => item.label == activeMenuL2Name).items.Find(item => item.label == selectedName);
            //ActivateL3MenuItem(menuL3Selected, true);
            string containerId = NavigationMenu.items.Find(item => item.label == activeMenuL1Name).items.Find(item => item.label == activeMenuL2Name).items.Find(item => item.label == menuL3Selected.label).containerId;
            NavigationService.MoveContent(containerId);
        }

        private void ActivateL3MenuItem(NavItem itemL3, bool move)
        {
            if (menuL3.Controls.ContainsKey("menuL3ScrollBar") == false)
            {
                if (menuL2.Controls.ContainsKey("menuL2ScrollBar") == true)
                {
                    menuL2.Controls.Find("menuL2ScrollBar", false)[0].Dispose();
                }
                menuL3ScrollBar = new Panel()
                {
                    Location = new Point(0, 0),
                    Height = 5,
                    Width = 0,
                    BackColor = blue03,
                    Name = "menuL3ScrollBar",
                };
                menuL3.Controls.Add(menuL3ScrollBar);
                menuL3ScrollBar.BringToFront();
            }

            string selectedName = itemL3.label;

            if ((activeMenuL3Name != null) && (activeMenuL3Name != selectedName))
            {
                MenuL2L3Item oldActive = (MenuL2L3Item)menuL3.Controls.Find(activeMenuL3Name, false)[0];
                oldActive.Font = new Font(oldActive.Font, FontStyle.Regular);
                oldActive.BackColor = Color.Transparent;
            }

            MenuL2L3Item newActive = (MenuL2L3Item)menuL3.Controls.Find(itemL3.label, false)[0];
            newActive.Font = new Font(newActive.Font, FontStyle.Bold);
            newActive.BackColor = blue02;

            activeMenuL3Name = selectedName;

            string containerId = NavigationMenu.items.Find(item => item.label == activeMenuL1Name).items.Find(item => item.label == activeMenuL2Name).items.Find(item => item.label == activeMenuL3Name).containerId;

            if (move == true)
            {
                NavigationService.MoveContent(containerId);
            }
        }

        public void FirstActive() //Con questo ci sarebbe anche la possibilità di attivare un particolare container
        {
            List<NavItem> tempItem = NavigationMenu.items;
            int level = 0;
            while (tempItem[0].items != null)
            {
                tempItem = tempItem[0].items;
                level++;
            }
            NavItem first = tempItem[0];
            switch (level)
            {
                case 0: ActivateL1MenuItem(first); break; //caso che non deve capitare nella realtà
                case 1: ActivateL2MenuItem(first, false); break;
                case 2: ActivateL3MenuItem(first, false); break;
            }
        }

        public void ActivateMenu(string centralContainerId)
        {
            List<NavItem> pathToCentralContainer = TreeFunctions.Search(NavigationMenu.items, centralContainerId);
            pathToCentralContainer.Reverse();

            if (pathToCentralContainer.Count > 1)
            {
                if (pathToCentralContainer[0].label != activeMenuL1Name)
                {
                    ActivateL1MenuItem(pathToCentralContainer[0]);
                    BuildL2Menu(pathToCentralContainer[0].items);
                }
                if (pathToCentralContainer[1].label != activeMenuL2Name)
                {
                    ActivateL2MenuItem(pathToCentralContainer[1], false);
                    menuL2.Refresh();
                }
                if (pathToCentralContainer.Count == 3)
                {
                    if ((menuL3 == null) || (menuL3.IsDisposed))
                    {
                        BuildL3Menu(pathToCentralContainer[1].items);
                        if (menuL2ScrollBar != null)
                        {
                            menuL2ScrollBar.Width = 0;
                        }
                        menuL2.Refresh();
                    }
                    if (pathToCentralContainer[2].label != activeMenuL3Name)
                    {
                        ActivateL3MenuItem(pathToCentralContainer[2], false);
                    }
                }
            }
        }

        public void ChangeBar(Dictionary<string, int[]> containerVisiblePercentage)
        {
            if (activeMenuL3Name != null)
            {
                int i = -1;
                Control left, body;

                //Calcolo la parte sinistra della barra
                do
                {
                    i++;
                    left = null;
                    NavItem leftControlNavItem = NavigationMenu.items.Find(item => item.label == activeMenuL1Name).items.Find(item => item.label == activeMenuL2Name).items.Find(item => item.containerId == containerVisiblePercentage.ElementAt(i).Key);
                    if (leftControlNavItem != null)
                    {
                        left = menuL3.Controls.Find(leftControlNavItem.label, false).First();
                        menuL3ScrollBar.Location = new Point(left.Location.X + containerVisiblePercentage.ElementAt(i).Value[1] * left.Width / 100, menuL3ScrollBar.Location.Y);
                        menuL3ScrollBar.Width = (containerVisiblePercentage.ElementAt(i).Value[2] - containerVisiblePercentage.ElementAt(i).Value[1]) * left.Width / 100;
                    }
                } while ((left == null) && (i < containerVisiblePercentage.Count - 1));

                //Calcolo il resto
                do
                {
                    body = null;
                    if (i < containerVisiblePercentage.Count - 1)
                    {
                        i++;
                        NavItem bodyControlNavItem = NavigationMenu.items.Find(item => item.label == activeMenuL1Name).items.Find(item => item.label == activeMenuL2Name).items.Find(item => item.containerId == containerVisiblePercentage.ElementAt(i).Key);
                        if (bodyControlNavItem != null)
                        {
                            body = menuL3.Controls.Find(bodyControlNavItem.label, false).First();
                            menuL3ScrollBar.Width += (containerVisiblePercentage.ElementAt(i).Value[2] - containerVisiblePercentage.ElementAt(i).Value[1]) * body.Width / 100;
                        }
                    }
                } while (body != null);
                menuL3.Refresh();
            }
            else if (activeMenuL2Name != null)
            {
                int i = -1;
                Control left, body;

                //Calcolo la parte sinistra della barra
                do
                {
                    i++;
                    left = null;
                    NavItem leftControlNavItem = NavigationMenu.items.Find(item => item.label == activeMenuL1Name).items.Find(item => item.containerId == containerVisiblePercentage.ElementAt(i).Key);
                    if (leftControlNavItem != null)
                    {
                        left = menuL2.Controls.Find(leftControlNavItem.label, false).First();
                        menuL2ScrollBar.Location = new Point(left.Location.X + containerVisiblePercentage.ElementAt(i).Value[1] * left.Width / 100, menuL2ScrollBar.Location.Y);
                        menuL2ScrollBar.Width = (containerVisiblePercentage.ElementAt(i).Value[2] - containerVisiblePercentage.ElementAt(i).Value[1]) * left.Width / 100;
                    }
                } while ((left == null) && (i < containerVisiblePercentage.Count - 1));


                //Calcolo il resto
                do
                {
                    body = null;
                    if (i < containerVisiblePercentage.Count - 1)
                    {
                        i++;
                        NavItem bodyControlNavItem = NavigationMenu.items.Find(item => item.label == activeMenuL1Name).items.Find(item => item.containerId == containerVisiblePercentage.ElementAt(i).Key);
                        if (bodyControlNavItem != null)
                        {
                            body = menuL2.Controls.Find(bodyControlNavItem.label, false).First();
                            menuL2ScrollBar.Width += (containerVisiblePercentage.ElementAt(i).Value[2] - containerVisiblePercentage.ElementAt(i).Value[1]) * body.Width / 100;
                        }
                    }
                } while (body != null);
                menuL2.Refresh();
            }
        }

        protected override void Footer_Paint(object sender, PaintEventArgs e)
        {
            Graphics g = this.CreateGraphics();
            //Rectangle greyRect = new Rectangle(new Point(0, 40), new Size(this.Width, 80));
            //g.FillRectangle(new SolidBrush(ColorTranslator.FromHtml("#F2F2F2")), greyRect);

            Rectangle blueRect = new Rectangle(new Point(0, 0), new Size(this.Width, 40));
            g.FillRectangle(new SolidBrush(footerL2BarColor), blueRect);
        }

        protected override void LoginMenu_Closed(object sender, EventArgs e)
        {
            base.LoginMenu_Closed(sender, e);
            if ((menuL2 != null) && (!menuL2.IsDisposed))
            {
                menuL2.Invalidate();
            }
        }
    }
}
