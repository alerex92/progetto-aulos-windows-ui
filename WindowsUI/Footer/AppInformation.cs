﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsUI.Footer
{
    public partial class AppInformation : UserControl
    {
        [Description("Change the app name"), Category("Aulos")]
        public string AppName { get { return nameLabel.Text;} set {nameLabel.Text = value;} }

        public AppInformation()
        {
            InitializeComponent();
        }

        private void nameLabel_TextChanged(object sender, EventArgs e)
        {
            int textwidth = TextRenderer.MeasureText(nameLabel.Text, nameLabel.Font).Width;
            this.Width = textwidth;
        }
    }
}
