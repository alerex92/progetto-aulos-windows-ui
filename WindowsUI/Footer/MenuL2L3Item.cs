﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsUI.Footer
{
    public partial class MenuL2L3Item : UserControl
    {
        public override string Text { get { return menuLabel.Text; } set {menuLabel.Text = value;} }

        public MenuL2L3Item()
        {
            InitializeComponent();
        }

        private void menuLabel_Click(object sender, EventArgs e)
        {
            this.OnClick(e);
        }
    }
}
