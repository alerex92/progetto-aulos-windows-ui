﻿using System;
using System.Drawing;
using System.Windows.Forms;
using WindowsUI.UI;

namespace WindowsUI.Footer
{
    public partial class MenuL1Item : UserControl
    {

        public string LabelText { get { return menuLabel.Text; } set { menuLabel.Text = value; } }

        public string Icon { set { menuIcon.Text = FontAulos.GetIconByName(value); } }

        public override Font Font { get {return menuLabel.Font; } set { menuLabel.Font = value; } }

        public MenuL1Item()
        {
            InitializeComponent();
            SetStyle(ControlStyles.OptimizedDoubleBuffer, true);
            /*SetStyle(ControlStyles.UserPaint, true);
            SetStyle(ControlStyles.AllPaintingInWmPaint, true);*/
        }

        private void menuIcon_Click(object sender, EventArgs e)
        {
            this.OnClick(e);
        }

        private void menuLabel_Click(object sender, EventArgs e)
        {
            this.OnClick(e);
        }

        private void MenuL1Item_BackColorChanged(object sender, EventArgs e)
        {
            this.Refresh();
        }
    }
}
