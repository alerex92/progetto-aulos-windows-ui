﻿using WindowsUI.UI;

namespace WindowsUI.Footer
{
    partial class MenuL1Item
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuLabel = new System.Windows.Forms.Label();
            this.menuIcon = new WindowsUI.UI.NoPaddingLabel();
            this.SuspendLayout();
            // 
            // menuLabel
            // 
            this.menuLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.menuLabel.Font = new System.Drawing.Font("Open Sans", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.menuLabel.Location = new System.Drawing.Point(2, 55);
            this.menuLabel.Margin = new System.Windows.Forms.Padding(0);
            this.menuLabel.Name = "menuLabel";
            this.menuLabel.Size = new System.Drawing.Size(75, 17);
            this.menuLabel.TabIndex = 0;
            this.menuLabel.Text = "menuLabel";
            this.menuLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.menuLabel.Click += new System.EventHandler(this.menuLabel_Click);
            // 
            // menuIcon
            // 
            this.menuIcon.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.menuIcon.Font = new System.Drawing.Font("Aulos", 45F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.menuIcon.Location = new System.Drawing.Point(17, 8);
            this.menuIcon.Margin = new System.Windows.Forms.Padding(0);
            this.menuIcon.Name = "menuIcon";
            this.menuIcon.Size = new System.Drawing.Size(45, 45);
            this.menuIcon.TabIndex = 1;
            this.menuIcon.Text = "L";
            this.menuIcon.Click += new System.EventHandler(this.menuIcon_Click);
            // 
            // MenuL1Item
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this.menuIcon);
            this.Controls.Add(this.menuLabel);
            this.Margin = new System.Windows.Forms.Padding(0);
            this.Name = "MenuL1Item";
            this.Size = new System.Drawing.Size(80, 80);
            this.BackColorChanged += new System.EventHandler(this.MenuL1Item_BackColorChanged);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label menuLabel;
        private NoPaddingLabel menuIcon;
    }
}
