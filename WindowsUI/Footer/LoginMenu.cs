﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsUI.UI;
using Loccioni.Aulos.Security.WebClient;
using Loccioni.Aulos;
using Loccioni.Aulos.Security;
using WebSocketSharp;
using Session = Loccioni.Aulos.Security.TransferModel.Session;

namespace WindowsUI.Footer
{
    public partial class LoginMenu : UserControl
    {
        private Panel loginMenuPanel;
        private Panel confirmPanel;

        private Timer leftAnimationTimer;
        //Timer rightAnimationTimer;
        private int animationProgress;

        private SecurityClient securityClient;

        public bool LoginMenuOpened { get; private set; }
        private string activeLoginMenuName;

        public int FormHeight { private get; set; }
        public int FooterWidth { private get; set; }
        public string Username { private get; set; }
        public bool LoginRequired { private get; set; }

        public event EventHandler MenuOpenedHandler;
        public event EventHandler MenuClosedHandler;
        public event EventHandler UserLogut;

        public LoginMenu()
        {
            InitializeComponent();
            loginIcon.Text = FontAulos.Login;

            leftAnimationTimer = new Timer();
            leftAnimationTimer.Tick += new EventHandler(LeftAnimation_Tick);
            leftAnimationTimer.Interval = 20;
        }

        public void Setup()
        {
            WebClientService webClientService = ServiceBroker.GetService<WebClientService>();
            securityClient = (SecurityClient)webClientService[typeof(SecurityClient)];

            if (!Username.IsNullOrEmpty())
            {
                loginLabel.Text = Username;
            }
        }

        /*private async void CheckUser()
        {
            IEnumerable<Session> session = await securityClient.GetSessions();
            
        }*/

        private void LoginMenu_Click(object sender, EventArgs e)
        {
            if (Username.IsNullOrEmpty() && LoginRequired)
            {
                LoginPopup loginPopup = new LoginPopup { MainForm = this.ParentForm };
                loginPopup.BuildLoginPopup();
                loginPopup.LoggedUser += LoggedUser;
            }
            else
            {
                string[] loginMenuItems = new string[] { "Shutdown", "Restart", "Logout" };
                if (LoginMenuOpened == false)
                {
                    LoginMenuOpened = true;
                    loginMenuPanel = new Panel()
                    {
                        Size = new Size(180, loginMenuItems.Length * 40 + (loginMenuItems.Length + 1) * 10), //3 elementi 4 spazi
                        BackColor = ColorTranslator.FromHtml("#F2F2F2"),
                    };
                    loginMenuPanel.Location = new Point(FooterWidth - 180, FormHeight - 80 - loginMenuPanel.Height);
                    this.ParentForm.Controls.Add(loginMenuPanel);
                    loginMenuPanel.BringToFront();

                    Label tempMenuItem;
                    for (int i = 0; i < 3; i++)
                    {
                        tempMenuItem = new Label()
                        {
                            Font = new Font("Open Sans", 12, GraphicsUnit.Pixel),
                            Text = loginMenuItems[i],
                            TextAlign = ContentAlignment.MiddleCenter,
                            Size = new Size(140, 40),
                            Location = new Point(20, loginMenuPanel.Height - (10 + 40) * (i + 1)),
                            Name = loginMenuItems[i],
                            Anchor = AnchorStyles.Bottom | AnchorStyles.Left,
                        };
                        tempMenuItem.Click += new EventHandler(LoginMenuItem_Click);
                        loginMenuPanel.Controls.Add(tempMenuItem);
                    }
                    MenuOpenedHandler?.Invoke(this, EventArgs.Empty);
                }
                else
                {
                    LoginMenuOpened = false;
                    activeLoginMenuName = null;
                    while (loginMenuPanel.Controls.Count > 0)
                    {
                        loginMenuPanel.Controls[0].Dispose();
                    }
                    loginMenuPanel.Dispose();
                    MenuClosedHandler?.Invoke(this, EventArgs.Empty);
                }
            }            
        }

        private void LoggedUser(object sender, LoggedUserEventArgs e)
        {
            Username = e.UserSession.Username;
            loginLabel.Text = Username;
        }

        private void LoginMenuItem_Click(object sender, EventArgs e)
        {
            if (activeLoginMenuName == null)
            {
                Label selectedItem = (Label)sender;
                activeLoginMenuName = selectedItem.Name;
                int itemIndex = loginMenuPanel.Controls.IndexOf(selectedItem);
                for (int i = 0; i < loginMenuPanel.Controls.Count; i++)
                {
                    if (i != itemIndex)
                    {
                        loginMenuPanel.Controls[i].ForeColor = ColorTranslator.FromHtml("#B3B3B3");
                    }
                }
                loginMenuPanel.Height = loginMenuPanel.Height + 40 + 10;
                loginMenuPanel.Location = new Point(loginMenuPanel.Location.X, loginMenuPanel.Location.Y - 40 - 10);
                Label sureLabel = new Label()
                {
                    Location = new Point(20, 10), //Dato che sarà sempre quella più in alto di tutti
                    Size = new Size(140, 40),
                    Font = new Font("Open Sans", 12, GraphicsUnit.Pixel),
                    Text = "Are you sure?",
                    TextAlign = ContentAlignment.MiddleLeft,
                };
                loginMenuPanel.Controls.Add(sureLabel);

                confirmPanel = new Panel()
                {
                    Size = new Size(140, 40),
                    Location = new Point(180, selectedItem.Location.Y),
                };
                Button noLabel = new Button()
                {
                    BackColor = ColorTranslator.FromHtml("#B0CFFE"),
                    Text = "No",
                    Font = new Font("Open Sans", 12, GraphicsUnit.Pixel),
                    TextAlign = ContentAlignment.MiddleCenter,
                    Size = new Size(60, 40),
                    Location = new Point(0, 0),
                    FlatStyle = FlatStyle.Flat,
                };
                noLabel.Click += new EventHandler(LoginCancel_Click);
                noLabel.FlatAppearance.BorderSize = 0;
                noLabel.FlatAppearance.MouseDownBackColor = ColorTranslator.FromHtml("#6699FF");
                noLabel.FlatAppearance.MouseOverBackColor = ColorTranslator.FromHtml("#B0CFFE");
                //noLabel.MouseDown += new MouseEventHandler(LoginConfirm_MouseDown);
                //noLabel.MouseUp += new MouseEventHandler(LoginConfirm_MouseUp);
                confirmPanel.Controls.Add(noLabel);
                Button yesLabel = new Button()
                {
                    BackColor = ColorTranslator.FromHtml("#B0CFFE"),
                    Text = "Yes",
                    Font = new Font("Open Sans", 12, GraphicsUnit.Pixel),
                    TextAlign = ContentAlignment.MiddleCenter,
                    Size = new Size(60, 40),
                    Location = new Point(80, 0),
                    FlatStyle = FlatStyle.Flat,
                };
                yesLabel.Click += new EventHandler(LoginConfirm_Click);
                yesLabel.FlatAppearance.BorderSize = 0;
                yesLabel.FlatAppearance.MouseDownBackColor = ColorTranslator.FromHtml("#6699FF");
                yesLabel.FlatAppearance.MouseOverBackColor = ColorTranslator.FromHtml("#B0CFFE");
                //yesLabel.MouseDown += new MouseEventHandler(LoginConfirm_MouseDown);
                //yesLabel.MouseUp += new MouseEventHandler(LoginConfirm_MouseUp);
                confirmPanel.Controls.Add(yesLabel);

                loginMenuPanel.Controls.Add(confirmPanel);
                confirmPanel.BringToFront();
                animationProgress = 160;
                leftAnimationTimer.Start();
            }
        }

        private void LoginConfirm_Click(object sender, EventArgs e)
        {
            switch (activeLoginMenuName)
            {
                case "Shutdown": 
                    ParentForm.Close();
                    DestroyLoginMenu();
                    break;
                case "Restart": break; 
                case "Logout":
                    Username = null;
                    loginLabel.Text = "Login";
                    UserLogut?.Invoke(this, EventArgs.Empty);
                    break;
            }
            MenuClosedHandler?.Invoke(this, EventArgs.Empty);
        }

        private void LoginCancel_Click(object sender, EventArgs e)
        {
            DestroyLoginMenu();
            MenuClosedHandler?.Invoke(this, EventArgs.Empty);
        }

        private void DestroyLoginMenu()
        {
            activeLoginMenuName = null;
            LoginMenuOpened = false;
            while (loginMenuPanel.Controls.Count > 0)
            {
                loginMenuPanel.Controls[0].Dispose();
            }
            loginMenuPanel.Dispose();
        }

        private void LeftAnimation_Tick(object sender, EventArgs e)
        {
            if (confirmPanel.Location.X > 20 + 5)
            {
                confirmPanel.Location = new Point((int)Animation.EaseInOut(animationProgress, 20, 160, 300), confirmPanel.Location.Y);
                animationProgress -= 160 / 15;
            }
            else
            {
                animationProgress = 0;
                confirmPanel.Location = new Point(20, confirmPanel.Location.Y);
                leftAnimationTimer.Stop();
            }
        }

        private void loginIcon_Click(object sender, EventArgs e)
        {
            this.OnClick(e);
        }

        private void loginLabel_Click(object sender, EventArgs e)
        {
            this.OnClick(e);
        }
    }
}
