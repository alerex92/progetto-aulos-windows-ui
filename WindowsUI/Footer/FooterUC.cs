﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using WindowsUI.UI;
using Chromium.Event;


namespace WindowsUI.Footer
{
    public partial class FooterUC : UserControl
    {
        public event EventHandler MenuLoaded;
        public event EventHandler UserLogout;

        //Parametri esterni
        public int FormHeight { get; set; }
        //private int formWidth;
        //public int FormWidth { set { formWidth = value; } }
        /*private Dictionary<string, string> urlParam;
        public Dictionary<string, string> UrlParam { set { urlParam = value; } }*/
        public GlobalConfiguration Configuration { private get; set; }

        [Description("Change the app name"), Category("Aulos")]
        public string AppName { get { return appInformation.AppName; } set { appInformation.AppName = value; } }

        public string Username { get; set; }

        public FooterUC()
        {
            InitializeComponent();

            loginMenu.MenuClosedHandler += LoginMenu_Closed;
            loginMenu.MenuOpenedHandler += LoginMenu_Opened;
        }

        protected void MakeSmaller()
        {
            this.Height = 80;
        }

        public virtual void Setup()
        {
            GetMenu();

            appInformation.AppName = Configuration.BenchName;

            loginMenu.FooterWidth = this.Width;
            loginMenu.FormHeight = FormHeight;
            loginMenu.Username = Username;
            loginMenu.LoginRequired = Configuration.LoginRequired;
            loginMenu.UserLogut += BubbleUserLogout;
            loginMenu.Setup();
        }

        protected virtual void GetMenu() {}

        protected void OnMenuLoaded()
        {
            MenuLoaded?.Invoke(this, EventArgs.Empty);
        }

        private void BubbleUserLogout(object sender, EventArgs e)
        {
            UserLogout?.Invoke(sender, e);
        }

        protected bool IsLoginMenuOpened()
        {
            return loginMenu.LoginMenuOpened;
        }

        protected virtual void LoginMenu_Closed(object sender, EventArgs e)
        {
            clockDateTime.LoginMenuOpened = false;
            clockDateTime.Refresh();
        }

        protected void LoginMenu_Opened(object sender, EventArgs e)
        {
            clockDateTime.LoginMenuOpened = true;
            clockDateTime.Refresh();
        }

        protected virtual void Footer_Paint(object sender, PaintEventArgs e) {}

        private void appInformation_Click(object sender, EventArgs e)
        {
            /*AlertPopup alertPopup = new AlertPopup();
            alertPopup.MainForm = this.ParentForm;
            alertPopup.Critical = false;
            alertPopup.BuildPopup("SERVER RESTART", "Lorem ipsum dolor sit amet, consectetuer s sadipiscing elit. Aenean commodo ligula eget dolor.");*/
        }

        private void FooterUC_Click(object sender, EventArgs e)
        {
            /*ProgressPopup progressPopup = new ProgressPopup();
            progressPopup.MainForm = this.ParentForm;
            progressPopup.BuildProgressPopup("CONTAINER LOADING", "Loading all the container configured in the file nav.json in the folder configuration");
            progressPopup.SetProgress(70);*/
        }
    }
}
