﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsUI.UI;

namespace WindowsUI.Footer
{
    public partial class FooterUCSmall : FooterUC
    {
        private Panel menuL1;
        private Panel menuL2;

        public NavItems NavigationMenu { protected get; set; }
        private string activeMenuL1Name;
        private string activeMenuL2Name;

        private Panel menuL1ScrollBar;
        private Panel menuL2ScrollBar;

        private Color blue01 = ColorTranslator.FromHtml("#D2E6FF"); //più chiaro 
        private Color blue02 = ColorTranslator.FromHtml("#B0CFFE");
        private Color blue03 = ColorTranslator.FromHtml("#6699FF");

        public FooterUCSmall()
        {
            InitializeComponent();

            MakeSmaller();
        }

        protected override void GetMenu()
        {
            //NavClient navClient = new NavClient(urlParam);
            //navigationMenu = await navClient.GetNavigationItems();
            //navigationMenu = JsonConvert.DeserializeObject<NavItems>(File.ReadAllText(Configuration.MenuItemsFile));
            BuildL1Menu(NavigationMenu.items);
            List<NavItem> menuL2Items = ActivateL1MenuItem(NavigationMenu.items[0], false);
            if (menuL2Items != null)
            {
                BuildL2Menu(menuL2Items);
            }
            OnMenuLoaded();
        }

        private void DestroyMenu(Panel menuContainer)
        {
            //TODO magari invece di di distruggere e ricreare controlli è meglio nasconderli?
            if (menuContainer != null)
            {
                while (menuContainer.Controls.Count > 0)
                {
                    menuContainer.Controls[0].Dispose();
                }
                menuContainer.Dispose();
            }
        }

        private void BuildL1Menu(List<NavItem> menuL1Configuration)
        {
            /* Menu Livello 1 */
            menuL1 = new Panel()
            {
                Size = new Size(80 * menuL1Configuration.Count, 80),
                Location = new Point(this.Width / 2 - 40 * menuL1Configuration.Count, this.Height - 80),
                BackColor = Color.Transparent,
            };

            this.Controls.Add(menuL1);

            int i = 0;
            foreach (NavItem menuItem in menuL1Configuration)
            {
                MenuL1Item tempMenuItem = new MenuL1Item()
                {
                    Name = menuItem.label,
                    Icon = menuItem.icon,
                    LabelText = menuItem.label,
                    Location = new Point(i * 80, 0),
                };
                tempMenuItem.Click += new EventHandler(MenuL1Item_Click);
                menuL1.Controls.Add(tempMenuItem);
                i++;
            }
        }

        private void MenuL1Item_Click(object sender, EventArgs e)
        {
            string selectedName = ((Control)sender).Name;
            //selectedName = selectedName.Substring(0, selectedName.Length);
            NavItem menuL1Selected = NavigationMenu.items.Find(item => item.label == selectedName);
            if (menuL1Selected.containerId != null)
            {
                NavigationService.MoveContent(menuL1Selected.containerId);
            }
            else
            {
                List<NavItem> menuL2Items = ActivateL1MenuItem(menuL1Selected, true);
                if (menuL2Items != null)
                {
                    BuildL2Menu(menuL2Items);
                }
            }
        }

        private List<NavItem> ActivateL1MenuItem(NavItem itemL1, bool move)
        {
            string selectedName = itemL1.label;
            MenuL1Item tempItem;
            List<NavItem> returnItems;

            if ((activeMenuL1Name != null) && (activeMenuL1Name != selectedName))
            {
                //TODO da verificare se è più performante crearsi un dictionary con i controlli oppure no
                //sicuro così è più comodo
                tempItem = (MenuL1Item)menuL1.Controls.Find(activeMenuL1Name, false)[0];
                tempItem.BackColor = Color.Transparent;
                tempItem.Font = new Font(tempItem.Font, FontStyle.Regular);

                DestroyMenu(menuL2);
            }
            if (activeMenuL1Name != selectedName)
            {
                tempItem = (MenuL1Item)menuL1.Controls.Find(selectedName, false)[0];
                tempItem.BackColor = blue02;
                tempItem.Font = new Font(tempItem.Font, FontStyle.Bold);
                activeMenuL1Name = selectedName;
                activeMenuL2Name = null;
                if (itemL1.items == null)
                {
                    string containerId = NavigationMenu.items.Find(item => item.label == activeMenuL1Name).containerId;
                    if (move == true)
                    {
                        NavigationService.MoveContent(containerId);
                    }
                    if (menuL1.Controls.ContainsKey("menuL1ScrollBar") == false)
                    {
                        menuL1ScrollBar = new Panel() //chissà chi chiama il metodo (e dove sta questo metodo) per cambiare
                        {
                            // la dimensione di questa barra
                            Height = 5,
                            Width = 0,
                            Location = new Point(0, 0),
                            BackColor = blue03,
                            Name = "menuL1ScrollBar",
                        };
                        menuL1.Controls.Add(menuL1ScrollBar);
                        menuL1ScrollBar.BringToFront();
                    }
                }
                returnItems = itemL1.items;
            }
            else
            {
                //Faccio in modo che se ho selezionato la stessa cosa mi ritorna nullo e non ricrea il menu 2
                returnItems = null;
            }
            return returnItems;
        }

        private void BuildL2Menu(List<NavItem> menuL2Configuration)
        {
            //this.Invalidate(new Rectangle(0, screenHeight - 120, screenWidth, 40));

            menuL2 = new Panel()
            {
                Size = new Size(120 * menuL2Configuration.Count, 40),
                BackColor = blue02,
            };
            MenuL1Item menuL1LabelSelected = (MenuL1Item)menuL1.Controls.Find(activeMenuL1Name, false)[0];
            int offset = 0;
            if ((menuL1.Location.X + menuL1LabelSelected.Location.X) >= this.Width / 2)
            {
                if (menuL2.Width > (menuL1.Location.X + menuL1LabelSelected.Location.X + menuL1LabelSelected.Width))
                {
                    offset = menuL2.Width - (menuL1.Location.X + menuL1LabelSelected.Location.X + menuL1LabelSelected.Width);
                }
                menuL2.Location = new Point(menuL1.Location.X + menuL1LabelSelected.Location.X + menuL1LabelSelected.Width - menuL2.Width + offset, FormHeight - 120);
            }
            else
            {
                if (menuL2.Width > (this.Width - menuL1.Location.X - menuL1LabelSelected.Location.X))
                {
                    offset = menuL2.Width - (this.Width - menuL1.Location.X - menuL1LabelSelected.Location.X);
                }
                menuL2.Location = new Point(menuL1.Location.X + menuL1LabelSelected.Location.X - offset, FormHeight - 120);
            }
            this.ParentForm.Controls.Add(menuL2);
            if (IsLoginMenuOpened())
            {
                this.ParentForm.Controls.SetChildIndex(menuL2, 1);
            }
            else
            {
                this.ParentForm.Controls.SetChildIndex(menuL2, 0);
            }
            menuL2.Paint += Footer_Paint;
            //Label tempMenuLabel;
            int i = 0;
            i = 0;
            foreach (NavItem item in menuL2Configuration)
            {
                MenuL2L3Item tempMenuItem = new MenuL2L3Item()
                {
                    Text = item.label,
                    Location = new Point(i * 120, 0),
                    Name = item.label,
                };
                tempMenuItem.Click += MenuL2Item_Click;
                menuL2.Controls.Add(tempMenuItem);
                i++;
            }
        }

        private void MenuL2Item_Click(object sender, EventArgs e)
        {
            string selectedName = ((Control)sender).Name;
            NavItem menuL2Selected = NavigationMenu.items.Find(item => item.label == activeMenuL1Name).items.Find(item => item.label == selectedName);
            string containerId = menuL2Selected.containerId;
            NavigationService.MoveContent(containerId);
        }

        private void ActivateL2MenuItem(NavItem itemL2, bool move)
        {
            if (menuL2.Controls.ContainsKey("menuL2ScrollBar") == false)
            {
                if (menuL1.Controls.ContainsKey("menuL1ScrollBar") == true)
                {
                    menuL1.Controls.Find("menuL1ScrollBar", false)[0].Dispose();
                }
                menuL2ScrollBar = new Panel()
                {
                    Location = new Point(0, 0),
                    Height = 5,
                    Width = 0,
                    BackColor = blue03,
                    Name = "menuL2ScrollBar",
                };
                menuL2.Controls.Add(menuL2ScrollBar);
                menuL2ScrollBar.BringToFront();
            }

            string selectedName = itemL2.label;

            if ((activeMenuL2Name != null) && (activeMenuL2Name != selectedName))
            {
                MenuL2L3Item oldActive = (MenuL2L3Item)menuL2.Controls.Find(activeMenuL2Name, false)[0];
                oldActive.Font = new Font(oldActive.Font, FontStyle.Regular);
                oldActive.BackColor = Color.Transparent;
            }

            MenuL2L3Item newActive = (MenuL2L3Item)menuL2.Controls.Find(itemL2.label, false)[0];
            newActive.Font = new Font(newActive.Font, FontStyle.Bold);
            newActive.BackColor = blue02;

            activeMenuL2Name = selectedName;

            string containerId = NavigationMenu.items.Find(item => item.label == activeMenuL1Name).items.Find(item => item.label == activeMenuL2Name).containerId;

            if (move == true)
            {
                NavigationService.MoveContent(containerId);
            }
        }

        public void FirstActive() //Con questo ci sarebbe anche la possibilità di attivare un particolare container
        {
            List<NavItem> tempItem = NavigationMenu.items;
            int level = 0;
            while (tempItem[0].items != null)
            {
                tempItem = tempItem[0].items;
                level++;
            }
            NavItem first = tempItem[0];
            switch (level)
            {
                case 0: ActivateL1MenuItem(first, false); break;
                case 1: ActivateL2MenuItem(first, false); break;
            }
        }

        public void ActivateMenu(string centralContainerId)
        {
            List<NavItem> pathToCentralContainer = TreeFunctions.Search(NavigationMenu.items, centralContainerId);
            pathToCentralContainer.Reverse();

            if (pathToCentralContainer[0].label != activeMenuL1Name)
            {
                ActivateL1MenuItem(pathToCentralContainer[0], false);
            }
            if (pathToCentralContainer.Count > 1)
            {
                if ((menuL2 == null) || (menuL2.IsDisposed))
                {
                    BuildL2Menu(pathToCentralContainer[0].items);
                    if (menuL1ScrollBar != null)
                    {
                        menuL1ScrollBar.Width = 0;
                    }
                    menuL1.Refresh();
                }
                if (pathToCentralContainer[1].label != activeMenuL2Name)
                {
                    ActivateL2MenuItem(pathToCentralContainer[1], false);
                }
            }
        }

        public void ChangeBar(Dictionary<string, int[]> containerVisiblePercentage)
        {
            if (activeMenuL2Name != null)
            {
                int i = -1;
                Control left, body;

                //Calcolo la parte sinistra della barra
                do
                {
                    i++;
                    left = null;
                    NavItem leftControlNavItem = NavigationMenu.items.Find(item => item.label == activeMenuL1Name).items.Find(item => item.containerId == containerVisiblePercentage.ElementAt(i).Key);
                    if (leftControlNavItem != null)
                    {
                        left = menuL2.Controls.Find(leftControlNavItem.label, false).First();
                        menuL2ScrollBar.Location = new Point(left.Location.X + containerVisiblePercentage.ElementAt(i).Value[1] * left.Width / 100, menuL2ScrollBar.Location.Y);
                        menuL2ScrollBar.Width = (containerVisiblePercentage.ElementAt(i).Value[2] - containerVisiblePercentage.ElementAt(i).Value[1]) * left.Width / 100;
                    }
                } while ((left == null) && (i < containerVisiblePercentage.Count - 1));

                //Calcolo il resto
                do
                {
                    body = null;
                    if (i < containerVisiblePercentage.Count - 1)
                    {
                        i++;
                        NavItem bodyControlNavItem = NavigationMenu.items.Find(item => item.label == activeMenuL1Name).items.Find(item => item.containerId == containerVisiblePercentage.ElementAt(i).Key);
                        if (bodyControlNavItem != null)
                        {
                            body = menuL2.Controls.Find(bodyControlNavItem.label, false).First();
                            menuL2ScrollBar.Width += (containerVisiblePercentage.ElementAt(i).Value[2] - containerVisiblePercentage.ElementAt(i).Value[1]) * body.Width / 100;
                        }
                    }
                } while (body != null);
                menuL2.Refresh();
            }
            else
            {
                int i = -1;
                Control left, body;

                //Calcolo la parte sinistra della barra
                do
                {
                    i++;
                    left = null;
                    NavItem leftControlNavItem = NavigationMenu.items.Find(item => item.containerId == containerVisiblePercentage.ElementAt(i).Key);
                    if (leftControlNavItem != null)
                    {
                        left = menuL1.Controls.Find(leftControlNavItem.label, false).First();
                        menuL1ScrollBar.Location = new Point(left.Location.X + containerVisiblePercentage.ElementAt(i).Value[1] * left.Width / 100, menuL1ScrollBar.Location.Y);
                        menuL1ScrollBar.Width = (containerVisiblePercentage.ElementAt(i).Value[2] - containerVisiblePercentage.ElementAt(i).Value[1]) * left.Width / 100;
                    }
                } while ((left == null) && (i < containerVisiblePercentage.Count - 1));


                //Calcolo il resto
                do
                {
                    body = null;
                    if (i < containerVisiblePercentage.Count - 1)
                    {
                        i++;
                        NavItem bodyControlNavItem = NavigationMenu.items.Find(item => item.containerId == containerVisiblePercentage.ElementAt(i).Key);
                        if (bodyControlNavItem != null)
                        {
                            body = menuL1.Controls.Find(bodyControlNavItem.label, false).First();
                            menuL1ScrollBar.Width += (containerVisiblePercentage.ElementAt(i).Value[2] - containerVisiblePercentage.ElementAt(i).Value[1]) * body.Width / 100;
                        }
                    }
                } while (body != null);
                menuL1.Refresh();
            }
        }

        protected override void LoginMenu_Closed(object sender, EventArgs e)
        {
            base.LoginMenu_Closed(sender, e);
            if ((menuL2 != null) && (!menuL2.IsDisposed))
            {
                menuL2.Invalidate();
            }
        }
    }
}
