﻿using WindowsUI.UI;

namespace WindowsUI.Footer
{
    partial class FooterUC
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.loginMenu = new WindowsUI.Footer.LoginMenu();
            this.appInformation = new WindowsUI.Footer.AppInformation();
            this.clockDateTime = new WindowsUI.Footer.ClockDateTime();
            this.SuspendLayout();
            // 
            // loginMenu
            // 
            this.loginMenu.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.loginMenu.Font = new System.Drawing.Font("Open Sans", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.loginMenu.Location = new System.Drawing.Point(944, 40);
            this.loginMenu.Name = "loginMenu";
            this.loginMenu.Size = new System.Drawing.Size(80, 80);
            this.loginMenu.TabIndex = 0;
            // 
            // appInformation
            // 
            this.appInformation.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.appInformation.AppName = "Nome del banco";
            this.appInformation.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.appInformation.Location = new System.Drawing.Point(17, 55);
            this.appInformation.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.appInformation.Name = "appInformation";
            this.appInformation.Size = new System.Drawing.Size(125, 40);
            this.appInformation.TabIndex = 18;
            this.appInformation.Click += new System.EventHandler(this.appInformation_Click);
            // 
            // clockDateTime
            // 
            this.clockDateTime.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.clockDateTime.BackColor = System.Drawing.Color.Transparent;
            this.clockDateTime.Font = new System.Drawing.Font("Open Sans", 22F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.clockDateTime.Location = new System.Drawing.Point(841, 40);
            this.clockDateTime.LoginMenuOpened = false;
            this.clockDateTime.Margin = new System.Windows.Forms.Padding(0);
            this.clockDateTime.Name = "clockDateTime";
            this.clockDateTime.Size = new System.Drawing.Size(100, 80);
            this.clockDateTime.TabIndex = 17;
            // 
            // FooterUC
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.Controls.Add(this.loginMenu);
            this.Controls.Add(this.appInformation);
            this.Controls.Add(this.clockDateTime);
            this.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(0);
            this.Name = "FooterUC";
            this.Size = new System.Drawing.Size(1024, 120);
            this.Click += new System.EventHandler(this.FooterUC_Click);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.Footer_Paint);
            this.ResumeLayout(false);

        }

        #endregion
        private ClockDateTime clockDateTime;
        private AppInformation appInformation;
        private LoginMenu loginMenu;
    }
}
