﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsUI.Footer
{
    public partial class ClockDateTime : UserControl
    {
        private Timer timeTimer;
        public bool LoginMenuOpened { get; set; }

        public ClockDateTime()
        {
            InitializeComponent();

            timeTimer = new Timer();
            timeTimer.Tick += TimeTimer_Tick;
            timeTimer.Interval = 10000;
            timeTimer.Enabled = true;

            //TODO Il formato della data e dell'ora potresti ricavarlo dalla cultura
            timeLabel.Text = DateTime.Now.ToString("HH:mm");
            dateLabel.Text = DateTime.Now.ToString("dd/MM/yyyy");
        }

        private void TimeTimer_Tick(object sender, EventArgs e)
        {
            timeLabel.Text = DateTime.Now.ToString("HH:mm");
            dateLabel.Text = DateTime.Now.ToString("dd/MM/yyyy");
        }

        private void timeLabel_Click(object sender, EventArgs e)
        {
            this.OnClick(e);
        }

        private void dateLabel_Click(object sender, EventArgs e)
        {
            this.OnClick(e);
        }

        private void ClockDateTime_Paint(object sender, PaintEventArgs e)
        {
            if (LoginMenuOpened)
            {
                using (Bitmap image = new Bitmap(@"Resources\ShadowLoginMenu.png"))
                {
                    e.Graphics.DrawImage(image, new Rectangle(0,0,this.Width, this.Height));
                }
            }
        }
    }
}
