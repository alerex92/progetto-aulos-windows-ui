﻿namespace WindowsUI.MainContent
{
    public partial class ExampleContainer : WinContainer
    {

        public ExampleContainer()
        {
            InitializeComponent();
            ExpandEnabled = true;
            expandedWidth = 900;
            collapsedWidth = this.Width;
        }

        private void button1_Click(object sender, System.EventArgs e)
        {
            this.Expand();
        }

        private void button2_Click(object sender, System.EventArgs e)
        {
            this.Collapse();
        }

        private void button4_Click(object sender, System.EventArgs e)
        {
            ShowToaster("Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor.");
        }

        private void button3_Click(object sender, System.EventArgs e)
        {
            ContainerConfigurationPopup conf = new ContainerConfigurationPopup();
            ShowModal(conf);
        }
    }
}
