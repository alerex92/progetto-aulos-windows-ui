﻿using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace WindowsUI.MainContent
{
    public partial class ContainerToaster : UserControl
    {
        [Description("Change the message of the toaster"), Category("Aulos")]
        public string Message { get { return message.Text; } set { message.Text = value; } }

        public ContainerToaster()
        {
            InitializeComponent();
        }

        private void ContainerToaster_Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.DrawRectangle(new Pen(ColorTranslator.FromHtml("#6699FF")), 0, 0, this.Width - 1, this.Height - 1);
        }
    }
}
