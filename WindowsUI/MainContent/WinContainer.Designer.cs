﻿namespace WindowsUI.MainContent
{
    partial class WinContainer
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.containerName = new System.Windows.Forms.Label();
            this.header = new System.Windows.Forms.Panel();
            this.body = new System.Windows.Forms.Panel();
            this.operatorMessagePanel = new System.Windows.Forms.Panel();
            this.operatorMessageLabel = new System.Windows.Forms.Label();
            this.header.SuspendLayout();
            this.operatorMessagePanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // containerName
            // 
            this.containerName.AutoSize = true;
            this.containerName.Font = new System.Drawing.Font("Open Sans", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.containerName.Location = new System.Drawing.Point(10, 8);
            this.containerName.Name = "containerName";
            this.containerName.Size = new System.Drawing.Size(133, 24);
            this.containerName.TabIndex = 0;
            this.containerName.Text = "Container Title";
            // 
            // header
            // 
            this.header.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(176)))), ((int)(((byte)(207)))), ((int)(((byte)(254)))));
            this.header.Controls.Add(this.containerName);
            this.header.Dock = System.Windows.Forms.DockStyle.Top;
            this.header.Location = new System.Drawing.Point(0, 0);
            this.header.Margin = new System.Windows.Forms.Padding(0);
            this.header.Name = "header";
            this.header.Size = new System.Drawing.Size(1200, 40);
            this.header.TabIndex = 0;
            // 
            // body
            // 
            this.body.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(230)))));
            this.body.Dock = System.Windows.Forms.DockStyle.Fill;
            this.body.Location = new System.Drawing.Point(0, 80);
            this.body.Margin = new System.Windows.Forms.Padding(0);
            this.body.Name = "body";
            this.body.Size = new System.Drawing.Size(1200, 480);
            this.body.TabIndex = 1;
            // 
            // operatorMessagePanel
            // 
            this.operatorMessagePanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(230)))));
            this.operatorMessagePanel.Controls.Add(this.operatorMessageLabel);
            this.operatorMessagePanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.operatorMessagePanel.Location = new System.Drawing.Point(0, 40);
            this.operatorMessagePanel.Name = "operatorMessagePanel";
            this.operatorMessagePanel.Size = new System.Drawing.Size(1200, 40);
            this.operatorMessagePanel.TabIndex = 2;
            // 
            // operatorMessageLabel
            // 
            this.operatorMessageLabel.BackColor = System.Drawing.Color.Transparent;
            this.operatorMessageLabel.Location = new System.Drawing.Point(75, 11);
            this.operatorMessageLabel.Margin = new System.Windows.Forms.Padding(0);
            this.operatorMessageLabel.Name = "operatorMessageLabel";
            this.operatorMessageLabel.Size = new System.Drawing.Size(133, 19);
            this.operatorMessageLabel.TabIndex = 0;
            this.operatorMessageLabel.Text = "Operator Message";
            this.operatorMessageLabel.TextChanged += new System.EventHandler(this.OperatorMessageLabel_TextChanged);
            // 
            // WinContainer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this.body);
            this.Controls.Add(this.operatorMessagePanel);
            this.Controls.Add(this.header);
            this.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(0);
            this.Name = "WinContainer";
            this.Size = new System.Drawing.Size(1200, 560);
            this.Load += new System.EventHandler(this.WinContainer_Load);
            this.header.ResumeLayout(false);
            this.header.PerformLayout();
            this.operatorMessagePanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label containerName;
        protected System.Windows.Forms.Panel body;
        private System.Windows.Forms.Label operatorMessageLabel;
        protected System.Windows.Forms.Panel header;
        protected System.Windows.Forms.Panel operatorMessagePanel;
    }
}
