﻿namespace WindowsUI.MainContent
{
    partial class ExampleContainer
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.listModule1 = new WindowsUI.MainContent.ListModule();
            this.searchModule1 = new WindowsUI.MainContent.SearchModule();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.body.SuspendLayout();
            this.header.SuspendLayout();
            this.operatorMessagePanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // body
            // 
            this.body.Controls.Add(this.listModule1);
            this.body.Controls.Add(this.searchModule1);
            this.body.Location = new System.Drawing.Point(0, 40);
            this.body.Size = new System.Drawing.Size(600, 502);
            // 
            // header
            // 
            this.header.Controls.Add(this.button2);
            this.header.Controls.Add(this.button1);
            this.header.Size = new System.Drawing.Size(600, 40);
            this.header.Controls.SetChildIndex(this.button1, 0);
            this.header.Controls.SetChildIndex(this.button2, 0);
            // 
            // operatorMessagePanel
            // 
            this.operatorMessagePanel.Controls.Add(this.button3);
            this.operatorMessagePanel.Controls.Add(this.button4);
            this.operatorMessagePanel.Size = new System.Drawing.Size(600, 40);
            this.operatorMessagePanel.Controls.SetChildIndex(this.button4, 0);
            this.operatorMessagePanel.Controls.SetChildIndex(this.button3, 0);
            // 
            // listModule1
            // 
            this.listModule1.BackColor = System.Drawing.Color.Transparent;
            this.listModule1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listModule1.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.listModule1.HeaderActive = true;
            this.listModule1.ImportantModule = false;
            this.listModule1.Location = new System.Drawing.Point(0, 143);
            this.listModule1.ModuleTitle = "List";
            this.listModule1.Name = "listModule1";
            this.listModule1.Size = new System.Drawing.Size(600, 359);
            this.listModule1.TabIndex = 0;
            // 
            // searchModule1
            // 
            this.searchModule1.BackColor = System.Drawing.Color.Transparent;
            this.searchModule1.Dock = System.Windows.Forms.DockStyle.Top;
            this.searchModule1.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.searchModule1.HeaderActive = false;
            this.searchModule1.ImportantModule = true;
            this.searchModule1.Location = new System.Drawing.Point(0, 0);
            this.searchModule1.ModuleTitle = "Header";
            this.searchModule1.Name = "searchModule1";
            this.searchModule1.Size = new System.Drawing.Size(600, 143);
            this.searchModule1.TabIndex = 1;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(432, 5);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(79, 29);
            this.button1.TabIndex = 2;
            this.button1.Text = "Expand";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(517, 5);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(79, 29);
            this.button2.TabIndex = 2;
            this.button2.Text = "Collapse";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(517, 6);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(79, 29);
            this.button3.TabIndex = 3;
            this.button3.Text = "Modal";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(432, 6);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(79, 29);
            this.button4.TabIndex = 4;
            this.button4.Text = "Toaster";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // ExampleContainer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ContainerName = "Example";
            this.Name = "ExampleContainer";
            this.OperatorMessageActive = false;
            this.Size = new System.Drawing.Size(600, 542);
            this.body.ResumeLayout(false);
            this.header.ResumeLayout(false);
            this.header.PerformLayout();
            this.operatorMessagePanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private ListModule listModule1;
        private SearchModule searchModule1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
    }
}
