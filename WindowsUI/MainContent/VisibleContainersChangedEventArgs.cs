﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsUI.MainContent
{
    public class VisibleContainersChangedEventArgs : EventArgs
    {
        public Dictionary<string, int[]> ContainerVisiblePercentage { get; set; }
        public string CentralContainer { get; set; }
        //public bool First { get; set; }
    }

    public delegate void VisibleContainersChangedHandler(object sender, VisibleContainersChangedEventArgs e);
}
