﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using BackEndBridge;
using Chromium;
using Chromium.Event;
using Chromium.WebBrowser;
using Chromium.WebBrowser.Event;
using WebSocketSharp;

namespace WindowsUI.MainContent
{
    public partial class WebContainer : Container
    {
        public ChromiumWebBrowser CFXBrowser { get; }

        public object WindowStyles { get; private set; }

        public Bridge AulosBackEndBridge;

        private bool loaded;
        private List<string> BackedUpMethods;

        public WebContainer()
        {
            InitializeComponent();

            if (!CfxRuntime.LibrariesLoaded)
            {
                CfxRuntime.LibCefDirPath = AppDomain.CurrentDomain.BaseDirectory + "cef";
                CfxRuntime.LibCfxDirPath = AppDomain.CurrentDomain.BaseDirectory + "cef";
                ChromiumWebBrowser.OnBeforeCfxInitialize += ChromiumWebBrowser_OnBeforeCfxInitialize;
                ChromiumWebBrowser.Initialize();
            }

            CFXBrowser = new ChromiumWebBrowser()
            {
                Dock = DockStyle.Fill,
                Margin = new Padding(0),
                Padding = new Padding(0),
            };

            /*try //E' in un try catch perchè altrimenti il designer da un errore nel main layout. Ho provato anche altri
            {   //metodi (come il designMode) ma non sembravano funzionare
                CFXBrowser.ContextMenuHandler.OnBeforeContextMenu += ChromiumWebBrowser_OnBeforeContextMenu;
                CFXBrowser.ContextMenuHandler.OnContextMenuCommand += ChromiumWebBrowser_OnContextMenuCommand;
            }
            catch (Exception e) { }*/
            CFXBrowser.ContextMenuHandler.OnBeforeContextMenu += ChromiumWebBrowser_OnBeforeContextMenu;
            CFXBrowser.ContextMenuHandler.OnContextMenuCommand += ChromiumWebBrowser_OnContextMenuCommand;

            CFXBrowser.LifeSpanHandler.OnBeforePopup += OnBeforePopup;

            AulosBackEndBridge = new Bridge(CFXBrowser);
            CFXBrowser.GlobalObject.Add("AulosBackEndBridge", AulosBackEndBridge);
            AulosBackEndBridge.OnAppLoaded += AppLoaded;

            this.Controls.Add(CFXBrowser);
            CFXBrowser.BringToFront();

            BackedUpMethods = new List<string>();
        }

        private void AppLoaded(object source, AppLoadedHandlerArgs e)
        {
            loaded = true;
            System.Diagnostics.Debug.WriteLine(this.Name + " loaded");
            foreach (string methName in BackedUpMethods.ToArray())
            {
                try
                {
                    this.GetType().GetMethod(methName).Invoke(this, null);
                    System.Diagnostics.Debug.WriteLine(this.Name + " " + methName + " executed after load");
                }
                catch (Exception exc)
                {
                    System.Diagnostics.Debug.WriteLine(exc.ToString());
                }
                BackedUpMethods.Remove(methName);
            }
        }

        private void OnBeforePopup(object sender, CfxOnBeforePopupEventArgs e)
        {
            Invoke(new MethodInvoker(delegate
            {
                Form popupBrowserForm = new Form()
                {
                    Size = this.Size,
                    Name = this.Name + "Popup",
                    Text = this.Name,
                };

                WebContainer popupBrowser = new WebContainer()
                {
                    Margin = new Padding(0),
                    Name = this.Name,
                    Dock = DockStyle.Fill
                };
                if (!e.TargetFrameName.IsNullOrEmpty())
                {
                    popupBrowserForm.Text = e.TargetFrameName;
                }
                popupBrowser.CFXBrowser.LoadUrl(e.TargetUrl);
                popupBrowserForm.Controls.Add(popupBrowser);
                popupBrowserForm.Show();
            }));
            e.SetReturnValue(true);
        }

        private static void ChromiumWebBrowser_OnBeforeCfxInitialize(OnBeforeCfxInitializeEventArgs e)
        {
            e.Settings.BrowserSubprocessPath = AppDomain.CurrentDomain.BaseDirectory + "CFXSubprocess.exe";
        }

        private void ChromiumWebBrowser_OnBeforeContextMenu(object sender, CfxOnBeforeContextMenuEventArgs e)
        {
            e.Model.InsertItemAt(0, 25600, "Dev Tools");
            e.Model.InsertItemAt(0, 25601, "Reload");
        }

        private void ChromiumWebBrowser_OnContextMenuCommand(object sender, CfxOnContextMenuCommandEventArgs e)
        {
            if (e.CommandId == 25600)
            {
                CfxWindowInfo windowInfo = new CfxWindowInfo();

                windowInfo.Style = (WindowStyle.WS_OVERLAPPEDWINDOW | WindowStyle.WS_CLIPCHILDREN | WindowStyle.WS_CLIPSIBLINGS | WindowStyle.WS_VISIBLE);
                windowInfo.ParentWindow = e.Browser.Host.WindowHandle;
                windowInfo.WindowName = "Dev Tools";
                windowInfo.X = 200;
                windowInfo.Y = 200;
                windowInfo.Width = 400;
                windowInfo.Height = 300;

                CFXBrowser.BrowserHost.ShowDevTools(windowInfo, new CfxClient(), new CfxBrowserSettings(), null);
            }
            if (e.CommandId == 25601)
            {
                CFXBrowser.Browser.Reload();
            }
        }

        public override void Freeze()
        {
            base.Freeze();
            if (loaded)
            {
                AulosBackEndBridge.FreezeContainer(Name);
            }
            else
            {
                BackedUpMethods.Add("Freeze");
                System.Diagnostics.Debug.WriteLine(this.Name + " Freeze" + " backed");
            }
        }

        public override void UnFreeze()
        {
            base.UnFreeze();
            if (loaded)
            {   
                AulosBackEndBridge.UnfreezeContainer(Name);
            }
            else
            {
                BackedUpMethods.Add("UnFreeze");
                System.Diagnostics.Debug.WriteLine(this.Name + " UnFreeze" + " backed");
            }
        }

        public void UnloadLibraries()
        {
            if (CfxRuntime.LibrariesLoaded)
            {
                CfxRuntime.Shutdown();
            }
        }
    }
}
