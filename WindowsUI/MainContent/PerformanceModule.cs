﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsUI.MainContent
{
    public partial class PerformanceModule : ContainerModule
    {
        public PerformanceModule()
        {
            InitializeComponent();
        }

        private void PerformanceModule_Load(object sender, EventArgs e)
        {
            Control tempControl;
            int i = 1;
            do
            {
                tempControl = new Button();
                tempControl.Location = new Point(button.Location.X + i*(20), button.Location.Y);
                tempControl.Width = button.Width;
                tempControl.Name = "button" + i;
                tempControl.Text = button.Text;
                tempControl.Visible = true;
                i++;
                body.Controls.Add(tempControl);
            } while (tempControl.Location.X + tempControl.Width < this.Width - 80);
            i = 1;
            do
            {
                tempControl = new CheckBox();
                tempControl.Location = new Point(checkBox.Location.X + i * (20), checkBox.Location.Y);
                tempControl.Width = checkBox.Width;
                tempControl.Name = "checkBox" + i;
                tempControl.Text = checkBox.Text;
                tempControl.Visible = true;
                i++;
                body.Controls.Add(tempControl);
            } while (tempControl.Location.X + tempControl.Width < this.Width - 80);
            i = 1;
            do
            {
                tempControl = new ComboBox();
                tempControl.Location = new Point(comboBox.Location.X + i * (20), comboBox.Location.Y);
                tempControl.Width = comboBox.Width;
                tempControl.Name = "comboBox" + i;
                tempControl.Text = comboBox.Text;
                tempControl.Visible = true;
                i++;
                body.Controls.Add(tempControl);
            } while (tempControl.Location.X + tempControl.Width < this.Width - 80);
            i = 1;
            do
            {
                tempControl = new Label();
                tempControl.Location = new Point(label.Location.X + i * (20), label.Location.Y);
                tempControl.Width = label.Width;
                tempControl.Name = "label" + i;
                tempControl.Text = label.Text;
                tempControl.Visible = true;
                i++;
                body.Controls.Add(tempControl);
            } while (tempControl.Location.X + tempControl.Width < this.Width - 80);
            i = 1;
            /*do
            {
                tempControl = new PictureBox();
                tempControl.Location = new Point(pictureBox.Location.X + i * (20), pictureBox.Location.Y);
                tempControl.Width = pictureBox.Width;
                tempControl.Name = "pictureBox" + i;
                tempControl.Text = pictureBox.Text;
                ((PictureBox) tempControl).Image = pictureBox.Image;
                ((PictureBox)tempControl).SizeMode = PictureBoxSizeMode.StretchImage;
                tempControl.Visible = true;
                i++;
                body.Controls.Add(tempControl);
            } while (tempControl.Location.X + tempControl.Width < this.Width - 80);*/
            do
            {
                tempControl = new Panel();
                tempControl.Location = new Point(panel.Location.X + i * (20), panel.Location.Y);
                tempControl.Width = panel.Width;
                tempControl.Name = "panel" + i;
                tempControl.Text = panel.Text;
                tempControl.Visible = true;
                tempControl.BackColor = panel.BackColor;
                ((Panel) tempControl).BorderStyle = BorderStyle.Fixed3D;
                i++;
                body.Controls.Add(tempControl);
            } while (tempControl.Location.X + tempControl.Width < this.Width - 80);
        }
    }
}
