﻿using System;
using System.Windows.Forms;
using WindowsUI.UI;

namespace WindowsUI.MainContent
{
    public partial class Container : UserControl
    {
        /* Qui sono da inserire parametri e metodi che dovrebbero essere comuni ad entrambi i tipi di container
         * ad esempio i freeze/unfreeze (anche se implementati diversamente tra i due) o i metodi expand/collapse
         * (che invece sono uguali)
        */

        protected bool ExpandEnabled = false;
        protected bool Expanded = false;
        public bool Freezed { get; protected set; }
        public int expandedWidth = 0;
        public int collapsedWidth = 0;

        private Timer expandAnimationTimer;
        private Timer collapseAnimationTimer;

        private int animationProgress = 0;
        private int animationDuration = 500;
        private int widthDifference;

        public virtual string ContainerName { get; set; }

        public event ContainerSizeChangedHandler ContainerSizeChanged;

        public Container()
        {
            InitializeComponent();

            expandAnimationTimer = new Timer();
            expandAnimationTimer.Tick += ExpandAnimation_Tick;
            expandAnimationTimer.Interval = 20;
            collapseAnimationTimer = new Timer();
            collapseAnimationTimer.Tick += CollapseAnimation_Tick;
            collapseAnimationTimer.Interval = 20;
        }

        public virtual void Freeze()
        {
            System.Diagnostics.Debug.WriteLine(this.Name + " Freze");
            Freezed = true;
        }

        public virtual void UnFreeze()
        {
            System.Diagnostics.Debug.WriteLine(this.Name + " UnFreeze");
            Freezed = false;
        }

        public int Expand()
        {
            if (collapseAnimationTimer.Enabled)
            {
                collapseAnimationTimer.Stop();
            }
            if (!expandAnimationTimer.Enabled)
            {
                expandAnimationTimer.Start();
            }
            return expandedWidth;
        }

        public int Collapse()
        {
            if (expandAnimationTimer.Enabled)
            {
                expandAnimationTimer.Stop();
            }
            if (!collapseAnimationTimer.Enabled)
            {
                collapseAnimationTimer.Start();
            }
            return collapsedWidth;
        }

        private void ExpandAnimation_Tick(object sender, EventArgs e)
        {
            if (this.Width < expandedWidth - 10)
            {
                widthDifference = (int)Animation.EaseInOut(animationProgress, collapsedWidth, expandedWidth - collapsedWidth, animationDuration) - this.Width;
                this.Width += widthDifference;
                animationProgress += 20;
            }
            else
            {
                widthDifference = expandedWidth - this.Width;
                this.Width = expandedWidth;
                animationProgress = animationDuration;
                Expanded = true;
                expandAnimationTimer.Stop();
            }
            ContainerSizeChangedEventArgs args = new ContainerSizeChangedEventArgs()
            {
                WidthDifference = widthDifference
            };
            ContainerSizeChanged?.Invoke(this, args);
        }

        private void CollapseAnimation_Tick(object sender, EventArgs e)
        {
            if (this.Width > collapsedWidth + 10)
            {
                widthDifference = (int)Animation.EaseInOut(animationProgress, collapsedWidth, expandedWidth - collapsedWidth, animationDuration) - this.Width;
                this.Width += widthDifference;
                animationProgress -= 20;
            }
            else
            {
                widthDifference = collapsedWidth - this.Width;
                this.Width = collapsedWidth;
                animationProgress = 0;
                Expanded = false;
                collapseAnimationTimer.Stop();
            }
            ContainerSizeChangedEventArgs args = new ContainerSizeChangedEventArgs()
            {
                WidthDifference = widthDifference
            };
            ContainerSizeChanged?.Invoke(this, args);
        }
    }
}
