﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Windows.Forms;

namespace WindowsUI.MainContent
{
    public partial class ContainerPopup : UserControl
    {
        [Description("Change the Operator message"), Category("Aulos")]
        public string Title { get { return modalTitle.Text; } set { modalTitle.Text = value; } }

        int shadowDepth = 9;
        int shadowMaxOpacity = 125;
        Color shadowColor = Color.Black;
        Rectangle rect;

        PictureBox backgroundScreen;

        //Parametri esterni
        public WinContainer ParentContainer { get; set;}

        public ContainerPopup()
        {
            InitializeComponent();

            this.SetStyle(ControlStyles.OptimizedDoubleBuffer, true);
            this.SetStyle(ControlStyles.AllPaintingInWmPaint, true);
            this.SetStyle(ControlStyles.UserPaint, true);

            body.MaximumSize = MaximumSize;
            MaximumSize = new Size(MaximumSize.Width + (2 * shadowDepth), MaximumSize.Height + (2 * shadowDepth));
        }

        private void header_Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.DrawLine(new Pen(ColorTranslator.FromHtml("#B3B3B3")), 0,
                    modalTitle.Location.Y + modalTitle.Height, modalTitle.Location.X + this.Width, modalTitle.Location.Y + modalTitle.Height);
        }

        private void ContainerPopup_Paint(object sender, PaintEventArgs e)
        {
            rect = new Rectangle(new Point(0, 0), this.Size);
            RectangleDropShadow(e.Graphics, rect, shadowColor, shadowDepth, shadowMaxOpacity);
        }

        private void Body_Resize(object sender, EventArgs e)
        {
            body.Location = new Point(shadowDepth, shadowDepth);
            this.Size = new Size(body.Width + (2 * shadowDepth), body.Height + (2 * shadowDepth));
            this.Invalidate();
        }

        public void Add()
        {
            /*Bitmap screenshot = new Bitmap(ParentContainer.Width, ParentContainer.Height, PixelFormat.Format32bppArgb);
            ParentContainer.DrawToBitmap(screenshot, ParentContainer.ClientRectangle);
            backgroundScreen = new PictureBox()
            {
                Image = screenshot,
                //Image = Image.FromFile(@"C:\Users\aless\Desktop\test.png"),
                SizeMode = PictureBoxSizeMode.StretchImage,
                Size = new Size(ParentContainer.Width, ParentContainer.Height),
                Location = new Point(0, 0),
            };
            ParentContainer.Controls.Add(backgroundScreen);
            backgroundScreen.BringToFront();
            darkOverlay = new Panel()
            {
                BackColor = Color.FromArgb(100, Color.Black),
                Size = new Size(ParentContainer.Width - ParentContainer.Padding.Left, ParentContainer.Height - 40),
                Location = new Point(ParentContainer.Padding.Left, 40),
            };
            //darkOverlay.Paint += DarkOverlayOnPaint;
            backgroundScreen.Controls.Add(darkOverlay);
            darkOverlay.Controls.Add(this);
            //ParentContainer.Controls.Add(this);
            //this.BringToFront();*/

            Bitmap screenshot = new Bitmap(ParentContainer.Width, ParentContainer.Height, PixelFormat.Format32bppArgb);
            ParentContainer.DrawToBitmap(screenshot, ParentContainer.ClientRectangle);
            using (var canvas = Graphics.FromImage(screenshot))
            {
                canvas.InterpolationMode = InterpolationMode.HighQualityBicubic;
                Rectangle rect = new Rectangle(new Point(0, 0), screenshot.Size);
                canvas.FillRectangle(new SolidBrush(Color.FromArgb(100, Color.Black)), rect);
                canvas.Save();
            }
            backgroundScreen = new PictureBox()
            {
                Image = screenshot,
                //Image = Image.FromFile(@"C:\Users\aless\Desktop\test.png"),
                SizeMode = PictureBoxSizeMode.StretchImage,
                Size = new Size(ParentContainer.Width, ParentContainer.Height),
                Location = new Point(0, 0),
            };
            ParentContainer.Controls.Add(backgroundScreen);
            backgroundScreen.BringToFront();
            backgroundScreen.Controls.Add(this);
        }

        /*Brush black = new SolidBrush(Color.FromArgb(100, Color.Black));
        private void DarkOverlayOnPaint(object sender, PaintEventArgs e)
        {
            Rectangle rect2 = new Rectangle(darkOverlay.Location, darkOverlay.Size);
            e.Graphics.FillRectangle(black, rect2);
        }*/

        protected void CustomDispose()
        {
            backgroundScreen.Dispose();
        }

        private void RectangleDropShadow(Graphics tg, Rectangle rc, Color shadowColor, int depth, int maxOpacity)
        {
            //calculate the opacities
            Color darkShadow = Color.FromArgb(maxOpacity, shadowColor);
            Color lightShadow = Color.FromArgb(0, shadowColor);
            //Create a brush that will create a softshadow circle
            GraphicsPath gp = new GraphicsPath();
            gp.AddEllipse(0, 0, 2 * depth, 2 * depth);
            PathGradientBrush pgb = new PathGradientBrush(gp);
            pgb.CenterColor = darkShadow;
            pgb.SurroundColors = new Color[] { lightShadow };
            //generate a softshadow pattern that can be used to paint the shadow
            Bitmap patternbm = new Bitmap(2 * depth, 2 * depth);
            Graphics g = Graphics.FromImage(patternbm);
            g.FillEllipse(pgb, 0, 0, 2 * depth, 2 * depth);
            g.Dispose();
            pgb.Dispose();
            //SolidBrush sb = new SolidBrush(Color.FromArgb(maxOpacity, shadowColor));
            //tg.FillRectangle(sb, rc.Left + depth, rc.Top + depth, rc.Width - (2 * depth), rc.Height - (2 * depth));
            //sb.Dispose();
            //top left corner
            tg.DrawImage(patternbm, new Rectangle(rc.Left, rc.Top, depth, depth), 0, 0, depth, depth, GraphicsUnit.Pixel);
            //top side
            tg.DrawImage(patternbm, new Rectangle(rc.Left + depth, rc.Top, rc.Width - (2 * depth), depth), depth, 0, 1, depth, GraphicsUnit.Pixel);
            //top right corner
            tg.DrawImage(patternbm, new Rectangle(rc.Right - depth, rc.Top, depth, depth), depth, 0, depth, depth, GraphicsUnit.Pixel);
            //right side
            tg.DrawImage(patternbm, new Rectangle(rc.Right - depth, rc.Top + depth, depth, rc.Height - (2 * depth)), depth, depth, depth, 1, GraphicsUnit.Pixel);
            //bottom left corner
            tg.DrawImage(patternbm, new Rectangle(rc.Right - depth, rc.Bottom - depth, depth, depth), depth, depth, depth, depth, GraphicsUnit.Pixel);
            //bottom side
            tg.DrawImage(patternbm, new Rectangle(rc.Left + depth, rc.Bottom - depth, rc.Width - (2 * depth), depth), depth, depth, 1, depth, GraphicsUnit.Pixel);
            //bottom left corner
            tg.DrawImage(patternbm, new Rectangle(rc.Left, rc.Bottom - depth, depth, depth), 0, depth, depth, depth, GraphicsUnit.Pixel);
            //left side
            tg.DrawImage(patternbm, new Rectangle(rc.Left, rc.Top + depth, depth, rc.Height - (2 * depth)), 0, depth, depth, 1, GraphicsUnit.Pixel);
            patternbm.Dispose();
        }
    }
}
