﻿using System;

namespace WindowsUI.MainContent
{
    public partial class ContainerConfigurationPopup : ContainerPopup
    {
        public ContainerConfigurationPopup()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.CustomDispose();
        }
    }
}
