﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsUI.MainContent
{
    public class ContainerSizeChangedEventArgs : EventArgs
    {
        public int WidthDifference { get; set; }
    }

    public delegate void ContainerSizeChangedHandler(object sender, ContainerSizeChangedEventArgs e);
}
