﻿using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace WindowsUI.MainContent
{
    public partial class ContainerModule : UserControl
    {

        [Description("Change the title of the module"), Category("Aulos")]
        public string ModuleTitle { get { return moduleTitle.Text; } set { moduleTitle.Text = value; } }

        [Description("Display the header"), Category("Aulos")]
        public bool HeaderActive { get { return header.Visible; } set { header.Visible = value; } }

        [Description("Set the importancy of a module"), Category("Aulos")]
        public bool ImportantModule { get { return !body.AutoScroll; } set { body.AutoScroll = !value; } }

        bool panning;
        Point startPoint;

        public MouseEventHandler BubbleMouseDown;
        public MouseEventHandler BubbleMouseMove;
        public MouseEventHandler BubbleMouseUp;
        bool parentPanning;
        Point mouseLocationAbsolute;

        public ContainerModule()
        {
            InitializeComponent();
            this.SetStyle(ControlStyles.OptimizedDoubleBuffer, true);
        }

        private void header_Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.DrawLine(new Pen(ColorTranslator.FromHtml("#B3B3B3")),moduleTitle.Location.X,
                    moduleTitle.Location.Y + moduleTitle.Height, moduleTitle.Location.X + this.Width - 60, moduleTitle.Location.Y + moduleTitle.Height);
        }

        private void body_MouseDown(object sender, MouseEventArgs e)
        {
            if(!ImportantModule)
            {
                panning = true;
                startPoint = e.Location;
            }
            else
            {
                mouseLocationAbsolute = this.PointToScreen(e.Location);
                BubbleMouseDown?.Invoke(this, new MouseEventArgs(e.Button, e.Clicks, mouseLocationAbsolute.X, mouseLocationAbsolute.Y, e.Delta));
                parentPanning = true;
            }
        }

        private void body_MouseMove(object sender, MouseEventArgs e)
        {
            if (panning)
            {
                Point currentPosition = e.Location;
                int deltaY = startPoint.Y - currentPosition.Y;
                startPoint = currentPosition;
                body.AutoScrollPosition = new Point(body.AutoScrollPosition.X, -body.AutoScrollPosition.Y + deltaY);
                body.Refresh();
            }
            else
            {
                if (parentPanning == true)
                {
                    mouseLocationAbsolute = this.PointToScreen(e.Location);                  
                    BubbleMouseMove?.Invoke(this, new MouseEventArgs(e.Button, e.Clicks, mouseLocationAbsolute.X, mouseLocationAbsolute.Y, e.Delta));
                }
            }
        }

        private void body_MouseUp(object sender, MouseEventArgs e)
        {
            if(panning == true)
            {
                panning = false;
            }
            else
            {
                if(parentPanning == true)
                {
                    BubbleMouseUp?.Invoke(this, e);
                    parentPanning = false;
                }
            }
        }

        private void header_MouseDown(object sender, MouseEventArgs e)
        {
            mouseLocationAbsolute = this.PointToScreen(e.Location);
            BubbleMouseDown?.Invoke(this, new MouseEventArgs(e.Button, e.Clicks, mouseLocationAbsolute.X, mouseLocationAbsolute.Y, e.Delta));
            parentPanning = true;
        }

        private void header_MouseMove(object sender, MouseEventArgs e)
        {
            if (parentPanning == true)
            {
                mouseLocationAbsolute = this.PointToScreen(e.Location);
                BubbleMouseMove?.Invoke(this, new MouseEventArgs(e.Button, e.Clicks, mouseLocationAbsolute.X, mouseLocationAbsolute.Y, e.Delta));
            }
        }

        private void header_MouseUp(object sender, MouseEventArgs e)
        {
            if (parentPanning == true)
            {
                BubbleMouseUp?.Invoke(this, e);
                parentPanning = false;
            }
        }
        //TODO: da fare la stessa cosa per header(container non del modulo) e operatormessage quando non mi servono più per i toaster e popup

        private void header_SizeChanged(object sender, System.EventArgs e)
        {
            this.Refresh();
        }
    }
}
