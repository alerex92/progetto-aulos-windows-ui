﻿namespace WindowsUI.MainContent
{
    partial class Example2Container
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.performanceModule1 = new WindowsUI.MainContent.PerformanceModule();
            this.body.SuspendLayout();
            this.SuspendLayout();
            // 
            // body
            // 
            this.body.Controls.Add(this.performanceModule1);
            this.body.Size = new System.Drawing.Size(900, 480);
            // 
            // header
            // 
            this.header.Size = new System.Drawing.Size(900, 40);
            // 
            // operatorMessagePanel
            // 
            this.operatorMessagePanel.Size = new System.Drawing.Size(900, 40);
            // 
            // performanceModule1
            // 
            this.performanceModule1.BackColor = System.Drawing.Color.Transparent;
            this.performanceModule1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.performanceModule1.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.performanceModule1.HeaderActive = true;
            this.performanceModule1.ImportantModule = true;
            this.performanceModule1.Location = new System.Drawing.Point(0, 0);
            this.performanceModule1.ModuleTitle = "Performance";
            this.performanceModule1.Name = "performanceModule1";
            this.performanceModule1.Size = new System.Drawing.Size(900, 480);
            this.performanceModule1.TabIndex = 0;
            // 
            // Example2Container
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "Example2Container";
            this.Size = new System.Drawing.Size(900, 560);
            this.body.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private PerformanceModule performanceModule1;
    }
}
