﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using WindowsUI.UI;
using BackEndBridge;
using WebSocketSharp;
using Timer = System.Windows.Forms.Timer;

namespace WindowsUI.MainContent
{
    public partial class ContentTape : UserControl
    {
        //private Dictionary<string, string> urlParam;
        //public Dictionary<string, string> UrlParam { set { urlParam = value; } }

        public GlobalConfiguration Configuration { private get; set; }
        public NavItems NavigationMenu { private get; set; }

        private bool panning;
        private Point startPoint;

        //private bool[] containerVisible;
        private Dictionary<string, int[]> containerVisiblePercentage; // [0] index, [1] percentage, [2] right (0) left(1) part
        //private Dictionary<string, int[]> containerVisiblePercentage2; // [0] index, [1] leftBound, [2] rightBound

        public event EventHandler ContainerLoaded;
        public event VisibleContainersChangedHandler VisibleContainersChanged;

        private int containerMargins = 20;

        //anche se non è ideale metto un int[] nel value di questo anche se mi serve solo un valore cioè la posizione,
        //lo faccio perchè così almeno ho un dizionario con valore variabile, in versioni future sicuramente sarà una cosa da 
        //moficare ma per ora mi permette di avere quello che mi serve senza modificare il codice già fatto
        //private Dictionary<string, int[]> virtualContainerPosition; 
        class VirtualContainerPosition
        {
            public string Name;
            public int Position;
        }
        private List<VirtualContainerPosition> virtualContainerPositions;

        private int[] virtualVisiblePosition; // [0] left limit, [1] right limit
        private string centralContainer = "";
        private int centralContainerIndex;

        private Timer rightScrollAnimationTimer;
        private Timer leftScrollAnimationTimer;
        private int start = 0;
        private int locationProgress = 0;
        private int animationDeltaX;
        private int animationDistance;
        private int easeDistance;
        private int animationProgress = 0;
        private int animationDuration = 500;
        private int allowedPercentage;

        public ContentTape()
        {
            InitializeComponent();

            this.SetStyle(ControlStyles.OptimizedDoubleBuffer, true);
            this.SetStyle(ControlStyles.AllPaintingInWmPaint, true);
            Padding = new Padding(0, containerMargins, containerMargins, containerMargins);
            //Padding = new Padding(0, containerMargins, containerMargins, containerMargins - SystemInformation.HorizontalScrollBarHeight);

            rightScrollAnimationTimer = new Timer();
            rightScrollAnimationTimer.Tick += new EventHandler(RightScrollAnimation_Tick);
            rightScrollAnimationTimer.Interval = 20;
            leftScrollAnimationTimer = new Timer();
            leftScrollAnimationTimer.Tick += new EventHandler(LeftScrollAnimation_Tick);
            leftScrollAnimationTimer.Interval = 20;
        }

        /*protected override void WndProc(ref System.Windows.Forms.Message m)
        {
            //ShowScrollBar(this.Handle, 1, false);
            ShowScrollBar(this.Handle, 0, false);
            //SetScrollState(ScrollableControl.ScrollStateHScrollVisible, false);
            //SetScrollState(ScrollableControl.ScrollStateFullDrag, true);
            base.WndProc(ref m);
        }

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool ShowScrollBar(IntPtr hWnd, int wBar, bool bShow);*/

        public void Setup()
        {
            //NavClient navClient = new NavClient(urlParam);
            //NavItems navigationMenu = await navClient.GetNavigationItems();
            //NavItems navigationMenu = JsonConvert.DeserializeObject<NavItems>(File.ReadAllText(Configuration.MenuItemsFile));
            List<NavItem> flatList = TreeFunctions.ToList(NavigationMenu.items);
            virtualContainerPositions = new List<VirtualContainerPosition>();
            virtualVisiblePosition = new int[] {0, this.Width};
            if (Configuration.SmallScreens)
            {
                containerMargins = 10;
            }
            int tempX = containerMargins;
            for (int i = 0; i < flatList.Count; i++)
            {
                Type containerType;
                if (flatList[i].path.IsNullOrEmpty())
                {
                    containerType = Type.GetType(flatList[i].type);
                }
                else
                {
                    string absolutePath = Path.GetFullPath(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, flatList[i].path));
                    Assembly asm = Assembly.LoadFile(absolutePath);
                    containerType = asm.GetType(flatList[i].type);
                }
                Container tempContainer = (Container) Activator.CreateInstance(containerType);
                tempContainer.Margin = new Padding(0);
                tempContainer.Name = flatList[i].containerId;
                tempContainer.ContainerName += ": " + flatList[i].label;
                this.Controls.Add(tempContainer);

                //Devo impostare qualche proprietà dopo l'add se no poi non sono mantenute
                if (containerType.IsEquivalentTo(typeof(WebContainer)))
                {
                    if (flatList[i].options != null)
                    {
                        if (!flatList[i].options.url.IsNullOrEmpty())
                        {
                            ((WebContainer)tempContainer).CFXBrowser.LoadUrl(flatList[i].options.url);
                        }
                        if (flatList[i].options.width != 0)
                        {
                            tempContainer.Width = flatList[i].options.width;
                        }
                    }
                }
                tempContainer.Height = this.Height - (2 * containerMargins);
                //tempContainer.Width = 780; // da eliminare
                virtualContainerPositions.Add(new VirtualContainerPosition() { Name = tempContainer.Name, Position = tempX });
                if (tempX > this.Width)
                {
                    tempContainer.Location = new Point(this.Width, containerMargins);
                }
                else
                {
                    tempContainer.Location = new Point(tempX, containerMargins);
                }
                
                tempX += tempContainer.Width + containerMargins;
               
                tempContainer.ContainerSizeChanged += Container_OnSizeChange;
                /*temp.BubbleMouseDown += ContentTape_MouseDown;
                temp.BubbleMouseMove += ContentTape_MouseMove;
                temp.BubbleMouseUp += ContentTape_MouseUp;*/
            }

            NavigationService.RegisterContentTape(this);

            //containerVisible = new bool[this.Controls.Count];
            containerVisiblePercentage = new Dictionary<string, int[]>();
            //containerVisiblePercentage2 = new Dictionary<string, int[]>();
            //ContainersOnScreen();

            //TODO che succede se il ContainerLoaded non è caricato? perdi l'evento?
            ContainerLoaded?.Invoke(this, EventArgs.Empty);

        }

        private void Container_OnSizeChange(object sender, ContainerSizeChangedEventArgs e)
        {
            MoveContent(e.WidthDifference, true, (Container)sender);
        }

        public void ContainersOnScreen(bool firstTime = false)
        {
            containerVisiblePercentage.Clear();
            //containerVisiblePercentage2.Clear();
            centralContainer = "";
            double tempPercentage = 0;
            int distanceFromCenter = Int32.MaxValue;
            int tempDistance = 0;
            int containerIndex = 0;
            int leftBound = 0;
            int rightBound = 100;
            /*foreach (Control container in this.Controls)
            {
                if ((container.Location.X >= 0) && (container.Location.X < this.Width))
                {
                    tempPercentage = Math.Ceiling((double) (this.Width - container.Location.X) * 100 / container.Width);
                    containerVisiblePercentage.Add(container.Name, new int[] {containerPosition, Math.Min(100, (int)tempPercentage)});
                    tempDistance = Math.Abs(container.Location.X + container.Width / 2 - this.Width / 2);
                    if (tempDistance < distanceFromCenter)
                    {
                        distanceFromCenter = tempDistance;
                        centralContainer = container.Name;
                    }
                }
                else if ((container.Location.X < 0) && (container.Location.X + container.Width > 0))
                {
                    tempPercentage = Math.Ceiling((double)(container.Location.X + container.Width) * 100 / container.Width);
                    containerVisiblePercentage.Add(container.Name, new int[] {containerPosition, Math.Min(100, (int)tempPercentage)});
                    tempDistance = Math.Abs(container.Location.X + container.Width / 2 - this.Width / 2);
                    if (tempDistance < distanceFromCenter)
                    {
                        distanceFromCenter = tempDistance;
                        centralContainer = container.Name;
                    }
                }
                else
                {
                    //containerVisiblePercentage.Add(container.Name, 0);
                }
                containerPosition++;
            }*/
            /*for (int i = 0; i < this.Controls.Count; i++)
            {
                Control container = this.Controls[i];
                if ((virtualContainerPositions[i].Position >= virtualVisiblePosition[0]) && (virtualContainerPositions[i].Position < virtualVisiblePosition[1]))
                {
                    tempPercentage = Math.Ceiling((double)(virtualVisiblePosition[1] - virtualContainerPositions[i].Position) * 100 / container.Width);
                    containerVisiblePercentage.Add(container.Name, new int[] { containerIndex, Math.Min(100, (int)tempPercentage), 1 });
                    tempDistance = Math.Abs(virtualContainerPositions[i].Position - virtualVisiblePosition[0] + container.Width / 2 - this.Width / 2);
                    if (tempDistance < distanceFromCenter)
                    {
                        distanceFromCenter = tempDistance;
                        centralContainer = container.Name;
                        centralContainerIndex = containerIndex;
                    }
                }
                else if ((virtualContainerPositions[i].Position < virtualVisiblePosition[0]) && (virtualContainerPositions[i].Position + container.Width > virtualVisiblePosition[1]))
                {
                    tempPercentage = 100 - Math.Ceiling((double)(container.Width - this.Width) * 100 / container.Width);
                    containerVisiblePercentage.Add(container.Name, new int[] { containerIndex, (int)tempPercentage, 1 });
                    tempDistance = Math.Abs(virtualContainerPositions[i].Position - virtualVisiblePosition[0] + container.Width / 2 - this.Width / 2);
                    if (tempDistance < distanceFromCenter)
                    {
                        distanceFromCenter = tempDistance;
                        centralContainer = container.Name;
                        centralContainerIndex = containerIndex;
                    }
                }
                else if ((virtualContainerPositions[i].Position < virtualVisiblePosition[0]) && (virtualContainerPositions[i].Position + container.Width > virtualVisiblePosition[0]))
                {
                    tempPercentage = Math.Ceiling((double)(virtualContainerPositions[i].Position - virtualVisiblePosition[0] + container.Width) * 100 / container.Width);
                    containerVisiblePercentage.Add(container.Name, new int[] { containerIndex, Math.Min(100, (int)tempPercentage), 0 });
                    tempDistance = Math.Abs(virtualContainerPositions[i].Position - virtualVisiblePosition[0] + container.Width / 2 - this.Width / 2);
                    if (tempDistance < distanceFromCenter)
                    {
                        distanceFromCenter = tempDistance;
                        centralContainer = container.Name;
                        centralContainerIndex = containerIndex;
                    }
                }
                containerIndex++;
            }*/
            for (int i = 0; i < this.Controls.Count; i++)
            {
                Container container = (Container) this.Controls[i];
                if (!(virtualContainerPositions[i].Position >= virtualVisiblePosition[1]) &&
                    !(virtualContainerPositions[i].Position + container.Width <= virtualVisiblePosition[0]))
                {
                    leftBound = virtualContainerPositions[i].Position - virtualVisiblePosition[0];
                    if (leftBound >= 0)
                    {
                        leftBound = 0;
                    }
                    else
                    {
                        leftBound = Math.Abs(leftBound * 100/container.Width);
                    }
                    rightBound = virtualVisiblePosition[1] - virtualContainerPositions[i].Position - container.Width;
                    if (rightBound >= 0)
                    {
                        rightBound = 100;
                    }
                    else
                    {
                        rightBound = 100 - Math.Abs(rightBound * 100 / container.Width);
                    }
                    tempDistance = Math.Abs(virtualContainerPositions[i].Position - virtualVisiblePosition[0] + container.Width / 2 - this.Width / 2);
                    if (tempDistance < distanceFromCenter)
                    {
                        distanceFromCenter = tempDistance;
                        centralContainer = container.Name;
                        centralContainerIndex = containerIndex;
                    }
                    containerVisiblePercentage.Add(container.Name, new int[] {i, leftBound, rightBound});
                    if (/*(rightBound - leftBound > 50) &&*/ container.Freezed)
                    {
                        container.UnFreeze();
                    }
                }
                else
                {
                    if (!container.Freezed) 
                    {
                        container.Freeze();
                    }
                }
            }
            if (firstTime)
            {
                VisibleContainersChangedEventArgs args = new VisibleContainersChangedEventArgs()
                {
                    ContainerVisiblePercentage = containerVisiblePercentage,
                    CentralContainer = centralContainer,
                };
                VisibleContainersChanged?.Invoke(this, args);
            }
            /*Debug.WriteLine(" ");
            Debug.WriteLine(centralContainer);
            for (int m = 0; m < containerVisiblePercentage.Count; m++)
            {
                Debug.WriteLine(containerVisiblePercentage.ElementAt(m).Value[1]);
                Debug.WriteLine(containerVisiblePercentage2.ElementAt(m).Value[1] + " " + containerVisiblePercentage2.ElementAt(m).Value[2]);
            }*/
        }

        private int AllowMovement(int amount)
        {
            /*if (amount > 0)
            {
                if ((this.Controls[this.Controls.Count - 1].Location.X + this.Controls[this.Controls.Count - 1].Width < this.Width - containerMargins) || (this.Controls[this.Controls.Count - 1].Location.X + this.Controls[this.Controls.Count - 1].Width - amount < this.Width - containerMargins))
                {
                    amount = 0;
                }
            }
            else
            {
                if ((this.Controls[0].Location.X >= containerMargins) || (this.Controls[0].Location.X - amount >= containerMargins))
                {
                    amount = 0;
                }
            }*/
            
            if (virtualVisiblePosition[1] + amount > virtualContainerPositions.Last().Position + this.Controls[this.Controls.Count - 1].Width + containerMargins)
            {
                amount = virtualContainerPositions.Last().Position + this.Controls[this.Controls.Count - 1].Width + containerMargins - virtualVisiblePosition[1];
            }
            if (virtualVisiblePosition[0] + amount < 0)
            {
                amount = -virtualVisiblePosition[0];
            }
            return amount;
        }

        /*[DllImport("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, Int32 wMsg, bool wParam, Int32 lParam);

        private const int WM_SETREDRAW = 11;*/

        private void MoveContent(int amount, bool resize, Container resizedContainer = null)
        {
            //int i = 0;
            //bool foundFirst = false;
            //bool foundLast = false;
            int last = containerVisiblePercentage.Last().Value[0];
            int first = containerVisiblePercentage.First().Value[0];

            /*while ((i < this.Controls.Count) && (foundLast == false))
            {
                if (containerVisiblePercentage.ElementAt(i).Value[1] > 0)
                {
                    if (!foundFirst)
                    {
                        first = i;
                        foundFirst = true;
                    }
                    this.Controls[i].Location = new Point(this.Controls[i].Location.X - amount, this.Controls[i].Location.Y);
                }
                else
                {
                    if (foundFirst)
                    {
                        foundLast = true;
                        last = i - 1;
                    }
                }
                i++;
            }*/
            /*amount = AllowMovement(amount);
            if (amount != 0)
            {
                foreach (int[] value in containerVisiblePercentage.Values)
                {
                    this.Controls[value[0]].Location = new Point(this.Controls[value[0]].Location.X - amount, containerMargins);
                }

                if (amount > 0)
                {
                    //direzione sinistra
                    if ((last + 1 < this.Controls.Count) && (this.Controls[last].Location.X + this.Controls[last].Width < this.Width - containerMargins))
                    {
                        this.Controls[last + 1].Location = new Point(this.Controls[last].Location.X + this.Controls[last].Width + containerMargins, containerMargins);
                    }
                    if ((first >= 0) && (this.Controls[first].Location.X + this.Controls[first].Width <= 0))
                    {
                        this.Controls[first].Location = new Point(-this.Controls[first].Width, containerMargins);
                    }
                }
                else
                {
                    //direzione destra
                    if ((first - 1 >= 0) && (this.Controls[first].Location.X > containerMargins))
                    {
                        this.Controls[first - 1].Location = new Point(this.Controls[first].Location.X - this.Controls[first - 1].Width - containerMargins, containerMargins);
                    }
                    if (this.Controls[last].Location.X >= this.Width)
                    {
                        this.Controls[last].Location = new Point(this.Width, containerMargins);
                    }
                }
                ContainersOnScreen();
            }*/
            /*for (int k = 0; k < this.Controls.Count; k++)
            {
                this.Controls[k].Location = new Point(this.Controls[k].Location.X - amount, this.Controls[k].Location.Y);
            }*/
            if (!resize)
            {
                amount = AllowMovement(amount);
            }
            if (amount != 0)
            {
                if (!resize)
                {
                    virtualVisiblePosition[0] += amount;
                    virtualVisiblePosition[1] += amount;
                    foreach (KeyValuePair<string, int[]> item in containerVisiblePercentage)
                    {
                        //this.Controls[value[0]].Location = new Point(this.Controls[value[0]].Location.X - amount, containerMargins);
                        //this.Controls[item.Value[0]].SuspendLayout();
                        this.Controls[item.Value[0]].Location = new Point(virtualContainerPositions[item.Value[0]].Position - virtualVisiblePosition[0], containerMargins);
                        //this.Controls[item.Value[0]].ResumeLayout();
                    }
                }
                else
                {
                    int index = this.Controls.IndexOf(resizedContainer);
                    for (int i = index + 1; i < this.Controls.Count; i++)
                    {
                        //this.Controls[i].SuspendLayout();
                        virtualContainerPositions[i].Position += amount;
                        this.Controls[i].Location = new Point(virtualContainerPositions[i].Position - virtualVisiblePosition[0], containerMargins);
                        //this.Controls[i].ResumeLayout();
                    }
                }
                Dictionary<string, int[]> oldContainerVisiblePercentage = new Dictionary<string, int[]>(containerVisiblePercentage);
                ContainersOnScreen();
                foreach (KeyValuePair<string, int[]> item in oldContainerVisiblePercentage)
                {
                    int[] tempValue;
                    containerVisiblePercentage.TryGetValue(item.Key, out tempValue);
                    if (tempValue == null)
                    {
                        //container nuovo da nascondere o prima o dopo del tape in base al indice più grande o più piccolo
                        //del container centrale
                        if (item.Value[0] > centralContainerIndex)
                        {
                            //lo metto a destra
                            //this.Controls[item.Value[0]].SuspendLayout();
                            this.Controls[item.Value[0]].Location = new Point(this.Width, containerMargins);
                            //this.Controls[item.Value[0]].ResumeLayout();
                        }
                        else
                        {
                            //lo metto a sinistra
                            //this.Controls[item.Value[0]].SuspendLayout();
                            this.Controls[item.Value[0]].Location = new Point(0 - this.Controls[item.Value[0]].Width, containerMargins);
                            //this.Controls[item.Value[0]].ResumeLayout();
                        }
                    }
                }
                foreach (KeyValuePair<string, int[]> item in containerVisiblePercentage)
                {
                    int[] tempValue;
                    oldContainerVisiblePercentage.TryGetValue(item.Key, out tempValue);
                    if (tempValue == null)
                    {
                        //container nuovo da far vedere o prima o dopo del tape, questo lo vedo automaticamente con il classico calcolo
                        //this.Controls[item.Value[0]].SuspendLayout();
                        this.Controls[item.Value[0]].Location = new Point(virtualContainerPositions[item.Value[0]].Position - virtualVisiblePosition[0], containerMargins);
                        //this.Controls[item.Value[0]].ResumeLayout();
                    }
                }
                VisibleContainersChangedEventArgs args = new VisibleContainersChangedEventArgs()
                {
                    ContainerVisiblePercentage = containerVisiblePercentage,
                    CentralContainer = centralContainer,
                };
                VisibleContainersChanged?.Invoke(this, args);
            }
        }

        private void ContentTape_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                panning = true;
                startPoint = e.Location;
            }
            /*if (e.Button == MouseButtons.Right)
            {
                MoveContent(18);
            }*/
        }

        private void ContentTape_MouseMove(object sender, MouseEventArgs e)
        {
            if (panning)
            {
                Point currentPosition = e.Location;
                int deltaX = startPoint.X - currentPosition.X;
                if (Math.Abs(deltaX) > 0) //Perchè si diceva su internet che possono arrivare eventi anche senza muovere il mouse
                {
                    startPoint = currentPosition;
                    MoveContent(deltaX, false);
                    //this.AutoScrollPosition = new Point(-AutoScrollPosition.X + deltaX, AutoScrollPosition.Y);
                    this.Refresh();
                }
            }
        }

        private void ContentTape_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                panning = false;
            }
        }

        /*private int CalculateDistance(string containerId, int containerX, int containerWidth)
        {
            int currentContainerPosition = 0;
            int containerPosition = 0;
            int first = containerVisiblePercentage.First().Value[0];
            int last = containerVisiblePercentage.Last().Value[0];
            int distance = containerX - (this.Width - containerWidth) / 2;
            if (!containerVisiblePercentage.ContainsKey(containerId))
            {
                if (distance < 0)
                {
                    distance -= Math.Abs(this.Controls[first].Location.X);
                    while ((first - 1 > 0) && (this.Controls[first - 1].Name != containerId))
                    {
                        distance -= this.Controls[first - 1].Width + containerMargins;
                        first--;
                    }
                }
                else
                {
                    distance += Math.Abs(this.Width - this.Controls[last].Location.X - this.Controls[last].Width);
                    while ((last + 1 < this.Controls.Count) && (this.Controls[last + 1].Name != containerId))
                    {
                        distance += this.Controls[last + 1].Width + containerMargins;
                        last++;
                    }
                }
            }
            /*while (containerVisiblePercentage.ElementAt(currentContainerPosition).Value == 0)
            {
                currentContainerPosition++;
            }
            containerPosition = currentContainerPosition;
            distance += this.Controls[containerPosition].Location.X;
            while (containerVisiblePercentage.ElementAt(containerPosition).Key != containerId)
            {
                distance += this.Controls[containerPosition].Width + containerMargins;
                containerPosition++;
            }
            distance = containerX - (this.Width - containerWidth)/2;
            while (containerVisiblePercentage.ElementAt(containerPosition).Key != containerId)
            {
                containerPosition++;
            }
            if (distance < 0)
            {
                
            }
            else
            {
                last = containerPosition - 1;
                while (containerVisiblePercentage.ElementAt(containerPosition - 1).Value == 0)
                {
                    distance += this.Controls[last].Width + containerMargins;
                    last--;
                }
                if (containerVisiblePercentage.ElementAt(last).Value == 100)
                {
                    distance += this.Width - this.Controls[last].Location.X;
                }
                else if (containerVisiblePercentage.ElementAt(last).Value > 0)
                {
                    distance += this.Controls[last].Location.X + this.Controls[last].Width - this.Width;
                }
            }
            return distance;
        }*/

        public void MoveToContainer(string containerId)
        {
            if (rightScrollAnimationTimer.Enabled)
            {
                rightScrollAnimationTimer.Stop();
            }
            if (leftScrollAnimationTimer.Enabled)
            {
                leftScrollAnimationTimer.Stop();
            }
            //TODO sarebbe da verificare se la ricerca in questo modo è ottimizzata o no
            Container selectedContainer = (Container)this.Controls.Find(containerId, false)[0];
            //animationDistance = CalculateDistance(containerId, selectedContainer.Location.X, selectedContainer.Width);
            //animationDistance = virtualVisiblePosition[0] + selectedContainer.Location.X - (this.Width - selectedContainer.Width) / 2;
            animationDistance = virtualContainerPositions.Find(item => item.Name == containerId).Position - virtualVisiblePosition[0] - (this.Width - selectedContainer.Width) / 2;
            //animationDuration = 500 + (Math.Abs(animationDistance/3000))*500;
            animationDeltaX = 0;
            locationProgress = 0;
            animationProgress = 0;
            if (animationDistance > 0)
            {
                rightScrollAnimationTimer.Start();
            }
            else
            {
                leftScrollAnimationTimer.Start();
            }
            /*start = (-contentTape.AutoScrollPosition.X);
            distance = selectedContainer.Location.X + (selectedContainer.Width / 2) - (contentTape.Width / 2);
            if (start + distance < 0)
            {
                distance = -start;
            }
            if(start + distance + contentTape.Width > contentTape.HorizontalScroll.Maximum)
            {
                distance = contentTape.HorizontalScroll.Maximum - contentTape.Width - start;
            }
            if (distance >= 0) //sono sicuro che non sarà zero
            {
                animationProgress = 0;
                rightScrollAnimationTimer.Start();
            }
            else
            {
                animationProgress = animationDuration;
                leftScrollAnimationTimer.Start();
            }*/
        }

        private void RightScrollAnimation_Tick(object sender, EventArgs e)
        {
            easeDistance = (int) Animation.EaseInOut(animationProgress, start, animationDistance, animationDuration);
            if (locationProgress < animationDistance - 10)
            {
                animationDeltaX = easeDistance - locationProgress;
                allowedPercentage = 100;
                if (animationDeltaX > this.Width*90/100)
                {
                    allowedPercentage = this.Width * 90 / (animationDeltaX);
                    animationDeltaX = this.Width*90/100;
                }
                locationProgress += animationDeltaX;
                MoveContent(animationDeltaX, false);
                animationProgress += 20 * allowedPercentage / 100;
            }
            else
            {
                animationDeltaX = animationDistance - locationProgress;
                MoveContent(animationDeltaX, false);
                rightScrollAnimationTimer.Stop();
            }
            /*if ((-contentTape.AutoScrollPosition.X) < start + distance - 10)
            {
                int easeDistance = (int)Animation.EaseInOut(animationProgress, start, distance, animationDuration);
                contentTape.AutoScrollPosition = new Point(easeDistance, 0);
                animationProgress += 20;
            }
            else
            {
                contentTape.AutoScrollPosition = new Point(start + distance, 0);
                rightScrollAnimationTimer.Stop();
            }*/
        }

        private void LeftScrollAnimation_Tick(object sender, EventArgs e)
        {
            easeDistance = (int)Animation.EaseInOut(animationProgress, start, animationDistance, animationDuration);
            if (easeDistance > animationDistance + 10)
            {
                animationDeltaX = easeDistance - locationProgress;
                locationProgress += animationDeltaX;
                allowedPercentage = 100;
                if (animationDeltaX < -this.Width * 90 / 100)
                {
                    allowedPercentage = (-this.Width * 90) / (animationDeltaX);
                    animationDeltaX = -this.Width * 90 / 100;
                }
                MoveContent(animationDeltaX, false);
                animationProgress += 20 * allowedPercentage / 100;
            }
            else
            {
                animationDeltaX = animationDistance - locationProgress;
                MoveContent(animationDeltaX, false);
                leftScrollAnimationTimer.Stop();
            }
            /*if ((-contentTape.AutoScrollPosition.X) > start + distance + 10)
            {
                int easeDistance = (int)Animation.EaseInOut(animationProgress, start + distance, -distance, animationDuration);
                contentTape.AutoScrollPosition = new Point(easeDistance, 0);
                animationProgress -= 20;
            }
            else
            {
                contentTape.AutoScrollPosition = new Point(start + distance, 0);
                leftScrollAnimationTimer.Stop();
            }*/
        }

        private void ContentTape_SizeChanged(object sender, EventArgs e)
        {
            foreach (Control control in this.Controls)
            {
                control.Height = this.Height - (2 * containerMargins);
            }
        }
    }
}
