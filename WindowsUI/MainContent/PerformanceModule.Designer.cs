﻿namespace WindowsUI.MainContent
{
    partial class PerformanceModule
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button = new System.Windows.Forms.Button();
            this.checkBox = new System.Windows.Forms.CheckBox();
            this.comboBox = new System.Windows.Forms.ComboBox();
            this.label = new System.Windows.Forms.Label();
            this.pictureBox = new System.Windows.Forms.PictureBox();
            this.panel = new System.Windows.Forms.Panel();
            this.body.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // body
            // 
            this.body.Controls.Add(this.panel);
            this.body.Controls.Add(this.pictureBox);
            this.body.Controls.Add(this.label);
            this.body.Controls.Add(this.comboBox);
            this.body.Controls.Add(this.checkBox);
            this.body.Controls.Add(this.button);
            this.body.Size = new System.Drawing.Size(500, 459);
            // 
            // button
            // 
            this.button.Location = new System.Drawing.Point(13, 21);
            this.button.Name = "button";
            this.button.Size = new System.Drawing.Size(75, 23);
            this.button.TabIndex = 0;
            this.button.Text = "button1";
            this.button.UseVisualStyleBackColor = true;
            // 
            // checkBox
            // 
            this.checkBox.AutoSize = true;
            this.checkBox.Location = new System.Drawing.Point(13, 50);
            this.checkBox.Name = "checkBox";
            this.checkBox.Size = new System.Drawing.Size(98, 23);
            this.checkBox.TabIndex = 1;
            this.checkBox.Text = "checkBox1";
            this.checkBox.UseVisualStyleBackColor = true;
            // 
            // comboBox
            // 
            this.comboBox.FormattingEnabled = true;
            this.comboBox.Items.AddRange(new object[] {
            "One",
            "Two",
            "Three",
            "Four",
            "Five",
            "Six",
            "Seven",
            "Eight",
            "Nine",
            "Ten"});
            this.comboBox.Location = new System.Drawing.Point(13, 79);
            this.comboBox.Name = "comboBox";
            this.comboBox.Size = new System.Drawing.Size(121, 27);
            this.comboBox.TabIndex = 2;
            // 
            // label
            // 
            this.label.AutoSize = true;
            this.label.Location = new System.Drawing.Point(9, 109);
            this.label.Name = "label";
            this.label.Size = new System.Drawing.Size(50, 19);
            this.label.TabIndex = 3;
            this.label.Text = "label1";
            // 
            // pictureBox
            // 
            this.pictureBox.Image = global::WindowsUI.Properties.Resources.logoAulos;
            this.pictureBox.Location = new System.Drawing.Point(13, 142);
            this.pictureBox.Name = "pictureBox";
            this.pictureBox.Size = new System.Drawing.Size(100, 50);
            this.pictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox.TabIndex = 4;
            this.pictureBox.TabStop = false;
            // 
            // panel
            // 
            this.panel.BackColor = System.Drawing.Color.DarkGray;
            this.panel.Location = new System.Drawing.Point(13, 213);
            this.panel.Name = "panel";
            this.panel.Size = new System.Drawing.Size(100, 81);
            this.panel.TabIndex = 5;
            // 
            // PerformanceModule
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ModuleTitle = "Performance";
            this.Name = "PerformanceModule";
            this.Size = new System.Drawing.Size(500, 500);
            this.Load += new System.EventHandler(this.PerformanceModule_Load);
            this.body.ResumeLayout(false);
            this.body.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel;
        private System.Windows.Forms.PictureBox pictureBox;
        private System.Windows.Forms.Label label;
        private System.Windows.Forms.ComboBox comboBox;
        private System.Windows.Forms.CheckBox checkBox;
        private System.Windows.Forms.Button button;
    }
}
