﻿namespace WindowsUI.MainContent
{
    partial class ContainerPopup
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.modalTitle = new System.Windows.Forms.Label();
            this.header = new System.Windows.Forms.Panel();
            this.body = new System.Windows.Forms.Panel();
            this.header.SuspendLayout();
            this.body.SuspendLayout();
            this.SuspendLayout();
            // 
            // modalTitle
            // 
            this.modalTitle.AutoSize = true;
            this.modalTitle.BackColor = System.Drawing.Color.Transparent;
            this.modalTitle.Font = new System.Drawing.Font("Open Sans", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.modalTitle.Location = new System.Drawing.Point(20, 10);
            this.modalTitle.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.modalTitle.Name = "modalTitle";
            this.modalTitle.Size = new System.Drawing.Size(49, 24);
            this.modalTitle.TabIndex = 0;
            this.modalTitle.Text = "Title";
            // 
            // header
            // 
            this.header.BackColor = System.Drawing.Color.Transparent;
            this.header.Controls.Add(this.modalTitle);
            this.header.Dock = System.Windows.Forms.DockStyle.Top;
            this.header.Location = new System.Drawing.Point(0, 0);
            this.header.Name = "header";
            this.header.Size = new System.Drawing.Size(560, 40);
            this.header.TabIndex = 1;
            this.header.Paint += new System.Windows.Forms.PaintEventHandler(this.header_Paint);
            // 
            // body
            // 
            this.body.BackColor = System.Drawing.Color.White;
            this.body.Controls.Add(this.header);
            this.body.Location = new System.Drawing.Point(0, 0);
            this.body.Name = "body";
            this.body.Size = new System.Drawing.Size(560, 500);
            this.body.TabIndex = 2;
            this.body.Resize += new System.EventHandler(this.Body_Resize);
            // 
            // ContainerPopup
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this.body);
            this.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximumSize = new System.Drawing.Size(560, 500);
            this.Name = "ContainerPopup";
            this.Size = new System.Drawing.Size(560, 500);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.ContainerPopup_Paint);
            this.header.ResumeLayout(false);
            this.header.PerformLayout();
            this.body.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label modalTitle;
        private System.Windows.Forms.Panel header;
        protected System.Windows.Forms.Panel body;
    }
}
