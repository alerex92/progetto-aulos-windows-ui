﻿namespace WindowsUI.MainContent
{
    partial class ContentTape
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // ContentTape
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(0);
            this.Name = "ContentTape";
            this.Size = new System.Drawing.Size(1024, 600);
            this.SizeChanged += new System.EventHandler(this.ContentTape_SizeChanged);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ContentTape_MouseDown);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.ContentTape_MouseMove);
            this.MouseUp += new System.Windows.Forms.MouseEventHandler(this.ContentTape_MouseUp);
            this.ResumeLayout(false);

        }

        #endregion
    }
}
