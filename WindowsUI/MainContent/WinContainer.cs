﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using WindowsUI.UI;

namespace WindowsUI.MainContent
{
    public partial class WinContainer : Container
    {

        [Description("Display the Operator message"), Category("Aulos")]
        public bool OperatorMessageActive { get { return operatorMessagePanel.Visible; } set { operatorMessagePanel.Visible = value; } }

        [Description("Change the Operator message"), Category("Aulos")]
        public string OperatorMessage { get { return operatorMessageLabel.Text; } set { operatorMessageLabel.Text = value; } }

        protected Point operatorMessageLocation { get { return operatorMessageLabel.Location; } set { operatorMessageLabel.Location = value; } }

        [Description("Change the name of the container"), Category("Aulos")]
        public override string ContainerName { get { return containerName.Text; } set { containerName.Text = value; } }

        Timer popupMoveTimer;
        Timer toasterDisappearTimer;
        Timer toasterMoveTimer;

        int toasterMoveAnimationProgress;
        int popupMoveAnimationProgress;
        int moveAnimationDuration = 200;

        ContainerToaster toaster;
        ContainerPopup popup;

        public MouseEventHandler BubbleMouseDown;
        public MouseEventHandler BubbleMouseMove;
        public MouseEventHandler BubbleMouseUp;

        public WinContainer()
        {
            InitializeComponent();
            this.SetStyle(ControlStyles.OptimizedDoubleBuffer, true);
            this.SetStyle(ControlStyles.AllPaintingInWmPaint, true);

            popupMoveTimer = new Timer();
            popupMoveTimer.Tick += new EventHandler(PopupMoveAnimation_Tick);
            popupMoveTimer.Interval = 20;

            toasterMoveTimer = new Timer();
            toasterMoveTimer.Tick += new EventHandler(ToasterMoveAnimation_Tick);
            toasterMoveTimer.Interval = 20;

            toasterDisappearTimer = new Timer();
            toasterDisappearTimer.Tick += new EventHandler(ToasterDisappearAnimation_Tick);
            toasterDisappearTimer.Interval = 2000 + moveAnimationDuration;    
        }

        private void WinContainer_Load(object sender, EventArgs e)
        {
            //Magari funzionerebbe meglio un sistema di registrazione al container
            foreach (ContainerModule childModule in this.body.Controls)
            {
                childModule.BubbleMouseDown += BubbleChildMouseDown;
                childModule.BubbleMouseMove += BubbleChildMouseMove;
                childModule.BubbleMouseUp += BubbleChildMouseUp;
            }
        }

        /*public override void Freeze()
        {
            base.Freeze();
            foreach (ContainerModule item in body.Controls)
            {
                item.Visible = false;
            }
        }

        public override void UnFreeze()
        {
            base.UnFreeze();
            foreach (ContainerModule item in body.Controls)
            {
                item.Visible = true;
            }
        }*/

        private void BubbleChildMouseDown(object sender, MouseEventArgs e)
        {
            BubbleMouseDown?.Invoke(sender, e);
        }

        private void BubbleChildMouseMove(object sender, MouseEventArgs e)
        {
            BubbleMouseMove?.Invoke(sender, new MouseEventArgs(e.Button, e.Clicks, e.X, e.Y, e.Delta));
        }

        private void BubbleChildMouseUp(object sender, MouseEventArgs e)
        {
            BubbleMouseUp?.Invoke(sender, e);
        }        

        private void OperatorMessageLabel_TextChanged(object sender, EventArgs e)
        {
            int width = TextRenderer.MeasureText(operatorMessageLabel.Text, operatorMessageLabel.Font).Width;
            operatorMessageLabel.Width = width;
            operatorMessageLabel.Location = new Point((this.Width - width) / 2, operatorMessageLabel.Location.Y);
        }

        protected void ShowToaster (string message, Color? backgroundColor = null)
        {
            toaster = new ContainerToaster();
            Graphics g = this.CreateGraphics();
            toaster.Height = (int) g.MeasureString(message, toaster.Font, toaster.Width - 2 * toaster.Padding.Left).Height + 2 * toaster.Padding.Top;
            g.Dispose();
            toaster.Message = message;
            if (backgroundColor != null)
            {
                toaster.BackColor = (Color)backgroundColor;
            }
            toaster.Location = new Point((this.Width + this.Padding.Left - toaster.Width) / 2, 0 - toaster.Height);
            this.Controls.Add(toaster);
            toaster.BringToFront();
            toasterMoveTimer.Start();
            toasterDisappearTimer.Start();
        }

        public void ShowModal(ContainerPopup containerPopup)
        {
            popup = containerPopup;
            popup.Controls.Find("body", false)[0].Width = this.Width - this.Padding.Left - 40;
            popup.Location = new Point((this.Width - this.Padding.Left - popup.Width) / 2, 0 - popup.Height);
            popup.ParentContainer = this;
            popup.Add();
            popupMoveAnimationProgress = 0;
            popupMoveTimer.Start();
        }

        private void ToasterMoveAnimation_Tick(object sender, EventArgs e)
        {
            //TODO è possibile centralizzare il calcolo del movimento?
            if (toaster.Location.Y < 60 - 5)
            {
                toaster.Location = new Point(toaster.Location.X, (int)Animation.EaseInOut(toasterMoveAnimationProgress, 0 - toaster.Height, 60 + toaster.Height, moveAnimationDuration));
                toasterMoveAnimationProgress += moveAnimationDuration / 20;
            }
            else
            {
                toaster.Location = new Point(toaster.Location.X, 60);
                toasterMoveAnimationProgress = moveAnimationDuration;
                toasterMoveTimer.Stop();
            }
        }

        private void ToasterDisappearAnimation_Tick(object sender, EventArgs e)
        {
            toaster.Dispose();
            toasterDisappearTimer.Stop();
            toasterMoveAnimationProgress = 0;
        }

        private void PopupMoveAnimation_Tick(object sender, EventArgs e)
        {
            if (popup.Location.Y < 100 - 5)
            {
                popup.Location = new Point(popup.Location.X, (int)Animation.EaseInOut(popupMoveAnimationProgress, 0 - popup.Height, 100 + popup.Height, moveAnimationDuration));
                popupMoveAnimationProgress += moveAnimationDuration / 20;
            }
            else
            {
                popup.Location = new Point(popup.Location.X, 100);
                popupMoveAnimationProgress = moveAnimationDuration;
                popupMoveTimer.Stop();
            }
        }
    }
}
