﻿namespace WindowsUI.Header
{
    partial class NotificationListItem
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.notificationTimestamp = new System.Windows.Forms.Label();
            this.notificationName = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // notificationTimestamp
            // 
            this.notificationTimestamp.Dock = System.Windows.Forms.DockStyle.Left;
            this.notificationTimestamp.Location = new System.Drawing.Point(0, 0);
            this.notificationTimestamp.Margin = new System.Windows.Forms.Padding(0);
            this.notificationTimestamp.Name = "notificationTimestamp";
            this.notificationTimestamp.Padding = new System.Windows.Forms.Padding(20, 0, 0, 0);
            this.notificationTimestamp.Size = new System.Drawing.Size(150, 40);
            this.notificationTimestamp.TabIndex = 0;
            this.notificationTimestamp.Text = "Timestamp";
            this.notificationTimestamp.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.notificationTimestamp.Click += new System.EventHandler(this.notificationTimestamp_Click);
            // 
            // notificationName
            // 
            this.notificationName.AutoEllipsis = true;
            this.notificationName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.notificationName.Location = new System.Drawing.Point(150, 0);
            this.notificationName.Name = "notificationName";
            this.notificationName.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.notificationName.Size = new System.Drawing.Size(191, 40);
            this.notificationName.TabIndex = 1;
            this.notificationName.Text = "Name";
            this.notificationName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.notificationName.Click += new System.EventHandler(this.notificationName_Click);
            // 
            // NotificationListItem
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this.notificationName);
            this.Controls.Add(this.notificationTimestamp);
            this.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(0);
            this.Name = "NotificationListItem";
            this.Size = new System.Drawing.Size(341, 40);
            this.Load += new System.EventHandler(this.NotificationListItem_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label notificationTimestamp;
        private System.Windows.Forms.Label notificationName;
    }
}
