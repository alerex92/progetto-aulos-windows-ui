﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using Loccioni.Aulos.Messages.TransferModel;
using WindowsUI.UI;
using System.Diagnostics;
using Loccioni.Aulos;
using Loccioni.Aulos.Messages.WebClient;

namespace WindowsUI.Header
{
    public partial class NotificationList : UserControl
    {
        Color neutral = Color.White; //Lo uso come colore base quando creo gli elementi anche se poi non verrà mai mostrato
        Color warningListColor = ColorTranslator.FromHtml("#FFF9A9");
        Color warningDetailsColor = ColorTranslator.FromHtml("#FFF16C");
        Color alarmListColor = ColorTranslator.FromHtml("#FFA9A9");
        Color alarmDetailsColor = ColorTranslator.FromHtml("#FF7D7D");

        int margins = 20;
        string mode;
        string activeLabelName;

        AlarmsClient alarmsClient;
        WarningsClient warningsClient;
        List<NotificationDetails> alarmDetailsList;
        List<NotificationDetails> warningDetailsList;

        public int MaxHeight { private get; set; }
        public bool Small { private get; set; }
        public bool Closed { get; private set; }
        public GlobalConfiguration Configuration { private get; set; }

        public List<Notification> AlarmList { private get; set; }
        public List<Notification> WarningList { private get; set; }

        public NotificationList()
        {
            InitializeComponent();
            Closed = true;
        }

        public void Setup()
        {
            this.Size = new Size(this.Width, MaxHeight);
            notificationListPanel.Width = this.Width / 3;
            //notificationListTitle.Location = new Point(margins, margins);
            notificationListSubPanel.Height = notificationListPanel.Height - margins - notificationListTitle.Height;

            notificationDetail.Width = this.Width / 3;
            notificationDetail.Margins = margins;
            notificationDetail.MaxHeight = MaxHeight;
            //notificationDetail.Configuration = Configuration;
            notificationDetail.AlarmsClient = alarmsClient;
            notificationDetail.WarningsClient = warningsClient;
            notificationDetail.Setup();
            notificationDetail.GoToClickParent += GoToContainer;
            
            notificationTroubleshooting.Width = this.Width - notificationDetail.Width - notificationListPanel.Width;
            notificationTroubleshooting.Margins = margins;
            notificationTroubleshooting.Small = Small;
            notificationTroubleshooting.Setup();

            WebClientService webClientService = ServiceBroker.GetService<WebClientService>();

            warningsClient = (WarningsClient)webClientService[typeof(WarningsClient)];
            warningsClient.WarningFired += Warnings_Fired;

            alarmsClient = (AlarmsClient)webClientService[typeof(AlarmsClient)];
            alarmsClient.AlarmsChanged += Alarms_Changed;

            warningDetailsList = new List<NotificationDetails>();
        }

        private async void GetAlarmDetails(Notification alarm)
        {
            NotificationDetails tempDetail;
            
            tempDetail = await alarmsClient.GetAlarmDetails(alarm.Code);
            tempDetail.Code = alarm.Code; // Da eliminare: questi sono campi in più che in teoria ci sono in un json fatto bene
            tempDetail.ShortText = alarm.ShortText; //same
            tempDetail.Timestamp = alarm.Timestamp; //same
            alarmDetailsList?.Add(tempDetail);
        }

        public async void UpdateAlarmList()
        {
            alarmDetailsList = new List<NotificationDetails>();
            //Da eliminare quando ho a disposizione un collegamento al server

            /*Loccioni.Aulos.Messages.TransferModel.NotificationDetails listItem;
            for (int i = 0; i < 10; i++)
            {
                listItem = new Loccioni.Aulos.Messages.TransferModel.NotificationDetails();
                listItem.Code = "A00" + i;
                listItem.ImageUrl = @"C:\Users\aless\Desktop\imgProva\" + i + ".jpg";
                listItem.IsActive = true;
                listItem.Timestamp = new DateTime(2016, 1, i + 1, 10, 59, 22);
                listItem.ShortText = "Errore " + i;
                listItem.LongText = "Descrizione lunga dell'errore numero " + i;
                listItem.TroubleShooting = "Questo errore numero " + i + " si risolve accendendo e spegnendo il banco";
                alarmDetailsList.Add(listItem);
            }*/
            NotificationDetails tempDetail;
            foreach (Notification alarm in AlarmList)
            {
                tempDetail = await alarmsClient.GetAlarmDetails(alarm.Code);
                tempDetail.Code = alarm.Code; // Da eliminare: questi sono campi in più che in teoria ci sono in un json fatto bene
                tempDetail.ShortText = alarm.ShortText; //same
                tempDetail.Timestamp = alarm.Timestamp; //same
                alarmDetailsList?.Add(tempDetail);
            }
            //BuildAlarmsList();
        }

        private void BuildAlarmsList()
        {
            NotificationDetails tempDetail;
            /*int scrollWidth = 0;
            if (alarmDetailsList.Count >= maxHeight / 40)
            //if (alarmList.Count > maxHeight / 40) //se ci sono N allarmi significa che ci sarà la scrollbar
            {
                //scrollWidth = 17;
                scrollWidth = SystemInformation.VerticalScrollBarWidth;
            }*/
            int i = 0;
            foreach(Loccioni.Aulos.Messages.TransferModel.NotificationDetails alarm in alarmDetailsList)
            //for (int i = 0; i < 10; i++)
            {
                tempDetail = alarm;
                //tempDetail = alarmDetailsList[i];

                //int timeWidth = TextRenderer.MeasureText(tempDetail.Timestamp.ToString("HH:mm:ss dd/MM/yyyy"), new Font("Open Sans", 14, GraphicsUnit.Pixel)).Width;
                /*Label tempLabelTime = new Label()
                {
                    //TODO il formato data può essere messo su costante o preso dalla cultura
                    Text = tempDetail.Timestamp.ToString("HH:mm:ss dd/MM/yyyy"),
                    Location = new Point(0, 50 + 40 * i),
                    Padding = new Padding(margins, 0, 0, 0),
                    TextAlign = ContentAlignment.MiddleLeft,
                    Font = new Font("Open Sans", 14, GraphicsUnit.Pixel),
                    Name = tempDetail.Code,
                    Width = timeWidth + margins + 5,
                    Height = 40,
                };
              
                Label tempLabelTitle = new Label()
                {
                    Text = tempDetail.ShortText,
                    AutoEllipsis = true,
                    Location = new Point(tempLabelTime.Width + tempLabelTime.Location.X, 50 + 40 * i),
                    Padding = new Padding(margins, 0, 0, 0),
                    TextAlign = ContentAlignment.MiddleLeft,
                    Font = new Font("Open Sans", 14, GraphicsUnit.Pixel),
                    Name = tempDetail.Code,
                    Width = notificationListPanel.Width - tempLabelTime.Location.X - tempLabelTime.Width - scrollWidth,
                    Height = 40,
                };
                tempLabelTime.Click += new EventHandler(ListItem_Click);
                tempLabelTitle.Click += new EventHandler(ListItem_Click);
                notificationListPanel.Controls.Add(tempLabelTime);
                notificationListPanel.Controls.Add(tempLabelTitle);*/
                NotificationListItem tempListItem = new NotificationListItem()
                {
                    TimeStampText = tempDetail.Timestamp.ToString("HH:mm:ss dd/MM/yyyy"),
                    NotificationName = tempDetail.ShortText,
                    Dock = DockStyle.Top,
                    Name = tempDetail.Code,
                    Small = Small
                };
                tempListItem.Click += new EventHandler(ListItem_Click);
                notificationListSubPanel.Controls.Add(tempListItem);
                tempListItem.BringToFront();
                i++;
            }
        }

        //Da eliminare quando il package è fixato
        /*public async Task<Loccioni.Aulos.Messages.TransferModel.NotificationDetails> GetAlarmDetails(string code)
        {
            HttpClient client = new HttpClient();
            HttpResponseMessage response = await client.GetAsync(@"http://" + urlParam["baseUrl"] + ":" + urlParam["urlPort"] + "/jsonServer/messages/alarm/" + code);


            if (response.IsSuccessStatusCode)
            {
                return await response.Content.ReadAsAsync<Loccioni.Aulos.Messages.TransferModel.NotificationDetails>();
            }
            return null;
        }*/

        private async void GetWarningDetails(Notification warning)
        {
            NotificationDetails tempDetail;

            tempDetail = await warningsClient.GetWarningDetails(warning.Code);
            tempDetail.Code = warning.Code; // Da eliminare: questi sono campi in più che in teoria ci sono in un json fatto bene
            tempDetail.ShortText = warning.ShortText; //same
            tempDetail.Timestamp = warning.Timestamp; //same
            warningDetailsList.Add(tempDetail);
        }

        public async void UpdateWarningList()
        {
            warningDetailsList = new List<NotificationDetails>();
            NotificationDetails tempDetail;
            foreach (Notification warning in WarningList)
            {
                tempDetail = await warningsClient.GetWarningDetails(warning.Code);
                tempDetail.Code = warning.Code; // Da eliminare: questi sono campi in più che in teoria ci sono in un json fatto bene
                tempDetail.ShortText = warning.ShortText; //same
                tempDetail.Timestamp = warning.Timestamp; //same
                warningDetailsList.Add(tempDetail);
            }
            //BuildWarningsList();
        }

        private void BuildWarningsList()
        {
            Loccioni.Aulos.Messages.TransferModel.NotificationDetails tempDetail;
            /*int scrollWidth = 0;
            if (warningDetailsList.Count >= maxHeight / 40)
            //if (alarmList.Count > maxHeight / 40) //se ci sono N allarmi significa che ci sarà la scrollbar
            {
                scrollWidth = 17;
            }*/
            int i = 0;
            foreach (Loccioni.Aulos.Messages.TransferModel.NotificationDetails warning in warningDetailsList)
            //for (int i = 0; i < 10; i++)
            {
                tempDetail = warning;
                //tempDetail = alarmDetailsList[i];

                /*int timeWidth = TextRenderer.MeasureText(tempDetail.Timestamp.ToString("HH:mm:ss dd/MM/yyyy"), new Font("Open Sans", 14, GraphicsUnit.Pixel)).Width;
                Label tempLabelTime = new Label()
                {
                    Text = tempDetail.Timestamp.ToString("HH:mm:ss dd/MM/yyyy"),
                    Location = new Point(0, 50 + 40 * i),
                    Padding = new Padding(margins, 0, 0, 0),
                    TextAlign = ContentAlignment.MiddleLeft,
                    Font = new Font("Open Sans", 14, GraphicsUnit.Pixel),
                    Name = tempDetail.Code,
                    Width = timeWidth + margins + 5,
                    Height = 40,
                };
                Label tempLabelTitle = new Label()
                {
                    Text = tempDetail.ShortText,
                    AutoEllipsis = true,
                    Location = new Point(tempLabelTime.Width + tempLabelTime.Location.X, 50 + 40 * i),
                    Padding = new Padding(margins, 0, 0, 0),
                    TextAlign = ContentAlignment.MiddleLeft,
                    Font = new Font("Open Sans", 14, GraphicsUnit.Pixel),
                    Name = tempDetail.Code,
                    Width = notificationListPanel.Width - tempLabelTime.Location.X - tempLabelTime.Width - scrollWidth,
                    Height = 40,
                };
                tempLabelTime.Click += new EventHandler(ListItem_Click);
                tempLabelTitle.Click += new EventHandler(ListItem_Click);
                notificationListPanel.Controls.Add(tempLabelTime);
                notificationListPanel.Controls.Add(tempLabelTitle);*/

                NotificationListItem tempListItem = new NotificationListItem()
                {
                    TimeStampText = tempDetail.Timestamp.ToString("HH:mm:ss dd/MM/yyyy"),
                    NotificationName = tempDetail.ShortText,
                    Dock = DockStyle.Top,
                    Name = tempDetail.Code,
                    Small = Small
                };
                tempListItem.Click += new EventHandler(ListItem_Click);
                notificationListSubPanel.Controls.Add(tempListItem);
                tempListItem.BringToFront();
                i++;
            }
        }

        public void SelectNotification(Notification activeNotification)
        {
            NotificationListItem activeLabel = (NotificationListItem) notificationListSubPanel.Controls.Find(activeNotification.Code, false).FirstOrDefault();
            if (activeLabel.Name != activeLabelName)
            {
                Control activeListItem;
                Color modeColor = neutral;
                if (mode == "alarm")
                {
                    modeColor = alarmDetailsColor;
                }
                else if (mode == "warning")
                {
                    modeColor = warningDetailsColor;
                }

                if (activeLabelName != null) //Se è già attivo qualcosa lo "disattivo"
                {
                    activeListItem = notificationListSubPanel.Controls.Find(activeLabelName, false)[0];
                    activeListItem.BackColor = Color.Transparent;
                    activeListItem.Font = new Font(activeListItem.Font, FontStyle.Regular);
                }

                activeLabelName = activeLabel.Name;
                notificationDetail.ActiveLabelName = activeLabel.Name;
                activeListItem = notificationListSubPanel.Controls.Find(activeLabelName, false)[0];
                activeListItem.BackColor = modeColor;
                activeListItem.Font = new Font(activeListItem.Font, FontStyle.Bold);
                Loccioni.Aulos.Messages.TransferModel.NotificationDetails selectedDetails = null;
                if (mode == "alarm")
                {
                    selectedDetails = alarmDetailsList.Find(item => item.Code == activeLabelName);
                }
                else if (mode == "warning")
                {
                    selectedDetails = warningDetailsList.Find(item => item.Code == activeLabelName);
                }
                notificationDetail.ShowDetail(selectedDetails);
                notificationTroubleshooting.ShowTroubleshooting(selectedDetails);
            }   
        }

        private void DestroyList()
        {
            //Label titleLabel = (Label)notificationListPanel.Controls.Find("notificationListTitle", true)[0];
            /*for (int i = 0; i < notificationListPanel.Controls.Count; i++)
            {
                if (i != titleIndex)
                {
                    notificationListPanel.Controls[i].Dispose();
                }
            }*/

            /* In teoria questo metodo non dovrebbe funzionare perchè facendo dispose si incasinano gli indici
             cioè se elimino il 5 non è che posso andare al 6 per trovare il successivo ma il successivo si troverà
             sempre al posto 5, quindi va usata una struttura tipo While count > 0 elimina, però qui c'è il problema
             che non devo eliminare il titolo della lista quindi faccio una cosa così: */
            while (notificationListSubPanel.Controls.Count > 1)
            {
                notificationListSubPanel.Controls[0].Dispose();
                /*if(((Label) notificationListPanel.Controls[0]) != titleLabel)
                {
                    notificationListPanel.Controls[0].Dispose();
                }
                else
                {
                    notificationListPanel.Controls[1].Dispose();
                }*/
            }
            /* Così facendo elimino sempre quelli prima finchè non trovo l'elemento che voglio e poi elimino sempre
             quelli dopo */
        }

        private void ListItem_Click(object sender, EventArgs e)
        {
            NotificationListItem activeLabel = (NotificationListItem)sender;
            Control activeListItem;
            Color modeColor = neutral;
            if (mode == "alarm")
            {
                modeColor = alarmDetailsColor;
            }
            else if(mode == "warning")
            {
                modeColor = warningDetailsColor;
            }
            
            if (activeLabelName != null) //Se è già attivo qualcosa lo "disattivo"
            {
                /*activeLabels = notificationListPanel.Controls.Find(activeLabelName, true);
                foreach (Label item in activeLabels)
                {
                    item.BackColor = Color.Transparent;
                    item.Font = new Font(item.Font, FontStyle.Regular);
                }*/

                activeListItem = notificationListSubPanel.Controls.Find(activeLabelName, false)[0];
                activeListItem.BackColor = Color.Transparent;
                activeListItem.Font = new Font(activeListItem.Font, FontStyle.Regular);
            }

            if (activeLabel.Name != activeLabelName) //Se ho cliccato su uno nuovo attivo i dettagli e il background
            {
                activeLabelName = activeLabel.Name;
                notificationDetail.ActiveLabelName = activeLabel.Name;
                /*activeLabels = notificationListPanel.Controls.Find(activeLabelName, true);
                foreach (Label item in activeLabels)
                {
                    item.BackColor = modeColor;
                    item.Font = new Font(item.Font, FontStyle.Bold);
                }*/
                activeListItem = notificationListSubPanel.Controls.Find(activeLabelName, false)[0];
                activeListItem.BackColor = modeColor;
                activeListItem.Font = new Font(activeListItem.Font, FontStyle.Bold);

                //activeLabel.Font = new Font(activeLabel.Font, FontStyle.Bold);
                Loccioni.Aulos.Messages.TransferModel.NotificationDetails selectedDetails = null;
                if (mode == "alarm")
                {
                    selectedDetails = alarmDetailsList.Find(item => item.Code == activeLabelName);
                }
                else if (mode == "warning")
                {
                    selectedDetails = warningDetailsList.Find(item => item.Code == activeLabelName);
                }
                notificationDetail.ShowDetail(selectedDetails);
                notificationTroubleshooting.ShowTroubleshooting(selectedDetails);
            }
            else //Se ho cliccato sullo stesso allora disattivo anche i dettagli
            {
                activeLabelName = null;
                notificationDetail.ActiveLabelName = null;
                notificationDetail.HideDetail();
                notificationTroubleshooting.HideTroubleshooting();
            }
        }

        public void ToggleList(string lystType)
        {
            //StackTrace stackTrace = new StackTrace();
            //System.Diagnostics.Debug.WriteLine(stackTrace.GetFrame(1).GetMethod().Name);
            notificationListTitle.Visible = true;
            mode = lystType;
            notificationDetail.Mode = mode;
            if (Closed == true)
            {
                if (mode == "alarm")
                {
                    //notificationListPanel.BackColor = alarmListColor;
                    //notificationDetail.BackColor = alarmDetailsColor;
                    //notificationTroubleshooting.BackColor = alarmDetailsColor;
                    notificationListTitle.Text = "ALARMS";
                    /*if (alarmDetailsList == null)
                    {
                        UpdateAlarmList();
                    }
                    else
                    {
                        BuildAlarmsList();
                    }*/
                    BuildAlarmsList();
                }
                else if (mode == "warning")
                {
                    //notificationListPanel.BackColor = warningListColor;
                    //notificationDetail.BackColor = warningDetailsColor;
                    //notificationTroubleshooting.BackColor = warningDetailsColor;
                    notificationListTitle.Text = "WARNINGS";
                    /*if (warningDetailsList == null)
                    {
                        UpdateWarningList();
                    }
                    else
                    {
                        BuildWarningsList();
                    }*/
                    BuildWarningsList();
                }
                Closed = false;
            }
            else
            {
                DestroyList();
                notificationListTitle.Visible = false;
                notificationDetail.HideDetail();
                notificationTroubleshooting.HideTroubleshooting();
                Closed = true;
                activeLabelName = null;
                notificationDetail.ActiveLabelName = null;
            }
        }

        private void GoToContainer(object sender, EventArgs e)
        {
            //Ci sarà una chiamata al sistema di navigazione che dovrà essere scorporato sia da questo che dal main content
            //e quindi essere un third party accoppiato lascamente (non so ancora bene come fare)
            string containerId;
            if (mode == "alarm")
            {
                containerId = alarmDetailsList.Find(item => item.Code == activeLabelName).ContainerId;
            }
            else
            {
                containerId = warningDetailsList.Find(item => item.Code == activeLabelName).ContainerId;
            }
            NavigationService.MoveContent(containerId);
        }

        private void Alarms_Changed(Loccioni.Aulos.Messages.WebClient.AlarmsClient sender, Loccioni.Aulos.Messages.TransferModel.Notification notification)
        {
            if ((Closed == false) && (mode == "alarm"))
            {
                if (InvokeRequired)
                {
                    Invoke(new MethodInvoker(delegate
                    {
                        notificationDetail.HideDetail();
                        notificationTroubleshooting.HideTroubleshooting();
                        activeLabelName = null;
                        notificationDetail.ActiveLabelName = null;
                    }));
                }
                int index = AlarmList.FindIndex(item => item.Code == notification.Code);
                if (notification.IsActive)
                {
                    if (index == -1)
                    {
                        NotificationListItem tempListItem = new NotificationListItem()
                        {
                            TimeStampText = notification.Timestamp.ToString("HH:mm:ss dd/MM/yyyy"),
                            NotificationName = notification.ShortText,
                            Dock = DockStyle.Top,
                            Name = notification.Code,
                        };
                        if (InvokeRequired)
                        {
                            Invoke(new MethodInvoker(delegate
                            {
                                tempListItem.Click += new EventHandler(ListItem_Click);
                                notificationListSubPanel.Controls.Add(tempListItem);
                                tempListItem.BringToFront();
                                AlarmList.Add(notification);
                                GetAlarmDetails(notification);
                            }));
                        }
                    }
                }
                else
                {
                    int indexDetail = alarmDetailsList.FindIndex(item => item.Code == notification.Code);
                    Control tempControl =
                        notificationListSubPanel.Controls.Find(notification.Code, false).FirstOrDefault();
                    if (InvokeRequired)
                    {
                        Invoke(new MethodInvoker(delegate
                        {
                            AlarmList.RemoveAt(index);
                            alarmDetailsList.RemoveAt(indexDetail);
                            tempControl?.Dispose();
                        }));
                    }
                }
            }
            else
            {
                int index = AlarmList.FindIndex(item => item.Code == notification.Code);
                if (notification.IsActive)
                {
                    if (index == -1)
                    {
                        if (InvokeRequired)
                        {
                            Invoke(new MethodInvoker(delegate
                            {
                                AlarmList.Add(notification);
                                GetAlarmDetails(notification);
                            }));
                        }
                    }
                }
                else
                {
                    int indexDetail = alarmDetailsList.FindIndex(item => item.Code == notification.Code);
                    if (InvokeRequired)
                    {
                        Invoke(new MethodInvoker(delegate
                        {
                            AlarmList.RemoveAt(index);
                            alarmDetailsList.RemoveAt(indexDetail);
                        }));
                    }
                }
            }

            /*if ((closed == false) && (mode == "alarm"))
            {
                DestroyList();

                notificationTitle.Visible = false;
                notificationDescription.Visible = false;
                notificationDateTime.Visible = false;
                goTo.Visible = false;
                acknowledge.Visible = false;
                acknowledgeAll.Visible = true;
                troubleshootingTitle.Visible = false;
                troubleshootingDesc.Visible = false;
                troubleshootingImage.Visible = false;

                UpdateAlarmList();
            }
            else
            {
                alarmDetailsList = null; 
            }*/
        }

        private void Warnings_Fired(Loccioni.Aulos.Messages.WebClient.WarningsClient sender, Loccioni.Aulos.Messages.TransferModel.Notification notification)
        {
            if ((Closed == false) && (mode == "warning"))
            {
                if (InvokeRequired)
                {
                    Invoke(new MethodInvoker(delegate
                    {
                        notificationDetail.HideDetail();
                        notificationTroubleshooting.HideTroubleshooting();
                        activeLabelName = null;
                        notificationDetail.ActiveLabelName = null;
                    }));
                }
                int index = WarningList.FindIndex(item => item.Code == notification.Code);
                if (notification.IsActive)
                {
                    if (index == -1)
                    {
                        NotificationListItem tempListItem = new NotificationListItem()
                        {
                            TimeStampText = notification.Timestamp.ToString("HH:mm:ss dd/MM/yyyy"),
                            NotificationName = notification.ShortText,
                            Dock = DockStyle.Top,
                            Name = notification.Code,
                        };
                        if (InvokeRequired)
                        {
                            Invoke(new MethodInvoker(delegate
                            {
                                tempListItem.Click += new EventHandler(ListItem_Click);
                                notificationListSubPanel.Controls.Add(tempListItem);
                                tempListItem.BringToFront();
                                WarningList.Add(notification);
                                GetWarningDetails(notification);
                            }));
                        }
                    }
                }
                else
                {
                    int indexDetail = warningDetailsList.FindIndex(item => item.Code == notification.Code);
                    Control tempControl =notificationListSubPanel.Controls.Find(notification.Code, false).FirstOrDefault();
                    if (InvokeRequired)
                    {
                        Invoke(new MethodInvoker(delegate
                        {
                            WarningList.RemoveAt(index);
                            warningDetailsList.RemoveAt(indexDetail);
                            tempControl?.Dispose();
                        }));
                    }
                }
            }
            else
            {
                int index = WarningList.FindIndex(item => item.Code == notification.Code);
                if (notification.IsActive)
                {
                    if (index == -1)
                    {
                        if (InvokeRequired)
                        {
                            Invoke(new MethodInvoker(delegate
                            {
                                WarningList.Add(notification);
                                GetWarningDetails(notification);
                            }));
                        }
                    }
                }
                else
                {
                    int indexDetail = warningDetailsList.FindIndex(item => item.Code == notification.Code);
                    if (InvokeRequired)
                    {
                        Invoke(new MethodInvoker(delegate
                        {
                            WarningList.RemoveAt(index);
                            warningDetailsList.RemoveAt(indexDetail);
                        }));
                    }
                }
            }

            /*if ((closed == false) && (mode == "warning"))
            {
                DestroyList();

                notificationTitle.Visible = false;
                notificationDescription.Visible = false;
                notificationDateTime.Visible = false;
                goTo.Visible = false;
                acknowledge.Visible = false;
                acknowledgeAll.Visible = true;
                troubleshootingTitle.Visible = false;
                troubleshootingDesc.Visible = false;
                troubleshootingImage.Visible = false;

                UpdateWarningList();
            }
            else
            {
                warningDetailsList = null;
            }*/
        }

        private void NotificationList_Paint(object sender, PaintEventArgs e)
        {
            Color leftColor = alarmListColor;
            Color rightColor = alarmDetailsColor;
            if (mode == "warning")
            {
                leftColor = warningListColor;
                rightColor = warningDetailsColor;
            }
            e.Graphics.FillRectangle(new SolidBrush(leftColor), new Rectangle(0, 0, notificationListPanel.Width, MaxHeight));
            e.Graphics.FillRectangle(new SolidBrush(rightColor), new Rectangle(notificationListPanel.Width, 0, this.Width - notificationListPanel.Width, MaxHeight));
            using (Bitmap image = new Bitmap(@"Resources\ShadowNotification.png"))
            {
                e.Graphics.DrawImage(image, new Rectangle(0, 0, this.Width + 5, 20));
            }
        }

        //Da eliminare quando il package è fixato
        /*public async Task<Loccioni.Aulos.Messages.TransferModel.NotificationDetails> AcknowledgeAlarm(string code)
        {
            HttpClient client = new HttpClient();
            HttpResponseMessage response = await client.PutAsync(@"http://" + urlParam["baseUrl"] + ":" + urlParam["urlPort"] + "/Webservice/messages/alarm/" + code + "/acknowledge", null);


            if (response.IsSuccessStatusCode)
            {
                return await response.Content.ReadAsAsync<Loccioni.Aulos.Messages.TransferModel.NotificationDetails>();
            }
            return null;
        }*/

        /* C'è un problema con il flicker della scrollbar se scorro troppo veloce, appare senza motivo
         * per ora lascio la barra classica poi vedo se risolvo questo oppure cerco di fare stili per la scrollbar
        private void List_MouseWheel(object sender, MouseEventArgs e)
        {
            int numberOfTextLinesToMove = e.Delta * SystemInformation.MouseWheelScrollLines / 120;
            int numberOfPixelsToMove = numberOfTextLinesToMove * 19;
            location += numberOfPixelsToMove;
            if (location < list.VerticalScroll.Minimum)
            {
                location = list.VerticalScroll.Minimum;
                list.AutoScrollPosition = new Point(0, list.VerticalScroll.Minimum);
            }
            else if (location > list.VerticalScroll.Maximum)
            {
                location = list.VerticalScroll.Maximum;
                list.AutoScrollPosition = new Point(0, list.VerticalScroll.Maximum);
            }
            else
            {
                list.VerticalScroll.Value = location;
            }
            
        }*/
    }
}
