﻿using WindowsUI.UI;

namespace WindowsUI.Header
{
    partial class NotificationCenter
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.notificationBarPanel = new System.Windows.Forms.Panel();
            this.alarmIcon = new WindowsUI.UI.NoPaddingLabel();
            this.warningIcon = new WindowsUI.UI.NoPaddingLabel();
            this.currentMessage = new System.Windows.Forms.Label();
            this.notificationList = new WindowsUI.Header.NotificationList();
            this.notificationBarPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // notificationBarPanel
            // 
            this.notificationBarPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.notificationBarPanel.Controls.Add(this.alarmIcon);
            this.notificationBarPanel.Controls.Add(this.warningIcon);
            this.notificationBarPanel.Controls.Add(this.currentMessage);
            this.notificationBarPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.notificationBarPanel.Location = new System.Drawing.Point(0, 0);
            this.notificationBarPanel.Margin = new System.Windows.Forms.Padding(0);
            this.notificationBarPanel.Name = "notificationBarPanel";
            this.notificationBarPanel.Size = new System.Drawing.Size(1024, 48);
            this.notificationBarPanel.TabIndex = 0;
            // 
            // alarmIcon
            // 
            this.alarmIcon.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.alarmIcon.Font = new System.Drawing.Font("Aulos", 40F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.alarmIcon.Location = new System.Drawing.Point(904, 4);
            this.alarmIcon.Name = "alarmIcon";
            this.alarmIcon.Size = new System.Drawing.Size(40, 40);
            this.alarmIcon.TabIndex = 2;
            this.alarmIcon.Text = "A";
            this.alarmIcon.Paint += new System.Windows.Forms.PaintEventHandler(this.AlarmIcon_OnPaint);
            // 
            // warningIcon
            // 
            this.warningIcon.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.warningIcon.BackColor = System.Drawing.Color.Transparent;
            this.warningIcon.Font = new System.Drawing.Font("Aulos", 40F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.warningIcon.Location = new System.Drawing.Point(964, 4);
            this.warningIcon.Name = "warningIcon";
            this.warningIcon.Size = new System.Drawing.Size(40, 40);
            this.warningIcon.TabIndex = 1;
            this.warningIcon.Text = "W";
            this.warningIcon.Paint += new System.Windows.Forms.PaintEventHandler(this.WarningIcon_OnPaint);
            // 
            // currentMessage
            // 
            this.currentMessage.AutoSize = true;
            this.currentMessage.Font = new System.Drawing.Font("Open Sans", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.currentMessage.Location = new System.Drawing.Point(512, 12);
            this.currentMessage.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.currentMessage.Name = "currentMessage";
            this.currentMessage.Size = new System.Drawing.Size(0, 24);
            this.currentMessage.TabIndex = 0;
            this.currentMessage.TextChanged += new System.EventHandler(this.currentMessage_TextChanged);
            this.currentMessage.Click += new System.EventHandler(this.currentMessage_Click);
            // 
            // notificationList
            // 
            this.notificationList.Dock = System.Windows.Forms.DockStyle.Top;
            this.notificationList.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.notificationList.Location = new System.Drawing.Point(0, 48);
            this.notificationList.Margin = new System.Windows.Forms.Padding(0);
            this.notificationList.Name = "notificationList";
            this.notificationList.Size = new System.Drawing.Size(1024, 205);
            this.notificationList.TabIndex = 1;
            // 
            // NotificationCenter
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this.notificationList);
            this.Controls.Add(this.notificationBarPanel);
            this.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(0);
            this.Name = "NotificationCenter";
            this.Size = new System.Drawing.Size(1024, 253);
            this.Load += new System.EventHandler(this.NotificationCenter_Load);
            this.notificationBarPanel.ResumeLayout(false);
            this.notificationBarPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel notificationBarPanel;
        private System.Windows.Forms.Label currentMessage;
        private NoPaddingLabel warningIcon;
        private NoPaddingLabel alarmIcon;
        private NotificationList notificationList;
    }
}
