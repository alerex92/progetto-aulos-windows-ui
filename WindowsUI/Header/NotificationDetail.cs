﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsUI.UI;
using Loccioni.Aulos.Messages.WebClient;

namespace WindowsUI.Header
{
    public partial class NotificationDetail : UserControl
    {
        public int Margins { private get; set; }
        public int MaxHeight { private get; set; }
        public string ActiveLabelName { private get; set; }
        public string Mode { private get; set; }

        public AlarmsClient AlarmsClient { private get; set; }
        public WarningsClient WarningsClient { private get; set; }

        //public GlobalConfiguration Configuration { private get; set; }

        public event EventHandler GoToClickParent;

        public NotificationDetail()
        {
            InitializeComponent();
        }

        public void Setup()
        {
            notificationTitle.MaximumSize = new Size(this.Width - 20, 30);
            //notificationTitle.Location = new Point(Margins, Margins);
            //notificationDescription.Location = new Point(Margins, notificationTitle.Location.Y + notificationTitle.Height + Margins);
            goTo.Location = new Point(this.Width / 2 - 5 - goTo.Width, MaxHeight - Margins - goTo.Height);
            acknowledge.Location = new Point(this.Width / 2 + 5, MaxHeight - Margins - acknowledge.Height);
            acknowledgeAll.Location = new Point((this.Width - acknowledgeAll.Width) / 2, MaxHeight - Margins - acknowledgeAll.Height);
            notificationDescription.MaximumSize = new Size(this.Width - 2 * Margins, acknowledge.Location.Y - notificationDescription.Location.Y - notificationDateTime.Height);
            HideDetail();
        }

        public void ShowDetail(Loccioni.Aulos.Messages.TransferModel.NotificationDetails details)
        {
            notificationTitle.Text = details.ShortText;
            notificationDescription.Text = details.LongText;
            notificationDateTime.Text = details.Timestamp.ToString("HH:mm:ss dd/MM/yyyy");
            notificationDateTime.Location = new Point(Margins, notificationDescription.Height + notificationDescription.Location.Y);
            goTo.Visible = true;
            acknowledge.Visible = true;
            acknowledgeAll.Visible = false;
        }

        public void HideDetail()
        {
            notificationTitle.Text = "";
            notificationDescription.Text = "";
            notificationDateTime.Text = "";
            goTo.Visible = false;
            acknowledge.Visible = false;
            acknowledgeAll.Visible = true;
        }

        private void GoTo_Click(object sender, EventArgs e)
        {
            GoToClickParent?.Invoke(this, EventArgs.Empty);
        }

        private async void Acknowledge_Click(object sender, EventArgs e)
        {
            //In teoria l'acknowledge deve solo mandare il segnale e nient'altro perchè poi dovrebbe scaturirsi un
            //alarms changed event (o il corrispettivo per i warning) e quindi dovrebbe partire da lì l'aggiornamento
            if (Mode == "alarm")
            {
                //alarmsClient.Acknowledge(activeLabelName);
                await AlarmsClient.Acknowledge(ActiveLabelName);
                //DestroyList();
                //UpdateAlarmList();
            }
            else
            {
                //alarmsClient.Acknowledge(activeLabelName);
                await WarningsClient.Acknowledge(ActiveLabelName);
                //DestroyList();
                //UpdateWarningList();
            }

            /*NotificationListItem temp = (NotificationListItem)notificationListSubPanel.Controls.Find(activeLabelName, false)[0];
            temp.Dispose();

            NotificationListItem tempListItem = new NotificationListItem()
            {
                TimeStampText = DateTime.Now.ToString("HH:mm:ss dd/MM/yyyy"),
                NotificationName = "Test acknowledge",
                Dock = DockStyle.Top,
                Name = "22",
            };
            tempListItem.Click += new EventHandler(ListItem_Click);
            notificationListSubPanel.Controls.Add(tempListItem);*/
        }

        private async void AcknowledgeAll_Click(object sender, EventArgs e)
        {
            if (Mode == "alarm")
            {
                //alarmsClient.Acknowledge(activeLabelName);
                await AlarmsClient.Acknowledge("ALL");
                //DestroyList();
                //UpdateAlarmList();
            }
            else
            {
                //alarmsClient.Acknowledge(activeLabelName);
                await WarningsClient.Acknowledge("ALL");
                //DestroyList();
                //UpdateWarningList();
            }
        }
    }
}
