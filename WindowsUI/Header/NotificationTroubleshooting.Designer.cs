﻿namespace WindowsUI.Header
{
    partial class NotificationTroubleshooting
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.troubleshootingDesc = new System.Windows.Forms.Label();
            this.troubleshootingTitle = new System.Windows.Forms.Label();
            this.troubleshootingImage = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.troubleshootingImage)).BeginInit();
            this.SuspendLayout();
            // 
            // troubleshootingDesc
            // 
            this.troubleshootingDesc.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.troubleshootingDesc.Location = new System.Drawing.Point(20, 54);
            this.troubleshootingDesc.Name = "troubleshootingDesc";
            this.troubleshootingDesc.Size = new System.Drawing.Size(145, 131);
            this.troubleshootingDesc.TabIndex = 4;
            this.troubleshootingDesc.Text = "Troubleshooting description";
            // 
            // troubleshootingTitle
            // 
            this.troubleshootingTitle.AutoSize = true;
            this.troubleshootingTitle.Font = new System.Drawing.Font("Open Sans", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.troubleshootingTitle.Location = new System.Drawing.Point(20, 20);
            this.troubleshootingTitle.Name = "troubleshootingTitle";
            this.troubleshootingTitle.Size = new System.Drawing.Size(188, 24);
            this.troubleshootingTitle.TabIndex = 3;
            this.troubleshootingTitle.Text = "TROUBLESHOOTING";
            // 
            // troubleshootingImage
            // 
            this.troubleshootingImage.Image = global::WindowsUI.Properties.Resources.noImage;
            this.troubleshootingImage.Location = new System.Drawing.Point(178, 54);
            this.troubleshootingImage.Margin = new System.Windows.Forms.Padding(0);
            this.troubleshootingImage.Name = "troubleshootingImage";
            this.troubleshootingImage.Size = new System.Drawing.Size(145, 131);
            this.troubleshootingImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.troubleshootingImage.TabIndex = 5;
            this.troubleshootingImage.TabStop = false;
            // 
            // NotificationTroubleshooting
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.troubleshootingImage);
            this.Controls.Add(this.troubleshootingDesc);
            this.Controls.Add(this.troubleshootingTitle);
            this.Font = new System.Drawing.Font("Open Sans", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.Name = "NotificationTroubleshooting";
            this.Size = new System.Drawing.Size(341, 205);
            ((System.ComponentModel.ISupportInitialize)(this.troubleshootingImage)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label troubleshootingDesc;
        private System.Windows.Forms.Label troubleshootingTitle;
        private System.Windows.Forms.PictureBox troubleshootingImage;
    }
}
