﻿namespace WindowsUI.Header
{
    partial class NotificationDetail
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.acknowledgeAll = new System.Windows.Forms.Button();
            this.acknowledge = new System.Windows.Forms.Button();
            this.goTo = new System.Windows.Forms.Button();
            this.notificationDateTime = new System.Windows.Forms.Label();
            this.notificationDescription = new System.Windows.Forms.Label();
            this.notificationTitle = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // acknowledgeAll
            // 
            this.acknowledgeAll.BackColor = System.Drawing.Color.White;
            this.acknowledgeAll.FlatAppearance.BorderSize = 0;
            this.acknowledgeAll.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.acknowledgeAll.Location = new System.Drawing.Point(118, 145);
            this.acknowledgeAll.Name = "acknowledgeAll";
            this.acknowledgeAll.Size = new System.Drawing.Size(140, 40);
            this.acknowledgeAll.TabIndex = 11;
            this.acknowledgeAll.Text = "Acknowledge all";
            this.acknowledgeAll.UseVisualStyleBackColor = false;
            this.acknowledgeAll.Click += new System.EventHandler(this.AcknowledgeAll_Click);
            // 
            // acknowledge
            // 
            this.acknowledge.BackColor = System.Drawing.Color.White;
            this.acknowledge.FlatAppearance.BorderSize = 0;
            this.acknowledge.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.acknowledge.Location = new System.Drawing.Point(193, 145);
            this.acknowledge.Name = "acknowledge";
            this.acknowledge.Size = new System.Drawing.Size(120, 40);
            this.acknowledge.TabIndex = 10;
            this.acknowledge.Text = "Acknowledge";
            this.acknowledge.UseVisualStyleBackColor = false;
            this.acknowledge.Click += new System.EventHandler(this.Acknowledge_Click);
            // 
            // goTo
            // 
            this.goTo.BackColor = System.Drawing.Color.White;
            this.goTo.FlatAppearance.BorderSize = 0;
            this.goTo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.goTo.Location = new System.Drawing.Point(63, 145);
            this.goTo.Name = "goTo";
            this.goTo.Size = new System.Drawing.Size(120, 40);
            this.goTo.TabIndex = 9;
            this.goTo.Text = "Go To";
            this.goTo.UseVisualStyleBackColor = false;
            this.goTo.Click += new System.EventHandler(this.GoTo_Click);
            // 
            // notificationDateTime
            // 
            this.notificationDateTime.AutoSize = true;
            this.notificationDateTime.Location = new System.Drawing.Point(20, 73);
            this.notificationDateTime.Margin = new System.Windows.Forms.Padding(0);
            this.notificationDateTime.Name = "notificationDateTime";
            this.notificationDateTime.Size = new System.Drawing.Size(84, 19);
            this.notificationDateTime.TabIndex = 8;
            this.notificationDateTime.Text = "Timestamp";
            // 
            // notificationDescription
            // 
            this.notificationDescription.AutoSize = true;
            this.notificationDescription.Location = new System.Drawing.Point(20, 54);
            this.notificationDescription.Name = "notificationDescription";
            this.notificationDescription.Size = new System.Drawing.Size(135, 19);
            this.notificationDescription.TabIndex = 7;
            this.notificationDescription.Text = "Notification details";
            // 
            // notificationTitle
            // 
            this.notificationTitle.AutoEllipsis = true;
            this.notificationTitle.AutoSize = true;
            this.notificationTitle.Font = new System.Drawing.Font("Open Sans", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.notificationTitle.Location = new System.Drawing.Point(20, 20);
            this.notificationTitle.Name = "notificationTitle";
            this.notificationTitle.Size = new System.Drawing.Size(159, 24);
            this.notificationTitle.TabIndex = 6;
            this.notificationTitle.Text = "Notification title";
            // 
            // NotificationDetail
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.acknowledgeAll);
            this.Controls.Add(this.acknowledge);
            this.Controls.Add(this.goTo);
            this.Controls.Add(this.notificationDateTime);
            this.Controls.Add(this.notificationDescription);
            this.Controls.Add(this.notificationTitle);
            this.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "NotificationDetail";
            this.Size = new System.Drawing.Size(341, 205);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button acknowledgeAll;
        private System.Windows.Forms.Button acknowledge;
        private System.Windows.Forms.Button goTo;
        private System.Windows.Forms.Label notificationDateTime;
        private System.Windows.Forms.Label notificationDescription;
        private System.Windows.Forms.Label notificationTitle;
    }
}
