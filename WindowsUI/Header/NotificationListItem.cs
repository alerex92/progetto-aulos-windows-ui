﻿using System;
using System.Windows.Forms;

namespace WindowsUI.Header
{
    public partial class NotificationListItem : UserControl
    {
        public string TimeStampText { get { return notificationTimestamp.Text; } set { notificationTimestamp.Text = value; } }
        public string NotificationName { get { return notificationName.Text; } set { notificationName.Text = value; } }
        public int InternalMargin { set { notificationName.Padding = new Padding(value, 0, 0, 0); notificationTimestamp.Padding = new Padding(value, 0, 0, 0); } }
        public bool Small { get; set; }

        public NotificationListItem()
        {
            InitializeComponent();
            notificationTimestamp.Width += notificationTimestamp.Padding.Left;

            SetStyle(ControlStyles.OptimizedDoubleBuffer, true);     
        }

        private void NotificationListItem_Load(object sender, EventArgs e)
        {
            if (Small)
            {
                notificationTimestamp.Dock = DockStyle.Top;
                notificationTimestamp.Height = this.Height / 2;
            }
        }

        private void notificationName_Click(object sender, EventArgs e)
        {
            this.OnClick(e);
        }

        private void notificationTimestamp_Click(object sender, EventArgs e)
        {
            this.OnClick(e);
        }
    }
}