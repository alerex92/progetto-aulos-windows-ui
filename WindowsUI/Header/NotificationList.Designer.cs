﻿namespace WindowsUI.Header
{
    partial class NotificationList
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.notificationListPanel = new System.Windows.Forms.Panel();
            this.notificationListSubPanel = new System.Windows.Forms.Panel();
            this.notificationListTitle = new System.Windows.Forms.Label();
            this.notificationTroubleshooting = new WindowsUI.Header.NotificationTroubleshooting();
            this.notificationDetail = new WindowsUI.Header.NotificationDetail();
            this.panel1 = new System.Windows.Forms.Panel();
            this.notificationListPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // notificationListPanel
            // 
            this.notificationListPanel.BackColor = System.Drawing.Color.Transparent;
            this.notificationListPanel.Controls.Add(this.notificationListSubPanel);
            this.notificationListPanel.Controls.Add(this.notificationListTitle);
            this.notificationListPanel.Dock = System.Windows.Forms.DockStyle.Left;
            this.notificationListPanel.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.notificationListPanel.Location = new System.Drawing.Point(0, 0);
            this.notificationListPanel.Margin = new System.Windows.Forms.Padding(0);
            this.notificationListPanel.Name = "notificationListPanel";
            this.notificationListPanel.Size = new System.Drawing.Size(341, 205);
            this.notificationListPanel.TabIndex = 0;
            // 
            // notificationListSubPanel
            // 
            this.notificationListSubPanel.AutoScroll = true;
            this.notificationListSubPanel.BackColor = System.Drawing.Color.Transparent;
            this.notificationListSubPanel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.notificationListSubPanel.Location = new System.Drawing.Point(0, 50);
            this.notificationListSubPanel.Name = "notificationListSubPanel";
            this.notificationListSubPanel.Size = new System.Drawing.Size(341, 155);
            this.notificationListSubPanel.TabIndex = 1;
            // 
            // notificationListTitle
            // 
            this.notificationListTitle.AutoSize = true;
            this.notificationListTitle.BackColor = System.Drawing.Color.Transparent;
            this.notificationListTitle.Font = new System.Drawing.Font("Open Sans", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.notificationListTitle.Location = new System.Drawing.Point(20, 20);
            this.notificationListTitle.Margin = new System.Windows.Forms.Padding(0);
            this.notificationListTitle.Name = "notificationListTitle";
            this.notificationListTitle.Size = new System.Drawing.Size(84, 24);
            this.notificationListTitle.TabIndex = 0;
            this.notificationListTitle.Text = "List title";
            // 
            // notificationTroubleshooting
            // 
            this.notificationTroubleshooting.BackColor = System.Drawing.Color.Transparent;
            this.notificationTroubleshooting.Dock = System.Windows.Forms.DockStyle.Left;
            this.notificationTroubleshooting.Font = new System.Drawing.Font("Open Sans", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.notificationTroubleshooting.Location = new System.Drawing.Point(682, 0);
            this.notificationTroubleshooting.Name = "notificationTroubleshooting";
            this.notificationTroubleshooting.Size = new System.Drawing.Size(341, 205);
            this.notificationTroubleshooting.TabIndex = 4;
            // 
            // notificationDetail
            // 
            this.notificationDetail.BackColor = System.Drawing.Color.Transparent;
            this.notificationDetail.Dock = System.Windows.Forms.DockStyle.Left;
            this.notificationDetail.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.notificationDetail.Location = new System.Drawing.Point(341, 0);
            this.notificationDetail.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.notificationDetail.Name = "notificationDetail";
            this.notificationDetail.Size = new System.Drawing.Size(341, 205);
            this.notificationDetail.TabIndex = 3;
            // 
            // panel1
            // 
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(1023, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1, 20);
            this.panel1.TabIndex = 5;
            // 
            // NotificationList
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.LightGray;
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.notificationTroubleshooting);
            this.Controls.Add(this.notificationDetail);
            this.Controls.Add(this.notificationListPanel);
            this.Font = new System.Drawing.Font("Open Sans", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.Name = "NotificationList";
            this.Size = new System.Drawing.Size(1024, 205);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.NotificationList_Paint);
            this.notificationListPanel.ResumeLayout(false);
            this.notificationListPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel notificationListPanel;
        private System.Windows.Forms.Label notificationListTitle;
        private System.Windows.Forms.Panel notificationListSubPanel;
        private NotificationDetail notificationDetail;
        private NotificationTroubleshooting notificationTroubleshooting;
        private System.Windows.Forms.Panel panel1;
    }
}
