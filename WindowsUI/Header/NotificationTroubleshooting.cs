﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsUI.Header
{
    public partial class NotificationTroubleshooting : UserControl
    {
        public int Margins { private get; set; }
        public int ImageDimension { private get; set; }
        public bool Small { private get; set; }

        public NotificationTroubleshooting()
        {
            InitializeComponent();
        }

        public void Setup()
        {
            troubleshootingTitle.Location = new Point(Margins, Margins);
            //troubleshootingDesc.Location = new Point(Margins, troubleshootingTitle.Location.Y + troubleshootingTitle.Height + Margins);

            if (Small)
            {
                troubleshootingImage.Visible = false;
                troubleshootingDesc.Size = new Size(this.Width - 2 * Margins, this.Height - troubleshootingDesc.Location.Y - Margins);
            }
            else
            {
                troubleshootingImage.Location = new Point(this.Width + Margins / 2, 54);
                troubleshootingImage.Size = new Size(ImageDimension, ImageDimension);
                troubleshootingDesc.Size = new Size((this.Width - Margins) / 2 - Margins, this.Height - troubleshootingDesc.Location.Y - Margins);
            }
            HideTroubleshooting();
        }

        public void ShowTroubleshooting(Loccioni.Aulos.Messages.TransferModel.NotificationDetails details)
        {
            troubleshootingTitle.Visible = true;
            troubleshootingDesc.Text = details.TroubleShooting;
            troubleshootingDesc.Visible = true;
            if (!Small)
            {
                if (!string.IsNullOrEmpty(details.ImageUrl)) // Ancora questa è una parte non stabile nel core, non si sa bene come
                {                                   // verrà gestita
                    troubleshootingImage.Image = Image.FromFile(details.ImageUrl);
                }
                troubleshootingImage.Visible = true;
            }
        }

        public void HideTroubleshooting()
        {
            troubleshootingTitle.Visible = false;
            troubleshootingDesc.Visible = false;
            troubleshootingDesc.Text = "";
            troubleshootingImage.Visible = false;
        }
    }
}
