﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using WindowsUI.UI;
using Loccioni.Aulos;
using Loccioni.Aulos.Messages.TransferModel;
using Loccioni.Aulos.Messages.WebClient;

namespace WindowsUI.Header
{
    //TODO : Da verificare la responsiveness della notification bar e aggiustare in caso i valori
    public partial class NotificationCenter : UserControl
    {
        Color alarmColor = ColorTranslator.FromHtml("#FF3D3D");
        Color warningColor = ColorTranslator.FromHtml("#FFF13D");
        Color safeColor = ColorTranslator.FromHtml("#F2F2F2");

        Timer openAnimationTimer;
        Timer closeAnimationTimer;
        int animationProgress = 0;
        int animationDuration = 400;

        private int iconSizePx;
        private int barHeight;
        private int notificationCenterPercentage = 30;
        private int notificationListMaxHeight;
        private bool small;
        int activeAlarmsNumber = 0;
        int activeWarningsNumber = 0;

        AlarmsClient alarmsClient;
        WarningsClient warningsClient;

        private List<Notification> activeAlarms;
        private List<Notification> activeWarnings;
        private Notification activeNotification;

        //Parametri esterni
        public int FormHeight { private get; set; }
        //private int formWidth;
        //public int FormWidth { set { formWidth = value; } }
        //private Dictionary<string, string> urlParam;
        //public Dictionary<string, string> UrlParam { set{ urlParam = value; } }
        public GlobalConfiguration Configuration { private get; set; }

        public NotificationCenter()
        {
            InitializeComponent();

            openAnimationTimer = new Timer();
            openAnimationTimer.Tick += new EventHandler(OpenAnimation_Tick);
            openAnimationTimer.Interval = 20;

            closeAnimationTimer = new Timer();
            closeAnimationTimer.Tick += new EventHandler(CloseAnimation_Tick);
            closeAnimationTimer.Interval = 20;

            warningIcon.Text = FontAulos.WarningBaloon;
            alarmIcon.Text = FontAulos.AlarmBaloon;
        }

        /* 
         * Qui vado a settare il notification center, cose tipo il layout
        */
        private void NotificationCenter_Load(object sender, EventArgs e)
        {
            //notificationBarPanel.Click += new EventHandler(formclick);
            //alarmIcon.Click += new EventHandler(AlarmIcon_Click);
        }

        public void Setup()
        {
            //TODO dove possibile usa le costanti. invece di case 768 puoi mettere case screenRes.little
            if (FormHeight <= 600)
            {
                barHeight = 40;
                iconSizePx = 34;
                notificationCenterPercentage = 40;
                small = true;
            }
            else if (FormHeight <= 768)
            {
                barHeight = 48;
                iconSizePx = 40;
            }
            else if (FormHeight <= 1024)
            {
                barHeight = 64;
                iconSizePx = 48;
            }
            else
            {
                barHeight = 60;
                iconSizePx = 48;
            }
            notificationListMaxHeight = (FormHeight*notificationCenterPercentage)/100;

            //this.Size = new Size(formWidth, barHeight);
            //notificationBarPanel.Size = new Size(formWidth, barHeight);

            //this.Size = new Size(formWidth, barHeight);
            //this.BackColor = Color.Black;
            notificationBarPanel.Size = new Size(this.Width, barHeight);

            //currentMessage.Location = new Point((notificationBarPanel.Width) / 2, (notificationBarPanel.Height) / 2);
            /*Cambio la dimensione solo dopo che l'ho aggiunta al panel perchè cambia dimensione dopo la Add, perciò se 
             lo faccio prima la posizione viene sfasata*/

            warningIcon.Font = new Font("Aulos", iconSizePx, GraphicsUnit.Pixel);
            warningIcon.Size = new Size(iconSizePx, iconSizePx);
            warningIcon.Location = new Point(notificationBarPanel.Width - 20 - warningIcon.Width,
                                    (notificationBarPanel.Height - warningIcon.Height) / 2);

            alarmIcon.Font = new Font("Aulos", iconSizePx, GraphicsUnit.Pixel);
            alarmIcon.Size = new Size(iconSizePx, iconSizePx);
            alarmIcon.Location = new Point(notificationBarPanel.Width - 40 - alarmIcon.Width - warningIcon.Width,
                                    (notificationBarPanel.Height - alarmIcon.Height) / 2);

            WebClientService webClientService = ServiceBroker.GetService<WebClientService>();

            warningsClient = (WarningsClient) webClientService[typeof(WarningsClient)];
            warningsClient.WarningFired += Warnings_Fired;

            alarmsClient = (AlarmsClient) webClientService[typeof(AlarmsClient)];
            alarmsClient.AlarmsChanged += Alarms_Changed;

            GetAlarms();
            GetWarnings();

            //notificationList.FormWidth = this.Width;
            notificationList.MaxHeight = notificationListMaxHeight;
            notificationList.Small = small;
            //notificationList.UrlParam = urlParam;
            notificationList.Configuration = Configuration;
            notificationList.Setup();
        }

        private void ChangeNotification (string title, string type)
        {
            if(type == "alarm")
            {
                notificationBarPanel.BackColor = alarmColor;
            }
            else if(type == "warning")
            {
                notificationBarPanel.BackColor = warningColor;
            }
            else if(type == "safe")
            {
                notificationBarPanel.BackColor = safeColor;
            }
            if (currentMessage.InvokeRequired)
            {
                currentMessage.Invoke(new MethodInvoker(delegate {
                    currentMessage.Text = title;
                }));
            }
            else
            {
                currentMessage.Text = title;
            }
            //currentMessage.Location = new Point((notificationBarPanel.Width - currentMessage.Width) / 2, (notificationBarPanel.Height - currentMessage.Height) / 2);
        }

        private async void GetAlarms()
        {
            activeAlarms = await alarmsClient.GetActiveAlarms();
            
            activeAlarmsNumber = activeAlarms.Count;
            notificationList.AlarmList = new List<Notification>(activeAlarms);
            notificationList.UpdateAlarmList();
            UpdateAlarmUI();            
        }

        private void UpdateAlarmUI()
        {
            if (alarmIcon.InvokeRequired)
            {
                alarmIcon.Invoke(new MethodInvoker(delegate {
                    alarmIcon.Invalidate();
                    alarmIcon.Update();
                }));
            }
            else
            {
                alarmIcon.Invalidate();
                alarmIcon.Update();
            }
            
            if (activeAlarmsNumber > 0)
            {
                alarmIcon.Click -= AlarmIcon_Click;
                alarmIcon.Click += AlarmIcon_Click;
                ChangeNotification(activeAlarms[activeAlarmsNumber - 1].ShortText, "alarm");
                activeNotification = activeAlarms[activeAlarmsNumber - 1];
            }
            else
            {
                alarmIcon.Click -= AlarmIcon_Click;
                ChangeNotification("", "safe");
                activeNotification = null;
            }
        }

        private async void GetWarnings()
        {
            activeWarnings = await warningsClient.GetActiveWarning();
            
            activeWarningsNumber = activeWarnings.Count;
            notificationList.WarningList = new List<Notification>(activeWarnings);
            notificationList.UpdateWarningList();
            UpdateWarningUI();
        }

        private void UpdateWarningUI()
        {
            if (warningIcon.InvokeRequired)
            {
                warningIcon.Invoke(new MethodInvoker(delegate {
                    warningIcon.Invalidate();
                    warningIcon.Update();
                }));
            }
            else
            {
                warningIcon.Invalidate();
                warningIcon.Update();
            }

            if (activeWarningsNumber > 0)
            {
                warningIcon.Click -= WarningIcon_Click;
                warningIcon.Click += WarningIcon_Click;
                if (activeAlarmsNumber == 0)
                {
                    ChangeNotification(activeWarnings[activeWarningsNumber - 1].ShortText, "warning");
                    activeNotification = activeWarnings[activeWarningsNumber - 1];
                }
            }
            else
            {
                warningIcon.Click -= WarningIcon_Click;
                if (activeAlarmsNumber == 0)
                {
                    ChangeNotification("", "safe");
                    activeNotification = null;
                }
            }
        }

        private void Alarms_Changed(Loccioni.Aulos.Messages.WebClient.AlarmsClient sender, Loccioni.Aulos.Messages.TransferModel.Notification notification)
        {
            if (notification.IsActive)
            {
                activeAlarmsNumber++;
                activeAlarms.Add(notification);
            }
            else
            {
                int index = activeAlarms.FindIndex(item => item.Code == notification.Code);
                activeAlarms.RemoveAt(index);
                activeAlarmsNumber--;
            }
            UpdateAlarmUI();
        }

        private void Warnings_Fired(Loccioni.Aulos.Messages.WebClient.WarningsClient sender, Loccioni.Aulos.Messages.TransferModel.Notification notification)
        {
            if (notification.IsActive)
            {
                activeWarningsNumber++;
                activeWarnings.Add(notification);
            }
            else
            {
                int index = activeWarnings.FindIndex(item => item.Code == notification.Code);
                activeWarnings.RemoveAt(index);
                activeWarningsNumber--;
            }
            UpdateWarningUI();
        }

        private void AlarmIcon_OnPaint(object sender, PaintEventArgs e)
        {
            if (activeAlarmsNumber > 0)
            {
                e.Graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
                Pen pen = new Pen(ColorTranslator.FromHtml("#B0CFFE"));
                SolidBrush brush = new SolidBrush(ColorTranslator.FromHtml("#B0CFFE"));

                RectangleF rect = new RectangleF((iconSizePx / 2), (iconSizePx / 2), (iconSizePx / 2) - 2, (iconSizePx / 2) - 2);

                e.Graphics.DrawEllipse(pen, rect);
                e.Graphics.FillEllipse(brush, rect);

                StringFormat stringFormat = new StringFormat()
                {
                    Alignment = StringAlignment.Center,
                    LineAlignment = StringAlignment.Center
                };
                e.Graphics.DrawString(activeAlarmsNumber.ToString(), new Font("Open Sans", 10, FontStyle.Bold, GraphicsUnit.Pixel),
                                         new SolidBrush(Color.Black), rect, stringFormat);
                pen.Dispose();
                brush.Dispose();
            }
        }

        private void AlarmIcon_Click(object sender, EventArgs e)
        {
            //System.Diagnostics.Debug.WriteLine("entrato");
            if (this.Height == barHeight)
            {
                notificationList.ToggleList("alarm");
                openAnimationTimer.Start();
            }
            else
            {
                closeAnimationTimer.Start();
            }
        }

         private void WarningIcon_Click(object sender, EventArgs e)
        {
            if (this.Height == barHeight)
            {
                notificationList.ToggleList("warning");
                openAnimationTimer.Start();
            }
            else
            {
                closeAnimationTimer.Start();
            }
        }
        
        private void WarningIcon_OnPaint(object sender, PaintEventArgs e)
        {
            if(activeWarningsNumber > 0)
            {
                e.Graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
                Pen pen = new Pen(ColorTranslator.FromHtml("#B0CFFE"));
                SolidBrush brush = new SolidBrush(ColorTranslator.FromHtml("#B0CFFE"));

                RectangleF rect = new RectangleF((iconSizePx / 2), (iconSizePx / 2), (iconSizePx / 2) - 2, (iconSizePx / 2) - 2);

                e.Graphics.DrawEllipse(pen, rect);
                e.Graphics.FillEllipse(brush, rect);

                StringFormat stringFormat = new StringFormat()
                {
                    Alignment = StringAlignment.Center,
                    LineAlignment = StringAlignment.Center
                };
                
                e.Graphics.DrawString(activeWarningsNumber.ToString(), new Font("Open Sans", 10, FontStyle.Bold, GraphicsUnit.Pixel),
                                        new SolidBrush(Color.Black), rect, stringFormat);

                pen.Dispose();
                brush.Dispose();
            }
        }

        private void OpenAnimation_Tick(object sender, EventArgs e)
        {
            if (this.Height < (notificationListMaxHeight + barHeight) - 5)
            {
                this.Height = (int)Animation.EaseInOut(animationProgress, barHeight, notificationListMaxHeight, animationDuration);
                animationProgress += animationDuration / 20;
            }
            else
            {
                this.Height = notificationListMaxHeight + barHeight;
                animationProgress = animationDuration;
                openAnimationTimer.Stop();
            }
            //System.Diagnostics.Debug.WriteLine(this.ParentForm.Controls.Find("contentTape", false).FirstOrDefault().Height);
        }

        private void CloseAnimation_Tick(object sender, EventArgs e)
        {
            if (this.Height > barHeight + 5)
            {
                this.Height = (int)Animation.EaseInOut(animationProgress, barHeight, notificationListMaxHeight, animationDuration);
                animationProgress -= animationDuration / 20;
            }
            else
            {
                this.Height = barHeight;
                animationProgress = 0;
                notificationList.ToggleList("alarm");
                closeAnimationTimer.Stop();
            }
        }

        /* Giusto un test per vedere se funzionavano un pò di cose */
        /*private void formclick(object sender, EventArgs e)
        {
            ChangeNotification("Maximum Level of ES3 serbatoio racolta 5", "alarm");
        }*/

        private void currentMessage_TextChanged(object sender, EventArgs e)
        {
            Size textSize = TextRenderer.MeasureText(currentMessage.Text, currentMessage.Font);
            currentMessage.Width = textSize.Width;
            currentMessage.Location = new Point((notificationBarPanel.Width - textSize.Width) / 2, (notificationBarPanel.Height - textSize.Height) / 2);
        }

        private void currentMessage_Click(object sender, EventArgs e)
        {
            if (currentMessage.Text != "")
            {
                string mode = "warning";
                if (activeAlarmsNumber > 0)
                {
                    mode = "alarm";
                }
                if ((!openAnimationTimer.Enabled) && (notificationList.Closed))
                {
                    openAnimationTimer.Start();
                    notificationList.ToggleList(mode);   
                }
                notificationList.SelectNotification(activeNotification);
            }
        }
    }
}
